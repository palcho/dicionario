#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <stdbool.h>
#include <errno.h>
#include <SDL2/SDL.h>
#define Screen _Screen
#define Font _Font
#define Cursor _Cursor
#include <SDL2/SDL_syswm.h>
#undef Screen
#undef Font
#undef Cursor
#include <ft2build.h>
#include FT_FREETYPE_H
#define STB_TRUETYPE_IMPLEMENTATION
#include "stb_truetype.h"

#define SHIFT_DOWN (event.key.keysym.mod & KMOD_SHIFT)
#define GUI_DOWN (event.key.keysym.mod & KMOD_GUI)
#ifdef __MACOSX__
	#define CTRL_DOWN (event.key.keysym.mod & (KMOD_CTRL | KMOD_ALT))
	#define ALT_DOWN (0)
#else
	#define CTRL_DOWN (event.key.keysym.mod & KMOD_CTRL)
	#define ALT_DOWN (event.key.keysym.mod & KMOD_ALT)
#endif

#define err(...) (fprintf(stderr, "error: " __VA_ARGS__), fprintf(debug_file,"error: " __VA_ARGS__), fclose(debug_file),  exit(-1))
#define warn(...) (fprintf(stderr, "warning: " __VA_ARGS__), fprintf(debug_file, "warning: " __VA_ARGS__))
#define printd(...) (printf(__VA_ARGS__), fprintf(debug_file, __VA_ARGS__))

#define max(a, b) ((a) > (b) ? (a) : (b))
#define min(a, b) ((a) < (b) ? (a) : (b))
#define arrsz(a) (int)(sizeof(a)/sizeof(a[0]))
//#define NUM_GLYPHS 1024
#define INITIAL_BAKED_BUFFER_SIZE 256
#define INITIAL_KERNING_BUFFER_SIZE 512
#define INITIAL_BITMAP_WIDTH 1024
#define INITIAL_BITMAP_HEIGHT 512
#define TOFU_NOT_BAKED_YET 0xfffffffe
#define BLANK_CURSOR 0xffffffff
#define MAX_WIDTH_UNBOUND 0x3fffffff
#define RESQUEEZE_AMOUNT 4
#define MIN_SQUEEZING 1
#define INPUT_SIZE 128
#define MIN_INPUT_LENGTH 2

#define PI    3.14159265f
#define PI2_3 2.0943951f
#define PI4_3 4.1887902f
// unicode characters
#define DASH 8212
#define BREVE 728

typedef struct Font Font;
typedef struct Buffer Buffer;
typedef struct Dictionary Dictionary;
typedef struct Bitmap Bitmap;
typedef struct BakedChar BakedChar;
typedef struct Kerning Kerning;
typedef enum FontFlags FontFlags;
typedef struct Timer Timer;
typedef enum TextInputFlags TextInputFlags;
typedef struct TextInput TextInput;
typedef struct Fonts Fonts;
typedef enum FontWeight FontWeight;
typedef struct Screen Screen;
typedef struct Language Language;
typedef struct Cursor Cursor;
typedef struct ColorSelector ColorSelector;
typedef struct Configuration Configuration;
typedef struct Configs Configs;
//typedef enum FontSqueezing FontSqueezing;

struct Buffer {
	union {
		void *ptr;
		char *ch_ptr;
		uint8_t *u8_ptr;
		float *float_ptr;
		Kerning *kern_ptr;
		BakedChar *baked_ptr;
		Configuration *config_ptr;
	};
	int32_t count, size;
};

struct Bitmap {
	uint8_t *data;
	uint32_t x, y;
	uint32_t last_uploaded_x, last_uploaded_y;
	uint16_t width, height;
	uint16_t bottom_y;
};

enum FontFlags {
	MONOSPACED = 0x01,
	ITALIC     = 0x02,
	BOLDER     = 0x04,
};

struct BakedChar {
	uint32_t unicode;
	uint32_t kern_index, glyph_index;
	uint32_t num_kerns;
	uint32_t x, y;
	uint16_t width, height;
	int16_t lsb_delta, rsb_delta;
	float xoff, yoff, xadvance;
};

struct Kerning {
	uint32_t unicode;
	int16_t advance[2];
};

struct Font {
	const char *ttf_filename;
	FT_Face face;
	stbtt_fontinfo info;
	Buffer ttf_buffer;  // uint8_t *
	Buffer kerning;     // Kerning *
	Buffer baked_chars; // BakedChar *
	Bitmap bitmap;
	SDL_Texture *texture;
	FontFlags flags;
	uint32_t baked_tofu_index;
	int size; // font size
	struct { float x, y; } scaling; // font scaling
	float ascent, descent/*, line_gap*/;
};

struct Dictionary {
	Buffer text_buffer;
	const char *filename;
	char **entries;
	int num_entries;
	//int first_match, last_match; // indices for char **entries
	int input_line_number;
	struct EIL { int entry_index, line_start, line_end; } matched, selected, expanded;
	int selected_expected_line; // when paging up/down
	bool made_a_move; // to update the first_line_marker only when moved through the dictionary
	int first_line_number; // to return the line where the marker is in the new configuration
	char *first_line_marker; // points to first char in first line that appears of an expanded entry with line_start < 0
};

struct Cursor {
	//uint8_t *bitmap;
	SDL_Texture *texture;
	SDL_Rect char_src, char_dst;
	SDL_Rect slab_dst;
	uint32_t glyph_index;
};

struct Language {
	Dictionary *dicts;
	int num_dicts;
	char input[INPUT_SIZE];
	int input_length;
	int input_pos;
	//SDL_Rect cursor_pos;
	int (*strcmp)(char *, char *);
	int dict_index; // the index of the dictionary being used
};


struct Screen {
	SDL_Window *window;
	SDL_Renderer *renderer;
	int width, height;
	int x_pos, y_pos; // limited use
	SDL_Texture *texture;
	uint32_t texture_format;
	struct HDPISc { float x, y; } high_dpi_scaling;
	SDL_Color background_color, foreground_color;
	SDL_Color pure_bg_color, pure_fg_color;
	int refresh_count;
	int num_lines_before_input;
	int num_lines_on_screen;
	int border_width, border_height;
	int max_width, excess_width;
	int line_gap, relative_line_gap;
	float opacity;
	SDL_WindowFlags window_flags;
	bool show_entry_horizontal_border;
	//bool bordered, resizable, always_on_top;
};

struct Timer {
	uint32_t now, last_cursor_update, last_frame, last_screen_refresh, last_second;
};

struct Fonts {
	union {
		struct { Font monospaced, regular, italic, bold; };
		Font array[4];
	};
	int squeezing;
	int weight;

	int resize_amount;
	int resqueeze_amount;
	int reweight_amount;
};

struct ColorSelector {
	//struct Color32 { uint8_t a, b, g, r; } *color_map;
	//SDL_Texture *texture;

	struct Triangle {
		SDL_Texture *texture;
		SDL_Rect src_rect, dst_rect;
		SDL_Point white_vertex, black_vertex, hued_vertex;
	} triangle;

	struct Wheel {
		SDL_Texture *texture;
		SDL_Rect src_rect, dst_rect;
		SDL_Point center;
		int inner_radius, outer_radius;
	} wheel;

	float angle, saturation, lightness;
	SDL_Color pure_color;
	enum { FOREGROUND, BACKGROUND } which_color;
};

struct Configuration {
	// first dictionary PT or EN
	// screen
	int width, height, x_pos, y_pos;
	SDL_Color background_color, foreground_color;
	SDL_Color pure_bg_color, pure_fg_color;
	SDL_WindowFlags window_flags;
	//bool bordered, resizable, always_on_top, fullscreen;
	int num_lines_before_input;
	int max_width;
	int relative_line_gap;
	float opacity;
	// fonts
	int squeezing, weight, base_size;
	bool show_entry_horizontal_border;
};

struct Configs {
	Buffer buffer;
	Configuration *preferred;
	int count, *preferred_index;
	bool *load_window_configs;
};


enum FontWeight { THIN, EXTRA_LIGHT, LIGHT, REGULAR, MEDIUM, SEMI_BOLD, BOLD, BLACK };
//enum FontSqueezing { EXTRA_CONDENSED, CONDENSED, SEMI_CONDENSED, UNCONDENSED };

enum Greek_Unicodes {
	ALPHA_MAIUS   = 913,
	OMEGA_MAIUS   = 937,
	ALPHA_MINUS   = 945,
	OMEGA_MINUS   = 969,
	SIGMA_FINAL   = 962,
	SIGMA_COMMON  = 963,
	ZETA_MINUS    = 950,
	DIGAMMA_MAIUS = 988,
	DIGAMMA_MINUS = 989,
	YOT_MAIUS     = 895,
	YOT_MINUS     = 1011,
};


Buffer read_file (Buffer *buffer, const char *prefix, const char *filename);
void font_from_ttf_file (Font *font, Screen *screen, const char *ttf_filename, int size, int squeezing, FontFlags flags);
void render_font_texture (Font *font, Screen *screen, bool more_than_one_glyph);
uint32_t utf8_to_unicode (char **string);
float kern_advance (Font *font, int ch0_index, int ch1_index);
void load_dictionary (Dictionary *dict, const char *dict_filename);
int find_baked_char_index (BakedChar *baked_chars, int count, uint32_t unicode);
void resize_buffer_if_needed (Buffer *buffer, uint32_t size);
void bake_char_glyph (Font *font, uint32_t unicode, int char_index, bool rebake);
void resize_font (Font *font, Screen *screen, int new_size, int new_squeezing);
void render_text_input (Language *lang, Fonts *fonts, Screen *screen, Cursor *cursor);
void render_dictionary_entries (Dictionary *dict, Fonts *fonts, Screen *screen);
int search_for_entry_index (Dictionary *dict, int (*strcmp)(char*,char*), char *entry);
int latin_strcmp (char *s1, char *s2);
char *noto_font_filename (FontWeight weight, FontFlags flags);
void replace_font (Font *font, Screen *screen, const char *ttf_filename, int size, int squeezing);
char *previous_char (char *current, int num_backwards);
bool draw_selected_entry_marker (Dictionary *dict, Fonts *fonts, Screen *screen);
bool draw_blinking_cursor (Cursor *cursor, Timer *timer, Screen *screen);
int num_lines_from_expanded_entry (Dictionary *dict, Fonts *fonts, Screen *screen, bool mark_first_line);
void displace_lines_above (Dictionary *dict, int threshold, int displacement);
void displace_lines_below (Dictionary *dict, int threshold, int displacement);
void displace_lines (Dictionary *dict, int displacement);
void adapt_to_resizing (Language *langs, int num_langs, Fonts *fonts, Screen *screen);
int num_lines_on_screen_updated (Screen *screen, Fonts *fonts);
int greek_strcmp (char *str1, char *str2);
int convert_to_greek (char *dst, char *src/*, int input_length*/);
void convert_sigmas_if_needed (char *str, int strlen);
void new_cursor (Cursor *cursor, Font *font, Screen *screen, bool remake);
int border_width_updated (Fonts *fonts);
int border_height_updated (Screen *screen, Fonts *fonts);
void draw_color_triangle (ColorSelector *cs);
void init_color_selector (ColorSelector *cs, Screen *screen, bool reinit);
void draw_color_wheel (ColorSelector *cs);
SDL_Color pure_hue_color (SDL_Color color);
float wheel_angle_from_color (SDL_Color color);
SDL_Color color_from_wheel_angle (float angle);
float color_saturation (SDL_Color *color);
float color_lightness (SDL_Color *color);
void draw_color_triangle_cursor (Screen *screen, ColorSelector *cs);
SDL_Color selected_color (ColorSelector *cs);
SDL_HitTestResult hit_test_callback (SDL_Window *window, const SDL_Point *area, void *data);
void load_configurations (Configs *configs);
void recreate_window (Screen *screen, Fonts *fonts, Cursor *cursor);
void save_configuration (Configs *configs);
void assign_configuration (Configs *configs, Screen *screen, Fonts *fonts, int index);
int threaded_dictionaries_loading (void *ptr);
void mark_current_first_lines (Language *langs, int num_langs, Fonts *fonts, Screen *screen);
void modify_fonts (Fonts *fonts, Cursor *cursor, Screen *screen);
void load_window_icon (Screen *screen, char *img_filename);
void draw_signalizer (Screen *screen, Fonts *fonts, int state);
void update_input (Language *lang, Screen *screen, bool text_input_on);

FT_Library ttf_library;
int max_texture_width, max_texture_height;

Configuration default_config = {
	.width = 580, .height = 750,
	.x_pos = SDL_WINDOWPOS_CENTERED, .y_pos = SDL_WINDOWPOS_CENTERED,
	.foreground_color = { 0xff, 0xeb, 0xcc, 0xff },
	.background_color = { 0x12, 0x0e, 0x0c, 0xff },
	.pure_bg_color = { 0xff, 0x55, 0x00, 0xff },
	.pure_fg_color = { 0xff, 0x9b, 0x00, 0xff },
	.window_flags = SDL_WINDOW_ALLOW_HIGHDPI | SDL_WINDOW_RESIZABLE,
	.num_lines_before_input = 3,
	.max_width = MAX_WIDTH_UNBOUND,
	.relative_line_gap = 0, .opacity = 1.0f,
	.squeezing = 90, .weight = REGULAR, .base_size = 18,
	.show_entry_horizontal_border = false,
};

FILE *debug_file = NULL;

int main (int argc, char **argv) {
	Screen screen;
	SDL_Event event;
	Fonts fonts;
	Language langs[2];
	Timer timer;
	Cursor cursor;
	ColorSelector color_selector;
	Configs configs;
	SDL_Thread *thread;
	int result;

	timer.now = SDL_GetTicks();
	debug_file = fopen("misc/debug.txt", "w");
	if (!debug_file) err("failed to open misc/debug.txt: %s\n", strerror(errno));
	printd("opening debug file = %ums\n", SDL_GetTicks() - timer.now);

	timer.now = SDL_GetTicks();
	Dictionary latin_dicts_array[2], greek_dicts_array[1];
	langs[0] = (Language) {
		.dicts = latin_dicts_array,
		.num_dicts = arrsz(latin_dicts_array),
		.dict_index = 0,
		.strcmp = latin_strcmp,
	};
	langs[1] = (Language) {
		.dicts = greek_dicts_array,
		.num_dicts = arrsz(greek_dicts_array),
		.dict_index = 0,
		.strcmp = greek_strcmp,
	};
	thread = SDL_CreateThread(threaded_dictionaries_loading, "dict_thread", &langs);
	if (!thread) err("failed to create thread: %s\n", SDL_GetError());
	printd("create thread time = %ums\n", SDL_GetTicks() - timer.now);

	load_configurations(&configs);
	assign_configuration(&configs, &screen, &fonts, -1);
	printd("configuration loading took %ums\n", SDL_GetTicks() - timer.now);

	timer.now = SDL_GetTicks();
	result = SDL_Init(SDL_INIT_VIDEO);
	if (result != 0) err("failed to initialize video: %s\n", SDL_GetError());

	screen.window = SDL_CreateWindow("Dicionário Latino", screen.x_pos, screen.y_pos, screen.width, screen.height, screen.window_flags);
	if (!screen.window) err("failed to create window: %s\n", SDL_GetError());
	printd("creating window took %ums\n", SDL_GetTicks() - timer.now);

	result = SDL_SetHint("SDL_RENDER_SCALE_QUALITY", "1");
	if (!result) err("failed to set render scale quality hint to linear: %s\n", SDL_GetError());

	timer.now = SDL_GetTicks();
	screen.renderer = SDL_CreateRenderer(screen.window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC | SDL_RENDERER_TARGETTEXTURE);
	if (!screen.renderer) err("failed to create renderer: %s\n", SDL_GetError());
	{
		int width = screen.width, height = screen.height;
		SDL_GetRendererOutputSize(screen.renderer, &screen.width, &screen.height);
		screen.high_dpi_scaling.x = screen.width / (float)width;
		screen.high_dpi_scaling.y = screen.height / (float)height;
	}

	SDL_RendererInfo renderer_info;
	result = SDL_GetRendererInfo(screen.renderer, &renderer_info);
	if (result != 0) err("failed to get renderer info: %s\n", SDL_GetError());

	printd("number of texture formats = %u\n", renderer_info.num_texture_formats);
	screen.texture_format = renderer_info.texture_formats[0];
	max_texture_width = renderer_info.max_texture_width;
	max_texture_height = renderer_info.max_texture_height;
	screen.texture = SDL_CreateTexture(screen.renderer, screen.texture_format, SDL_TEXTUREACCESS_TARGET, screen.width, screen.height);
	if (!screen.texture) err("failed to create texture: %s\n", SDL_GetError());

	//screen.foreground_color = (SDL_Color){ 0xff, 0xeb, 0xcc, 0xff };
	//screen.background_color = (SDL_Color){ 0x12, 0x0e, 0x0c, 0xff };
	//screen.pure_fg_color = pure_hue_color(screen.foreground_color);
	//screen.pure_bg_color = pure_hue_color(screen.background_color);
	SDL_SetRenderDrawColor(screen.renderer, screen.background_color.r, screen.background_color.g, screen.background_color.b, screen.background_color.a);

	screen.refresh_count = 0;
	//SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
	printd("renderer output size = %dx%d, high dpi scale = %gx%g\n", screen.width, screen.height, screen.high_dpi_scaling.x, screen.high_dpi_scaling.y);

	{ // for curiosity's sake
		int num_renders = SDL_GetNumRenderDrivers();
		printd("number of renderers = %d\n", num_renders);
		SDL_RendererInfo info;
		for (int i = 0; i < num_renders; i++) {
			result = SDL_GetRenderDriverInfo(i, &info);
			if (result != 0) err("failed to get renderer info: %s\n", SDL_GetError());
			printd("renderer %d: %s\n", i, info.name);
		}
	}
	printd("renderer and textures loading took %ums\n", SDL_GetTicks() - timer.now);

	// fonts
	timer.now = SDL_GetTicks();
	result = FT_Init_FreeType(&ttf_library);
	if (result != 0) err("failed to initialize freetype2\n");

	fonts.weight = configs.preferred->weight;
	fonts.squeezing = configs.preferred->squeezing;
	fonts.resize_amount = fonts.resqueeze_amount = fonts.reweight_amount = 0;
	font_from_ttf_file(&fonts.regular, &screen, noto_font_filename(fonts.weight, 0), configs.preferred->base_size, fonts.squeezing, 0);
	font_from_ttf_file(&fonts.bold, &screen, noto_font_filename(min(fonts.weight+3 , BLACK), BOLDER), configs.preferred->base_size, 1.3f*fonts.squeezing, BOLDER);
	font_from_ttf_file(&fonts.italic, &screen, noto_font_filename(fonts.weight, ITALIC), configs.preferred->base_size, fonts.squeezing, ITALIC);
	font_from_ttf_file(&fonts.monospaced, &screen, "cmuntb.ttf", configs.preferred->base_size + 2, fonts.squeezing, MONOSPACED);
	printd("fonts loading took %ums\n", SDL_GetTicks() - timer.now);
	timer.now = SDL_GetTicks();

	for (int i = 0; i < arrsz(fonts.array); i++) {
		for (uint32_t c = 0; c < 128; c++) {
			bake_char_glyph(&fonts.array[i], c, c, false);
		}
		render_font_texture(&fonts.array[i], &screen, true);
		//printf("has kerning = %ld\n", FT_HAS_KERNING(fonts.array[i].face));
		//printf("kerning count = %d\n", fonts.array[i].kerning.count);
	}

	//screen.num_lines_before_input = 3;
	//screen.relative_line_gap = screen.line_gap = 0;
	screen.line_gap = 0.05f * screen.relative_line_gap * (fonts.regular.ascent - fonts.regular.descent) + 0.5f;
	screen.num_lines_on_screen = num_lines_on_screen_updated(&screen, &fonts);
	screen.border_width = border_width_updated(&fonts);
	screen.border_height = border_height_updated(&screen, &fonts);

	//screen.max_width = MAX_WIDTH_UNBOUND;
	//screen.excess_width = 0;
	screen.excess_width = max((screen.width - screen.max_width) / 2, 0);
	//screen.opacity = 1.0f;
	//screen.window_flags = SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI;
	//screen.bordered = true;
	//screen.resizable = true;
	//screen.always_on_top = false;
	result = SDL_SetWindowHitTest(screen.window, ((~screen.window_flags & SDL_WINDOW_BORDERLESS) ? NULL : hit_test_callback), NULL);
	if (result != 0) warn("could not set window hit test: %s\n", SDL_GetError());


	new_cursor(&cursor, &fonts.monospaced, &screen, false);

	init_color_selector(&color_selector, &screen, false);

	/*
	// print font in a txt
	FILE *font_file = fopen("font.txt", "w");
	for (int k = 0; k < 4; k++) {
		for (int j = 0; j < 512; j++) {
			for (int i = 0; i < 256; i++) {
				fputc(" .:ioVM@"[fonts.monospaced.bitmap.data[j*256 + i] >> 5], font_file);
			}
			fputc('\n', font_file);
		}
		fputc('\n', font_file);
	}
	fclose(font_file);
	*/

	// system specific window manager
	bool is_x11 = false;
	SDL_SysWMinfo sys_wm;
	SDL_VERSION(&sys_wm.version);
	result = SDL_GetWindowWMInfo(screen.window, &sys_wm);
	if (!result) {
		warn("failed to get system's window manager info: %s\n", SDL_GetError());
	} else {
		if (sys_wm.subsystem == SDL_SYSWM_X11) {
			is_x11 = true;
		}
		printd("window manager %d\n", sys_wm.subsystem);
	}
	printd("glyphs, cursor, and color selector took %ums\n", SDL_GetTicks() - timer.now);

	timer.now = SDL_GetTicks();
	load_window_icon(&screen, "misc/icon.bmp");
	printd("loading window icon took %ums\n", SDL_GetTicks() - timer.now);

	timer.now = SDL_GetTicks();
	SDL_WaitThread(thread, &result);
	for (int j = 0; j < arrsz(langs); j++) {
		strcpy(langs[j].input, "> ");
		langs[j].input_pos = langs[j].input_length = MIN_INPUT_LENGTH;
		for (int i = 0; i < langs[j].num_dicts; i++) {
			langs[j].dicts[i].input_line_number = screen.num_lines_before_input;
			langs[j].dicts[i].matched.line_end = langs[j].dicts[i].matched.line_start = screen.num_lines_before_input+1;
			langs[j].dicts[i].matched.entry_index = 0;
		}
	}
	printd("waiting thread took %ums\n", SDL_GetTicks() - timer.now);

	fflush(debug_file);

	// very sketchy -- not so much anymore
	int lang_index = 0;
	Language *lang = &langs[lang_index];
	//char *text = lang->input;
	//int text_cursor = lang->input_length;
	Dictionary *dict = &lang->dicts[0];
	//int font_index = 5;
	bool text_input_on = true;
	SDL_StartTextInput();
	//SDL_Rect src, dst;
	int first_entry_index = 0;
	int selected_entry_index = -1;
	//bool cursor_changed = false;
	bool alt_key_pressed = false;

	timer.now = timer.last_second = timer.last_cursor_update = timer.last_frame = timer.last_screen_refresh = SDL_GetTicks();

	bool quit = false;
	bool refresh_screen = true;
	bool colors_changed = false;
	bool change_fullscreeness = false;
	bool show_color_selector = false;
	bool redraw_color_selector = true;
	bool change_color_triangle = true;
	bool window_focused = true;
	bool focus_just_gained = false;
	bool input_just_started = false; // for Windows
	int show_signalizer = 0;

	SDL_EventState(SDL_MOUSEMOTION, SDL_DISABLE);

	while (!quit) {
		//bool expanded_updated = false;
		bool input_updated = false;

		while (SDL_PollEvent(&event)) {
			//printf("event type = 0x%04x\n", event.type);
			switch (event.type) {
			case SDL_QUIT:
				quit = true; // useless
				goto quitting_jump;
			case SDL_KEYDOWN:
				//printf("DOWN: key = %d, ctrl: %u, shift: %u, alt: %u, gui: %u, repeat: %u, timestamp = %u\n", event.key.keysym.sym, event.key.keysym.mod & KMOD_CTRL, event.key.keysym.mod & KMOD_SHIFT, event.key.keysym.mod & KMOD_ALT, event.key.keysym.mod & KMOD_GUI, event.key.repeat, event.key.timestamp);
				if (focus_just_gained) break;
				//printf("key pressed = %c | %u\n", event.key.keysym.sym, event.key.keysym.mod & CTRL_MOD);

				switch (event.key.keysym.sym) {
				int displacement, target_line;
				case SDLK_ESCAPE:
					if (text_input_on) {
						SDL_StopTextInput();
						text_input_on = false;
						dict->made_a_move = true;
						screen.refresh_count = 0;
						// to fix bug in which text_input_on = false, but dict->selected.entry_index = -1, resulting in segfault
						update_input(lang, &screen, text_input_on);
					} else if (show_color_selector) {
						show_color_selector = false;
						screen.refresh_count = 0;
					}
					break;
				case SDLK_i:
					if (!text_input_on) {
						if (!CTRL_DOWN && !SHIFT_DOWN && !show_color_selector) { // insert text
							if (SHIFT_DOWN || ALT_DOWN || GUI_DOWN) break;
							SDL_StartTextInput();
							text_input_on = true;
							input_just_started = true;
							timer.last_cursor_update = timer.now;
							for (int i = 0; i < lang->num_dicts; i++) {
								Dictionary *dict = &lang->dicts[i];
								dict->input_line_number = screen.num_lines_before_input;
								dict->selected.entry_index = -1;
								//dict->expanded.entry_index = -1;
							}
							dict->made_a_move = true;
							input_updated = true;
							screen.refresh_count = 0;

						} else if (SHIFT_DOWN && CTRL_DOWN) { // invert colors
							if (ALT_DOWN || GUI_DOWN) break;
							SDL_Color temp_color;
							temp_color = screen.background_color;
							screen.background_color = screen.foreground_color;
							screen.foreground_color = temp_color;
							temp_color = screen.pure_bg_color;
							screen.pure_bg_color = screen.pure_fg_color;
							screen.pure_fg_color = temp_color;
							colors_changed = !colors_changed;
							if (show_color_selector) {
								if (color_selector.which_color == FOREGROUND) {
									goto foreground_invert_jump;
								} else {
									goto background_invert_jump;
								}
							}
							screen.refresh_count = 0;
						}

					}
					break;

				case SDLK_w:
					if (text_input_on && CTRL_DOWN && lang->input_pos > MIN_INPUT_LENGTH) { // erase word backwards
						if (SHIFT_DOWN || ALT_DOWN || GUI_DOWN) break;
						int text_cursor = lang->input_pos;
						while (lang->input[--text_cursor] == ' ' && text_cursor > MIN_INPUT_LENGTH);
						while (lang->input[--text_cursor] != ' ');
						int diff = lang->input_pos - (++text_cursor);
						for (char *p = &lang->input[text_cursor]; p <= &lang->input[lang->input_length] - diff; p++) {
							*p = *(p + diff);
						}
						lang->input_pos -= diff;
						lang->input_length -= diff;
						screen.refresh_count = 0;
						input_updated = true;
						convert_sigmas_if_needed(lang->input+MIN_INPUT_LENGTH, lang->input_length-MIN_INPUT_LENGTH);
					} else if (!text_input_on && SHIFT_DOWN && CTRL_DOWN) {  // load or not window's configurations
						if (ALT_DOWN || GUI_DOWN) break;
						*configs.load_window_configs = !*configs.load_window_configs;
						save_configuration(&configs);
						printf("load window configurations = %s\n", *configs.load_window_configs ? "true" : "false");
						show_signalizer = *configs.load_window_configs ? 1 : -1;
					}
					timer.last_cursor_update = timer.now;
					break;

				case SDLK_a:
					if (SHIFT_DOWN || ALT_DOWN || GUI_DOWN) break;
					if (text_input_on && CTRL_DOWN && lang->input_pos < lang->input_length) { // move cursor to end of input
						lang->input_pos = lang->input_length;
						screen.refresh_count = 0;
					}
					timer.last_cursor_update = timer.now;
					break;

				case SDLK_r:
					if (text_input_on && CTRL_DOWN && lang->input_length > MIN_INPUT_LENGTH) { // erase all input
						if (SHIFT_DOWN || ALT_DOWN || GUI_DOWN) break;
						for (int i = lang->input_length; i >= MIN_INPUT_LENGTH; i--) {
							lang->input[i] = '\0';
						}
						lang->input_pos = lang->input_length = MIN_INPUT_LENGTH;
						input_updated = true;
						screen.refresh_count = 0;
					} else if (!text_input_on && SHIFT_DOWN && CTRL_DOWN) { // make window resizable or not
						if (ALT_DOWN || GUI_DOWN) break;
						screen.window_flags = SDL_GetWindowFlags(screen.window);
						screen.window_flags ^= SDL_WINDOW_RESIZABLE;
						//screen.resizable = !screen.resizable;
						SDL_SetWindowResizable(screen.window, !(~screen.window_flags & SDL_WINDOW_RESIZABLE));
						printf("window resizable = %s\n", (screen.window_flags & SDL_WINDOW_RESIZABLE) ? "true" : "false");
						show_signalizer = (screen.window_flags & SDL_WINDOW_RESIZABLE) ? 1 : -1;
					}
					timer.last_cursor_update = timer.now;
					break;

				case SDLK_s:
					if (!CTRL_DOWN) break;
					// fallthrough
				case SDLK_BACKSPACE:
					if (SHIFT_DOWN || ALT_DOWN || GUI_DOWN) break;
					if (text_input_on && lang->input_pos > MIN_INPUT_LENGTH) { // delete backward
						int text_cursor = lang->input_pos;
						if (CTRL_DOWN && event.key.keysym.sym == SDLK_BACKSPACE) {
							while (lang->input[--text_cursor] == ' ' && text_cursor > MIN_INPUT_LENGTH);
							while (lang->input[--text_cursor] != ' ');
							text_cursor++;
						} else {
							while ((lang->input[--text_cursor] & 0xc0) == 0x80);
						}
						int diff = lang->input_pos - text_cursor;
						for (char *p = &lang->input[text_cursor]; p <= &lang->input[lang->input_length] - diff; p++) {
							*p = *(p + diff);
						}
						lang->input_pos -= diff;
						lang->input_length -= diff;
						screen.refresh_count = 0;
						input_updated = true;
						convert_sigmas_if_needed(lang->input+MIN_INPUT_LENGTH, lang->input_length-MIN_INPUT_LENGTH); // @fix
					}
					timer.last_cursor_update = timer.now;
					break;

				case SDLK_c:
				case SDLK_x:
				case SDLK_DELETE:
					if (SHIFT_DOWN || ALT_DOWN || GUI_DOWN) break;
					if ((event.key.keysym.sym == SDLK_x || event.key.keysym.sym == SDLK_c) && !CTRL_DOWN) break;
					if (text_input_on && lang->input_pos < lang->input_length) { // delete forward
						//printf("1: len = %d, pos = %d\n", lang->input_length, lang->input_pos);
						int text_cursor = lang->input_pos;
						if (event.key.keysym.sym == SDLK_c || (event.key.keysym.sym == SDLK_DELETE && CTRL_DOWN)) {
							while (lang->input[++text_cursor] == ' ');
							while (lang->input[++text_cursor] != ' ' && text_cursor <= lang->input_length);
							text_cursor = min(text_cursor, lang->input_length);
						} else {
							while ((lang->input[++text_cursor] & 0xc0) == 0x80);
						}
						int diff = text_cursor - lang->input_pos;
						for (char *p = &lang->input[lang->input_pos]; p <= &lang->input[lang->input_length] - diff; p++) {
							*p = *(p + diff);
						}
						lang->input_length -= diff;
						convert_sigmas_if_needed(lang->input+MIN_INPUT_LENGTH, lang->input_length-MIN_INPUT_LENGTH);
						screen.refresh_count = 0;
						input_updated = true;
					}
					timer.last_cursor_update = timer.now;
					break;

				case SDLK_SPACE:
					if (!text_input_on) {
						if (ALT_DOWN || GUI_DOWN) break;
						// @make a variable to distinguish between back and front color change
						if (SHIFT_DOWN) { // change foreground color
							if (show_color_selector && color_selector.which_color == FOREGROUND) {
								show_color_selector = false;
							} else {
								show_color_selector = true;
							foreground_invert_jump:
								change_color_triangle = true;
								//color_selector.pure_color = pure_hue_color(screen.foreground_color);
								color_selector.angle = wheel_angle_from_color(screen.pure_fg_color);
								//printf("foreground angle = %f\n", color_selector.angle);
								color_selector.pure_color = screen.pure_fg_color;
								color_selector.saturation = color_saturation(&screen.foreground_color);
								color_selector.lightness = color_lightness(&screen.foreground_color);
								color_selector.which_color = FOREGROUND;
							}
						} else if (CTRL_DOWN) { // change background color
							if (show_color_selector && color_selector.which_color == BACKGROUND) {
								show_color_selector = false;
							} else {
								show_color_selector = true;
							background_invert_jump:
								change_color_triangle = true;
								//color_selector.pure_color = pure_hue_color(screen.background_color);
								color_selector.angle = wheel_angle_from_color(screen.pure_bg_color);
								//printf("background angle = %f\n", color_selector.angle);
								color_selector.pure_color = screen.pure_bg_color;
								color_selector.saturation = color_saturation(&screen.background_color);
								color_selector.lightness = color_lightness(&screen.background_color);
								color_selector.which_color = BACKGROUND;
							}
						}/* else { // @debug: remove from final version
							if (CTRL_DOWN || SHIFT_DOWN || ALT_DOWN || GUI_DOWN) break;
							// show font texture
							font_index = (font_index + 1) % (arrsz(fonts.array) + 1);
						}*/
						screen.refresh_count = 0;
					}
					break;

				case SDLK_TAB:
					if (SHIFT_DOWN || ALT_DOWN || GUI_DOWN) break;
					dict->made_a_move = true;
					if (CTRL_DOWN) { // change language
						//lang->input_length = text_cursor;
						lang_index = (lang_index + 1) % arrsz(langs);
						lang = &langs[lang_index];
						//text_cursor = lang->input_length;
					} else { // change dictionary
						lang->dict_index = (lang->dict_index + 1) % lang->num_dicts;
					}
					dict = &lang->dicts[lang->dict_index];
					if (text_input_on && dict->selected.entry_index >= 0) {
						SDL_StopTextInput();
					} else if (!text_input_on && dict->selected.entry_index < 0) {
						SDL_StartTextInput();
					}
					text_input_on = dict->selected.entry_index < 0;
					screen.refresh_count = 0;
					timer.last_cursor_update = timer.now;
					break;
				//case SDLK_PLUS:
					//fonts.resize_amount++;
					//break;
				case SDLK_PLUS:
				case SDLK_EQUALS:
					if (ALT_DOWN || GUI_DOWN) break;
					if (!text_input_on) {
						// resize to original value
						if (/*SHIFT_DOWN && */CTRL_DOWN) {
							fonts.reweight_amount++;
						} else if (SHIFT_DOWN) {
							fonts.resize_amount++;
						}/* else if (CTRL_DOWN) {
							fonts.reweight_amount = REGULAR - fonts.weight;
						} else {
							fonts.resize_amount = 18 - fonts.regular.size;
						}*/
					}
					break;
				case SDLK_MINUS:
					if (/*SHIFT_DOWN || */ALT_DOWN || GUI_DOWN) break;
					if (!text_input_on) {
						if (CTRL_DOWN) {
							fonts.reweight_amount--;
						} else if (SHIFT_DOWN) {
							fonts.resize_amount--;
						}
					}
					break;

				case SDLK_LESS:
				case SDLK_COMMA:
					if (ALT_DOWN || GUI_DOWN) break;
					if (!text_input_on) {
						if (CTRL_DOWN) {
							if (screen.max_width < screen.width) {
								mark_current_first_lines(langs, arrsz(langs), &fonts, &screen);
								screen.max_width = min(screen.max_width + screen.border_width, screen.width);
								adapt_to_resizing(langs, arrsz(langs), &fonts, &screen);
								redraw_color_selector = true;
								screen.refresh_count = 0;
							}/* else if (screen.max_width < MAX_WIDTH_UNBOUND) {
								screen.max_width = screen.width;
							}*/
						} else if (SHIFT_DOWN) {
							fonts.resqueeze_amount -= SHIFT_DOWN ? RESQUEEZE_AMOUNT : 0;
						}
					}
					break;

				case SDLK_GREATER:
				case SDLK_PERIOD:
					if (!text_input_on) {
						if (CTRL_DOWN) {
							if (screen.max_width > 0) {
								mark_current_first_lines(langs, arrsz(langs), &fonts, &screen);
								screen.max_width = max(min(screen.max_width, screen.width) - screen.border_width, 0);
								adapt_to_resizing(langs, arrsz(langs), &fonts, &screen);
								redraw_color_selector = true;
								screen.refresh_count = 0;
							}/* else {
								if (screen.max_width < screen.width) {
									mark_current_first_lines(langs, arrsz(langs), &fonts, &screen);
									screen.max_width = MAX_WIDTH_UNBOUND;
									adapt_to_resizing(langs, arrsz(langs), &fonts, &screen);
									redraw_color_selector = true;
									screen.refresh_count = 0;
								} else {
									screen.max_width = MAX_WIDTH_UNBOUND;
								}
							}*/

						} else if (SHIFT_DOWN) {
							fonts.resqueeze_amount += SHIFT_DOWN ? RESQUEEZE_AMOUNT : 90 - fonts.squeezing;
						}
					}
					break;

				case SDLK_SLASH: // no lateral borders
					if (ALT_DOWN || GUI_DOWN) break;
					if (!text_input_on && CTRL_DOWN && SHIFT_DOWN) {
						if (screen.max_width < screen.width) {
							mark_current_first_lines(langs, arrsz(langs), &fonts, &screen);
							screen.max_width = MAX_WIDTH_UNBOUND;
							adapt_to_resizing(langs, arrsz(langs), &fonts, &screen);
							redraw_color_selector = true;
							screen.refresh_count = 0;
						} else {
							screen.max_width = MAX_WIDTH_UNBOUND;
						}
					}
					break;

					/* // remap these?
				case SDLK_LEFT: // must be remade
					num_entries_before_input = max(num_entries_before_input-1, 0);
					break;
				case SDLK_RIGHT: // must be remade
					num_entries_before_input++;
					break;
					*/
				case SDLK_j:
				case SDLK_DOWN:
					if (ALT_DOWN || GUI_DOWN) break;
					if (!text_input_on) {
						if (show_color_selector) {
							if (CTRL_DOWN) break;
							if (SHIFT_DOWN) {
								color_selector.angle = fmodf(color_selector.angle + PI/64, 2*PI);
								color_selector.pure_color = color_from_wheel_angle(color_selector.angle);
								change_color_triangle = true;
								if (color_selector.which_color == FOREGROUND) {
									screen.foreground_color = selected_color(&color_selector);
									screen.pure_fg_color = color_selector.pure_color;
								} else {
									screen.background_color = selected_color(&color_selector);
									screen.pure_bg_color = color_selector.pure_color;
								}
								colors_changed = true;
								screen.refresh_count = 0;
								//SDL_Color cfa = color_from_wheel_angle(color_selector.angle);
								//float angle = wheel_angle_from_color(c);
								//printf("angle = %f degrees | a<-c = %f | c<-a = %02x.%02x.%02x.%02x | color = %02x.%02x.%02x.%02x, a<-cfa = %f\n", color_selector.angle * 180.0f/PI, wheel_angle_from_color(c) * 180.0f/PI, cfa.r, cfa.g, cfa.b, cfa.a, c.r, c.g, c.b, c.a, angle * 180.0f/PI);
							} else {
								if (color_selector.lightness > 0) {
									color_selector.lightness = max(color_selector.lightness - 1/128.0f, 0.0f);
									if (color_selector.which_color == FOREGROUND) {
										screen.foreground_color = selected_color(&color_selector);
										screen.pure_fg_color = color_selector.pure_color;
									} else {
										screen.background_color = selected_color(&color_selector);
										screen.pure_bg_color = color_selector.pure_color;
									}
									colors_changed = true;
									screen.refresh_count = 0;
								}
							}
						} else {
							if (CTRL_DOWN) goto ctrl_e_jump;
							if (SHIFT_DOWN) {
								mark_current_first_lines(langs, arrsz(langs), &fonts, &screen);
								screen.relative_line_gap++;
								//printf("relative line gap = %d\n", screen.relative_line_gap);
								adapt_to_resizing(langs, arrsz(langs), &fonts, &screen);
								screen.refresh_count = 0;
							} else { // move down
								if (dict->selected.line_end >= screen.num_lines_on_screen) {
									displace_lines(dict, -1);
									dict->made_a_move = true;
									screen.refresh_count = 0;
								} else if (dict->selected.entry_index + 1 < dict->num_entries) {
									dict->selected.entry_index++;
									dict->selected.line_start = dict->selected.line_end + ((dict->selected.line_end+1 == dict->input_line_number) ? 2 : 1);
									dict->selected.line_end = (dict->selected.entry_index == dict->expanded.entry_index) ? dict->expanded.line_end : dict->selected.line_start;

									if (dict->selected.line_start >= screen.num_lines_on_screen) {
										displace_lines(dict, -(dict->selected.line_start - (screen.num_lines_on_screen-1)));
									}/* else if (move) {
										displace_lines(dict, -1);
									}*/
									dict->selected_expected_line = max(dict->selected.line_start, 0);
									dict->made_a_move = true;
									screen.refresh_count = 0;
								}
								//printf("%s\n", dict->entries[dict->selected.entry_index]);
							}
						}
					} else {
						if ((event.key.keysym.sym == SDLK_j && !CTRL_DOWN) || SHIFT_DOWN || ALT_DOWN || GUI_DOWN) break;
						if (screen.num_lines_before_input < screen.num_lines_on_screen - 1) {
							screen.num_lines_before_input++;
							for (int i = 0; i < arrsz(langs); i++) {
								for (int j = 0; j < langs[i].num_dicts; j++) {
									Dictionary *dict = &langs[i].dicts[j];
									if (dict->selected.entry_index < 0) {
										// displace only if it's in input mode
										displace_lines(dict, 1);
									}
								}
							}
							dict->made_a_move = true;
							screen.refresh_count = 0;
						}
						timer.last_cursor_update = timer.now;
					}
					break;

				case SDLK_z:
					if (CTRL_DOWN || SHIFT_DOWN || ALT_DOWN || GUI_DOWN) break;
					if (!text_input_on) {
						screen.show_entry_horizontal_border = !screen.show_entry_horizontal_border;
						screen.refresh_count = 0;
						show_signalizer = screen.show_entry_horizontal_border ? 1 : -1;
					}
					break;

				case SDLK_k:
				case SDLK_UP:
					if (ALT_DOWN || GUI_DOWN) break;
					if (!text_input_on) {
						if (show_color_selector) {
							if (CTRL_DOWN) break;
							if (SHIFT_DOWN) {
								color_selector.angle = color_selector.angle - PI/64;
								if (color_selector.angle < 0) color_selector.angle += 2*PI;
								color_selector.pure_color = color_from_wheel_angle(color_selector.angle);
								change_color_triangle = true;
								if (color_selector.which_color == FOREGROUND) {
									screen.foreground_color = selected_color(&color_selector);
									screen.pure_fg_color = color_selector.pure_color;
								} else {
									screen.background_color = selected_color(&color_selector);
									screen.pure_bg_color = color_selector.pure_color;
								}
								colors_changed = true;
								screen.refresh_count = 0;
								//printf("angle = %.0f degrees\n", color_selector.angle * 180.0f/PI);
							} else {
								if (color_selector.lightness + color_selector.saturation < 1.0f) {
									color_selector.lightness = min(color_selector.lightness + 1/128.0f, 1.0f - color_selector.saturation);
									if (color_selector.which_color == FOREGROUND) {
										screen.foreground_color = selected_color(&color_selector);
										screen.pure_fg_color = color_selector.pure_color;
									} else {
										screen.background_color = selected_color(&color_selector);
										screen.pure_bg_color = color_selector.pure_color;
									}
									colors_changed = true;
									screen.refresh_count = 0;
								} else if (color_selector.lightness < 1.0f) {
									color_selector.saturation = max(color_selector.saturation - 1/128.0f, 0.0f);
									color_selector.lightness = min(color_selector.lightness + 1/128.0f, 1.0f);
									if (color_selector.lightness + color_selector.saturation > 1.0f) {
										float diff = 0.5f * (1.0f - (color_selector.lightness + color_selector.saturation));
										color_selector.lightness  -= diff;
										color_selector.saturation -= diff;
									}
									if (color_selector.which_color == FOREGROUND) {
										screen.foreground_color = selected_color(&color_selector);
										screen.pure_fg_color = color_selector.pure_color;
									} else {
										screen.background_color = selected_color(&color_selector);
										screen.pure_bg_color = color_selector.pure_color;
									}
									colors_changed = true;
									screen.refresh_count = 0;
								}
							}
						} else {
							if (CTRL_DOWN) goto ctrl_y_jump;
							if (SHIFT_DOWN) {
								if (screen.relative_line_gap > -20) {
									mark_current_first_lines(langs, arrsz(langs), &fonts, &screen);
									screen.relative_line_gap--;
									//printf("relative line gap = %d\n", screen.relative_line_gap);
									adapt_to_resizing(langs, arrsz(langs), &fonts, &screen);
									screen.refresh_count = 0;
								}
							} else { // move up
								if (dict->selected.line_start < 0) {
									displace_lines(dict, 1);
									dict->made_a_move = true;
									screen.refresh_count = 0;
								} else if (dict->selected.entry_index - 1 >= 0) {
									dict->selected.entry_index--;
									if (dict->selected.entry_index == dict->expanded.entry_index) {
										dict->selected = dict->expanded;
									} else {
										dict->selected.line_start -= (dict->selected.line_start-1 == dict->input_line_number) ? 2 : 1;
										dict->selected.line_end = dict->selected.line_start;
									}

									if (dict->selected.line_end < 0) {
										displace_lines(dict, -dict->selected.line_end);
									}
									dict->selected_expected_line = dict->selected.line_end;
									dict->made_a_move = true;
									screen.refresh_count = 0;
								}
								//printf("%s\n", dict->entries[dict->selected.entry_index]);
							}
						}
					} else {
						if ((event.key.keysym.sym == SDLK_k && !CTRL_DOWN) || SHIFT_DOWN) break;
						if (screen.num_lines_before_input > 0) {
							screen.num_lines_before_input--;
							for (int i = 0; i < arrsz(langs); i++) {
								for (int j = 0; j < langs[i].num_dicts; j++) {
									Dictionary *dict = &langs[i].dicts[j];
									if (dict->selected.entry_index < 0) {
										// displace only if it's in input mode
										displace_lines(dict, -1);
									}
								}
							}
							dict->made_a_move = true;
							screen.refresh_count = 0;
						}
						timer.last_cursor_update = timer.now;
					}
					break;

				case SDLK_d:
					if (SHIFT_DOWN) break;
					displacement = -screen.num_lines_on_screen/2;
					goto displacement_down_jump;
				case SDLK_f:
					if (!CTRL_DOWN) break;
					if (!text_input_on && SHIFT_DOWN) {
						change_fullscreeness = !change_fullscreeness;
						break;
					}
					// fall through
				case SDLK_PAGEDOWN:
					displacement = -screen.num_lines_on_screen;
				displacement_down_jump:
					if (ALT_DOWN || GUI_DOWN) break;
					if (!text_input_on) {
						bool jumped_over_input = dict->selected_expected_line < dict->input_line_number && dict->selected_expected_line >= dict->input_line_number + displacement;
						if (jumped_over_input && dict->selected_expected_line == dict->input_line_number + displacement) {
							displacement++;
							jumped_over_input = false;
						}
						//printf("jumped over input = %s\n", jumped_over_input ? "true" : "false");
						displace_lines(dict, displacement);
						// if it's inside an expanded entry
						if (dict->selected.entry_index == dict->expanded.entry_index) {
							if (dict->selected.line_end < dict->selected_expected_line) {
								// step out of the expanded entry
								dict->selected.line_start = dict->selected.line_end = dict->selected_expected_line;
								dict->selected.entry_index += dict->selected.line_end - dict->expanded.line_end - jumped_over_input;
							}
						// outside an expanded entry
						} else {
							dict->selected.line_start -= displacement;
							dict->selected.line_end -= displacement;
							// if stepped inside an expanded entry
							if (dict->expanded.entry_index >= 0 && dict->selected.line_start >= dict->expanded.line_start && dict->selected.line_end <= dict->expanded.line_end) {
								dict->selected = dict->expanded;
							// if jumped over an expanded entry
							} else if (dict->expanded.entry_index >= 0 && dict->selected.line_end + displacement < dict->expanded.line_start && dict->selected.line_start > dict->expanded.line_end) {
								dict->selected.entry_index -= displacement + (dict->expanded.line_end - dict->expanded.line_start) + jumped_over_input;
							// unremarkable path
							} else {
								dict->selected.entry_index -= displacement + jumped_over_input;
							}
						}

						if (dict->selected.entry_index >= dict->num_entries) {
							displace_lines(dict, -((dict->num_entries-1) - (dict->selected.entry_index + (jumped_over_input && dict->matched.entry_index == dict->num_entries))));
							dict->selected.entry_index = dict->num_entries - 1;
							if (dict->selected.entry_index == dict->expanded.entry_index) {
								dict->selected = dict->expanded;
							} else {
								dict->selected.line_start = dict->selected.line_end = dict->selected_expected_line;
							}
						}
						dict->made_a_move = true;
						screen.refresh_count = 0;
						//printf("%s\n", dict->entries[dict->selected.entry_index]);
					}
					break;

				case SDLK_y:
					if (SHIFT_DOWN || ALT_DOWN || GUI_DOWN) break;
					if (CTRL_DOWN) {
					ctrl_y_jump:
						if (text_input_on) break;
						displace_lines(dict, 1);
						if (dict->selected.line_start >= screen.num_lines_on_screen) {
							dict->selected.entry_index--;
							if (dict->selected.entry_index < 0) {
								dict->selected.entry_index++;
								displace_lines(dict, -1);
							} else if (dict->selected.entry_index == dict->expanded.entry_index) {
								dict->selected = dict->expanded;
							} else {
								int displacement = (dict->input_line_number == screen.num_lines_on_screen-1) ? -2 : -1;
								dict->selected.line_start += displacement;
								dict->selected.line_end = dict->selected.line_start;
							}
						}
						dict->selected_expected_line = max(dict->selected.line_start, 0);
						dict->made_a_move = true;
						screen.refresh_count = 0;
					}
					break;
				case SDLK_e:
					if (SHIFT_DOWN || ALT_DOWN || GUI_DOWN) break;
					if (CTRL_DOWN) {
						if (text_input_on) {
							//goto input_word_jump;
							lang->input_pos = lang->input_length;
							timer.last_cursor_update = timer.now;
							screen.refresh_count = 0;
						} else {
						ctrl_e_jump:
							displace_lines(dict, -1);
							if (dict->selected.line_end < 0) {
								dict->selected.entry_index++;
								if (dict->selected.entry_index >= dict->num_entries) {
									dict->selected.entry_index--;
									displace_lines(dict, 1);
								} else if (dict->selected.entry_index == dict->expanded.entry_index) {
									dict->selected = dict->expanded;
								} else {
									int displacement = dict->input_line_number == 0 ? 2 : 1;
									dict->selected.line_end += displacement;
									dict->selected.line_start = dict->selected.line_end;
								}
							}
							dict->selected_expected_line = max(dict->selected.line_start, 0);
							dict->made_a_move = true;
							screen.refresh_count = 0;
						}
					} else {
						displacement = screen.num_lines_on_screen/2;
						goto displacement_up_jump;
					}
					break;
				case SDLK_u:
					if (!CTRL_DOWN || SHIFT_DOWN || ALT_DOWN || GUI_DOWN) break;
					displacement = screen.num_lines_on_screen/2;
					goto displacement_up_jump;
				case SDLK_b:
					if (!CTRL_DOWN || ALT_DOWN || GUI_DOWN) break;
					if (text_input_on && CTRL_DOWN && !SHIFT_DOWN) {
						lang->input_pos = MIN_INPUT_LENGTH;
						timer.last_cursor_update = timer.now;
						screen.refresh_count = 0;
					} else if (!text_input_on && SHIFT_DOWN) {
						screen.window_flags ^= SDL_WINDOW_BORDERLESS;
						//screen.bordered = !screen.bordered;
						SDL_SetWindowBordered(screen.window, !(screen.window_flags & SDL_WINDOW_BORDERLESS));
						result = SDL_SetWindowHitTest(screen.window, (!(screen.window_flags & SDL_WINDOW_BORDERLESS) ? NULL : hit_test_callback), NULL);
						if (result != 0) warn("could not set window hit test: %s\n", SDL_GetError());
						printf("window bordered = %s\n", (screen.window_flags & SDL_WINDOW_BORDERLESS) ? "false" : "true");
						show_signalizer = (screen.window_flags & SDL_WINDOW_BORDERLESS) ? -1 : 1;
						break;
					}
					//if (text_input_on) goto input_backward_jump;
					displacement = screen.num_lines_on_screen;
					goto displacement_up_jump;
				case SDLK_PAGEUP:
					if (CTRL_DOWN || SHIFT_DOWN || ALT_DOWN || GUI_DOWN) break;
					displacement = screen.num_lines_on_screen;
				displacement_up_jump:
					if (!text_input_on) {
						bool jumped_over_input = dict->selected_expected_line > dict->input_line_number && dict->selected_expected_line <= dict->input_line_number + displacement;
						if (jumped_over_input && dict->selected_expected_line == dict->input_line_number + displacement) {
							displacement--;
							jumped_over_input = false;
						}
						//printf("jumped over input = %s\n", jumped_over_input ? "true" : "false");
						displace_lines(dict, displacement);
						// if it's inside an expanded entry
						if (dict->selected.entry_index == dict->expanded.entry_index) {
							if (dict->selected.line_start > dict->selected_expected_line) {
								// step out of the expanded entry
								dict->selected.line_start = dict->selected.line_end = dict->selected_expected_line;
								dict->selected.entry_index += dict->selected.line_end - dict->expanded.line_start + jumped_over_input;
							}
						// outside an expanded entry
						} else {
							//dict->selected_expected_line = dict->selected.line_start - displacement;
							dict->selected.line_start -= displacement;
							dict->selected.line_end -= displacement;
							// if stepped inside an expanded entry
							if (dict->expanded.entry_index >= 0 && dict->selected.line_start >= dict->expanded.line_start && dict->selected.line_end <= dict->expanded.line_end) {
								dict->selected = dict->expanded;
							// if jumped over an expanded entry
							} else if (dict->expanded.entry_index >= 0 && dict->selected.line_start + displacement > dict->expanded.line_end && dict->selected.line_end < dict->expanded.line_start) {
								dict->selected.entry_index -= displacement - (dict->expanded.line_end - dict->expanded.line_start) - jumped_over_input;
							// unremarkable path
							} else {
								dict->selected.entry_index -= displacement - jumped_over_input;
							}
						}

						if (dict->selected.entry_index < 0) {
							displace_lines(dict, dict->selected.entry_index - (jumped_over_input && dict->matched.entry_index == 0));
							dict->selected.entry_index = 0;
							if (dict->selected.entry_index == dict->expanded.entry_index) {
								dict->selected = dict->expanded;
							} else {
								dict->selected.line_start = dict->selected.line_end = dict->selected_expected_line;
							}
						}
						dict->made_a_move = true;
						screen.refresh_count = 0;
						//printf("%s\n", dict->entries[dict->selected.entry_index]);
					}
					break;

				case SDLK_m:
					if (ALT_DOWN || GUI_DOWN) break;
					if (!text_input_on && SHIFT_DOWN && !CTRL_DOWN) {
						target_line = screen.num_lines_on_screen/2;
						if (target_line == dict->input_line_number) target_line--;
						if (target_line > dict->selected.line_end) goto shift_l_jump;
						if (target_line < dict->selected.line_start) goto shift_h_jump;
					} else if (text_input_on && CTRL_DOWN) {
						//lang->input_pos = (lang->input_length + MIN_INPUT_LENGTH) / 2;
						int char_counter = 0;
						// count number of characters
						lang->input_pos = MIN_INPUT_LENGTH;
						while (lang->input_pos < lang->input_length) {
							while ((lang->input[++lang->input_pos] & 0xc0) == 0x80);
							char_counter++;
						}
						// go to the middle of them
						lang->input_pos = MIN_INPUT_LENGTH;
						for (int i = 0; i < char_counter/2; i++) {
							while ((lang->input[++lang->input_pos] & 0xc0) == 0x80);
						}
						timer.last_cursor_update = timer.now;
						screen.refresh_count = 0;
					}
					break;

				case SDLK_l:
					if (ALT_DOWN || GUI_DOWN) break;
					if (text_input_on || show_color_selector || !SHIFT_DOWN || CTRL_DOWN) goto right_key_jump;
					target_line = dict->input_line_number != screen.num_lines_on_screen-1 ? screen.num_lines_on_screen-1 : screen.num_lines_on_screen-2;
				shift_l_jump: // from shift + m
					if (dict->selected.line_end < target_line) {
						if (dict->expanded.entry_index > 0 && dict->expanded.line_start <= target_line && dict->expanded.line_end >= target_line) {
							dict->selected = dict->expanded;
						} else {
							int delta_expansion = (dict->expanded.entry_index > 0 && dict->expanded.line_start < target_line && dict->expanded.line_start >= dict->selected.line_start) ? dict->expanded.line_end - dict->expanded.line_start : 0;
							int delta_input = (dict->input_line_number < target_line && dict->input_line_number > dict->selected.line_start) ? 1 : 0;
							dict->selected.entry_index += (target_line - dict->selected.line_start) - (delta_expansion + delta_input);
							dict->selected.line_end = dict->selected.line_start = target_line;
							if (dict->selected.entry_index >= dict->num_entries) {
								if (dict->expanded.entry_index == dict->num_entries-1) {
									dict->selected = dict->expanded;
								} else {
									int overshoot = (dict->num_entries-1) - dict->selected.entry_index;
									if (dict->selected.line_start + overshoot == dict->input_line_number) overshoot--;
									dict->selected.entry_index = dict->num_entries-1;
									dict->selected.line_start += overshoot;
									dict->selected.line_end += overshoot;
									target_line = max(dict->selected.line_start, 0);
								}
							}
						}
						dict->selected_expected_line = target_line;
						dict->made_a_move = true;
						screen.refresh_count = 0;
					}
					break;

				case SDLK_RIGHT:
				if (ALT_DOWN || GUI_DOWN || SHIFT_DOWN) break;
				right_key_jump: // from SDLK_l
					if (!text_input_on) {
						if (show_color_selector) {
							if (color_selector.saturation + color_selector.lightness < 1.0f) {
								color_selector.saturation = min(color_selector.saturation + 1/128.0f, 1.0f - color_selector.lightness);
								if (color_selector.which_color == FOREGROUND) {
									screen.foreground_color = selected_color(&color_selector);
									screen.pure_fg_color = color_selector.pure_color;
								} else {
									screen.background_color = selected_color(&color_selector);
									screen.pure_bg_color = color_selector.pure_color;
								}
								colors_changed = true;
								screen.refresh_count = 0;
							} else if (color_selector.saturation < 1.0f) {
								color_selector.lightness = max(color_selector.lightness - 1/128.0f, 0.0f);
								color_selector.saturation = min(color_selector.saturation + 1/128.0f, 1.0f);
								if (color_selector.lightness + color_selector.saturation > 1.0f) {
									float diff = 0.5f * (1.0f - (color_selector.lightness + color_selector.saturation));
									color_selector.lightness  -= diff;
									color_selector.saturation -= diff;
								}
								if (color_selector.which_color == FOREGROUND) {
									screen.foreground_color = selected_color(&color_selector);
									screen.pure_fg_color = color_selector.pure_color;
								} else {
									screen.background_color = selected_color(&color_selector);
									screen.pure_bg_color = color_selector.pure_color;
								}
								colors_changed = true;
								screen.refresh_count = 0;
							}
						} else {
							if (CTRL_DOWN) break;
							if (dict->selected.entry_index != dict->expanded.entry_index) {
								// shrink previous expanded index (copied from case SDLK_h)
								if (dict->expanded.entry_index >= 0) {
									if (dict->expanded.line_end < 0) {
										displace_lines_above(dict, dict->expanded.line_end, dict->expanded.line_end - dict->expanded.line_start);
									} else {
										if (dict->expanded.line_start < 0) {
											displace_lines_above(dict, 0, -dict->expanded.line_start);
										}
										if (dict->expanded.line_end > dict->expanded.line_start) {
											displace_lines_below(dict, dict->expanded.line_start, -(dict->expanded.line_end - dict->expanded.line_start));
										}
									}
								}

								dict->expanded = dict->selected;
								int expansion_displacement = num_lines_from_expanded_entry(dict, &fonts, &screen, false)/* - 1*/;
								displace_lines_below(dict, dict->expanded.line_start, expansion_displacement);

								dict->expanded.line_end = dict->expanded.line_start + expansion_displacement;
								dict->selected.line_end = dict->expanded.line_end;
								dict->selected_expected_line = max(dict->selected.line_start, 0);

								dict->made_a_move = true;
								screen.refresh_count = 0;
								//printf("expanded!\n");

							} else if (dict->selected.line_end >= screen.num_lines_on_screen) {
								displace_lines(dict, max(-dict->selected.line_start, -(dict->selected.line_end - screen.num_lines_on_screen +1)));
								dict->selected_expected_line = 0;
								dict->made_a_move = true;
								screen.refresh_count = 0;
							} else if (dict->selected.line_start < 0) {
								displace_lines(dict, -dict->selected.line_start);
								dict->selected_expected_line = 0;
								dict->made_a_move = true;
								screen.refresh_count = 0; // @replace it by: refresh_screen = true
							}
						}
					} else {
						if (event.key.keysym.sym == SDLK_l && !CTRL_DOWN) break;
					//input_word_jump: // from ctrl + e
						if (lang->input_pos < lang->input_length) {
							if (event.key.keysym.sym == SDLK_e || (event.key.keysym.sym == SDLK_RIGHT && CTRL_DOWN)) {
								while (lang->input[++lang->input_pos] == ' ');
								while (lang->input[++lang->input_pos] != ' ' && lang->input_pos <= lang->input_length);
								lang->input_pos = min(lang->input_pos, lang->input_length);
							} else {
								while ((lang->input[++lang->input_pos] & 0xc0) == 0x80);
							}
							//cursor_changed = true;
							screen.refresh_count = 0;
						}
						timer.last_cursor_update = timer.now;
					}
					break;

				case SDLK_h:
					if (ALT_DOWN || GUI_DOWN) break;
					if (text_input_on || show_color_selector || !SHIFT_DOWN || CTRL_DOWN) goto left_key_jump;
					target_line = dict->input_line_number != 0 ? 0 : 1;
				shift_h_jump: // from shift + m
					if (dict->selected.line_start > target_line) {
						if (dict->expanded.entry_index > 0 && dict->expanded.line_start <= target_line && dict->expanded.line_end >= target_line) {
							dict->selected = dict->expanded;
						} else {
							int delta_expansion = (dict->expanded.entry_index > 0 && dict->expanded.line_start > target_line && dict->expanded.line_start < dict->selected.line_start) ? dict->expanded.line_end - dict->expanded.line_start : 0;
							int delta_input = (dict->input_line_number > target_line && dict->input_line_number < dict->selected.line_start) ? 1 : 0;
							dict->selected.entry_index -= (dict->selected.line_start - target_line) - (delta_expansion + delta_input);
							dict->selected.line_end = dict->selected.line_start = target_line;
							if (dict->selected.entry_index < 0) {
								if (dict->expanded.entry_index == 0) {
									dict->selected = dict->expanded;
								} else {
									int overshoot = -dict->selected.entry_index;
									if (dict->selected.line_start + overshoot == dict->input_line_number) overshoot++;
									dict->selected.entry_index = 0;
									dict->selected.line_start += overshoot;
									dict->selected.line_end += overshoot;
								}
								target_line = max(dict->selected.line_start, 0);
							}
						}
						dict->selected_expected_line = target_line;
						dict->made_a_move = true;
						screen.refresh_count = 0;
					}
					break;

				case SDLK_LEFT:
				if (ALT_DOWN || GUI_DOWN) break;
				left_key_jump: // from SDLK_h
					if (!text_input_on) {
						if (show_color_selector) {
							if (color_selector.saturation > 0.0f) {
								color_selector.saturation = max(color_selector.saturation - 1/128.0f, 0.0f);
								if (color_selector.which_color == FOREGROUND) {
									screen.foreground_color = selected_color(&color_selector);
									screen.pure_fg_color = color_selector.pure_color;
								} else {
									screen.background_color = selected_color(&color_selector);
									screen.pure_bg_color = color_selector.pure_color;
								}
								colors_changed = true;
								screen.refresh_count = 0;
							}
						} else {
							if (CTRL_DOWN) break;
							if (dict->expanded.entry_index < 0) break;
							if (dict->expanded.line_end < 0) {
								displace_lines_above(dict, dict->expanded.line_end, dict->expanded.line_end - dict->expanded.line_start);
							} else {
								if (dict->expanded.line_start < 0) {
									displace_lines_above(dict, 0, -dict->expanded.line_start);
								}
								if (dict->expanded.line_end > dict->expanded.line_start) {
									displace_lines_below(dict, dict->expanded.line_start, -(dict->expanded.line_end - dict->expanded.line_start));
								}
							}
							dict->selected_expected_line = max(dict->selected.line_start, 0);
							dict->expanded.entry_index = -1;
							dict->made_a_move = true;
							screen.refresh_count = 0;
						}
					} else {
						if (event.key.keysym.sym == SDLK_h && !CTRL_DOWN) break;
					//input_backward_jump: // from ctrl + b
						if (lang->input_pos > MIN_INPUT_LENGTH) {
							if (event.key.keysym.sym == SDLK_b || (event.key.keysym.sym == SDLK_LEFT && CTRL_DOWN)) {
								while (lang->input[--lang->input_pos] == ' ' && lang->input_pos > MIN_INPUT_LENGTH);
								while (lang->input[--lang->input_pos] != ' ');
								lang->input_pos++;
							} else {
								while ((lang->input[--lang->input_pos] & 0xc0) == 0x80);
							}
							//cursor_changed = true;
							screen.refresh_count = 0; // @replace it by: refresh_screen = true
						}
						timer.last_cursor_update = timer.now;
					}
					break;

				case SDLK_o:
					if (ALT_DOWN || GUI_DOWN) break;
					if (!text_input_on) {
						if (SHIFT_DOWN) {
							screen.opacity = min(screen.opacity + 0.05f, 1.0f);
						} else if (CTRL_DOWN) {
							screen.opacity = max(screen.opacity - 0.05f, 0.0f);
						} else break;
						result = SDL_SetWindowOpacity(screen.window, screen.opacity);
						//if (result == 0) printf("window made transparent\n");
						if (result != 0)  warn("failed to change window opacity: %s\n", SDL_GetError());
					}
					break;

				case SDLK_t:
					if (ALT_DOWN || GUI_DOWN) break;
					if (!text_input_on && SHIFT_DOWN && CTRL_DOWN) {
						screen.window_flags = SDL_GetWindowFlags(screen.window);
						screen.window_flags ^= SDL_WINDOW_ALWAYS_ON_TOP;
						SDL_GetWindowPosition(screen.window, &screen.x_pos, &screen.y_pos);
						recreate_window(&screen, &fonts, &cursor);
						printf("window always on top = %s\n", (screen.window_flags & SDL_WINDOW_ALWAYS_ON_TOP) ? "true" : "false");
						show_signalizer = (screen.window_flags & SDL_WINDOW_ALWAYS_ON_TOP) ? 1 : -1;
						// etc
						redraw_color_selector = true;
						timer.last_cursor_update = timer.now;
						screen.refresh_count = 0;
					}
					break;

				case SDLK_HOME:
					if (ALT_DOWN || GUI_DOWN) break;
					if (text_input_on) {
						lang->input_pos = MIN_INPUT_LENGTH;
						//cursor_changed = true;
						timer.last_cursor_update = timer.now;
						screen.refresh_count = 0; // @replace it by: refresh_screen = true
					} else {
						target_line = dict->input_line_number != 0 ? 0 : 1;
						goto shift_h_jump;
					}
					break;

				case SDLK_END:
					if (ALT_DOWN || GUI_DOWN) break;
					if (text_input_on) {
						lang->input_pos = lang->input_length;
						//cursor_changed = true;
						timer.last_cursor_update = timer.now;
						screen.refresh_count = 0; // @replace it by: refresh_screen = true
					} else {
						target_line = dict->input_line_number != screen.num_lines_on_screen-1 ? screen.num_lines_on_screen-1 : screen.num_lines_on_screen-2;
						goto shift_l_jump;
					}
					break;

				case SDLK_RETURN:
					if (ALT_DOWN || GUI_DOWN) break;
					if (!show_color_selector) {
						text_input_on ? SDL_StopTextInput() : SDL_StartTextInput();
						text_input_on = !text_input_on;
						timer.last_cursor_update = timer.now;

						if (text_input_on) {
							for (int i = 0; i < lang->num_dicts; i++) {
								Dictionary *dict = &lang->dicts[i];
								dict->selected.entry_index = -1;
								dict->input_line_number = screen.num_lines_before_input;
								//dict->expanded.entry_index = -1;
							}
							input_updated = true;
						} else {
							// to fix bug in which text_input_on = false, but dict->selected.entry_index = -1, resulting in segfault
							update_input(lang, &screen, text_input_on);
						}
						dict->made_a_move = true;
						screen.refresh_count = 0;
					} else {
						show_color_selector = false;
						screen.refresh_count = 0;
					}
					break;

				case SDLK_LALT:
				case SDLK_RALT:
					// for macos
					alt_key_pressed = true;
					//if (text_input_on) SDL_StopTextInput();
					//printf("alt key pressed\n");
					break;

				case SDLK_0: case SDLK_1: case SDLK_2: case SDLK_3: case SDLK_4:
				case SDLK_5: case SDLK_6: case SDLK_7: case SDLK_8: case SDLK_9:
					if (text_input_on) break;
					if (ALT_DOWN || GUI_DOWN) break;

					if (SHIFT_DOWN && CTRL_DOWN) { // save_configuration
						int index = event.key.keysym.sym - SDLK_0;
						Configuration *saved_config = &configs.buffer.config_ptr[index];

						SDL_GetWindowPosition(screen.window, &screen.x_pos, &screen.y_pos);
						screen.window_flags = SDL_GetWindowFlags(screen.window);
						*saved_config = (Configuration) {
							.width = screen.width / screen.high_dpi_scaling.x,
							.height = screen.height / screen.high_dpi_scaling.y,
							.x_pos = screen.x_pos, .y_pos = screen.y_pos,
							.foreground_color = screen.foreground_color,
							.background_color = screen.background_color,
							.pure_bg_color = screen.pure_bg_color,
							.pure_fg_color = screen.pure_fg_color,
							.window_flags = screen.window_flags,
							.num_lines_before_input = screen.num_lines_before_input,
							.max_width = screen.max_width,
							.relative_line_gap = screen.relative_line_gap,
							.opacity = screen.opacity,
							.squeezing = fonts.squeezing,
							.weight = fonts.weight,
							.base_size = fonts.regular.size,
							.show_entry_horizontal_border = screen.show_entry_horizontal_border,
						};
						printf("configuration %d saved\n", index);
						save_configuration(&configs);
						show_signalizer = 1;

					} else if (CTRL_DOWN) { // make the preferred configuration
						int index = event.key.keysym.sym - SDLK_0;
						Configuration *saved_config = &configs.buffer.config_ptr[index];
						*configs.preferred_index = index;
						configs.preferred = saved_config;
						printf("configuration %d set as preferred\n", index);
						save_configuration(&configs);
						show_signalizer = -1;

					} else if (!SHIFT_DOWN) { // load configuration
						//change_configuration(&configs, &screen, &fonts, index);
						int index = event.key.keysym.sym - SDLK_0;
						mark_current_first_lines(langs, arrsz(langs), &fonts, &screen);
						SDL_WindowFlags previous_flags = SDL_GetWindowFlags(screen.window);

						assign_configuration(&configs, &screen, &fonts, index);

						if (*configs.load_window_configs) {
							if ((previous_flags & SDL_WINDOW_ALWAYS_ON_TOP) ^ (screen.window_flags & SDL_WINDOW_ALWAYS_ON_TOP)) {
								recreate_window(&screen, &fonts, &cursor);
							} else {
								if ((previous_flags & SDL_WINDOW_FULLSCREEN) ^ (screen.window_flags & SDL_WINDOW_FULLSCREEN)) {
									result = SDL_SetWindowFullscreen(screen.window, ((screen.window_flags & SDL_WINDOW_FULLSCREEN) ? SDL_WINDOW_FULLSCREEN_DESKTOP : 0));
									if (result != 0) warn("could not set window fullscreen: %s\n", SDL_GetError());
								}
								if ((previous_flags & SDL_WINDOW_BORDERLESS) ^ (screen.window_flags & SDL_WINDOW_BORDERLESS)) {
									SDL_SetWindowBordered(screen.window, !(screen.window_flags & SDL_WINDOW_BORDERLESS));
								}
								if ((previous_flags & SDL_WINDOW_RESIZABLE) ^ (screen.window_flags & SDL_WINDOW_RESIZABLE)) {
									SDL_SetWindowResizable(screen.window, !(~screen.window_flags & SDL_WINDOW_RESIZABLE));
								}
								result = SDL_SetWindowHitTest(screen.window, (!(screen.window_flags & SDL_WINDOW_BORDERLESS) ? NULL : hit_test_callback), NULL);
								if (result != 0) warn("could not set window hit test: %s\n", SDL_GetError());
								SDL_SetWindowOpacity(screen.window, screen.opacity);
								SDL_SetWindowSize(screen.window, screen.width/screen.high_dpi_scaling.x, screen.height/screen.high_dpi_scaling.y);
								SDL_DestroyTexture(screen.texture);
								screen.texture = SDL_CreateTexture(screen.renderer, screen.texture_format, SDL_TEXTUREACCESS_TARGET, screen.width, screen.height);
								if (!screen.texture) err("failed to recreate screen texture (%dx%d): %s\n", screen.width, screen.height, SDL_GetError());
								printf("renderer size is now: %dx%d\n", screen.width, screen.height);
								//if (screen.x_pos != loaded_config->x_pos || screen.y_pos != loaded_config->y_pos) {
								SDL_SetWindowPosition(screen.window, screen.x_pos, screen.y_pos);
							}
						}
						SDL_SetRenderDrawColor(screen.renderer, screen.background_color.r, screen.background_color.g, screen.background_color.b, screen.background_color.a);
						modify_fonts(&fonts, &cursor, &screen);
						adapt_to_resizing(langs, arrsz(langs), &fonts, &screen);
						redraw_color_selector = true;
						colors_changed = true;
						timer.last_cursor_update = timer.now;
						screen.refresh_count = 0;
						printf("configuration %d loaded\n", index);

						if (color_selector.which_color == FOREGROUND) {
							goto foreground_invert_jump;
						} else {
							goto background_invert_jump;
						}
					}
					break;

				default:
					break;
				}
				break;

			case SDL_KEYUP:
				//printf("UP: key = %d, ctrl: %u, shift: %u, alt: %u, gui: %u, repeat: %u, timestamp: %u\n", event.key.keysym.sym, event.key.keysym.mod & KMOD_CTRL, event.key.keysym.mod & KMOD_SHIFT, event.key.keysym.mod & KMOD_ALT, event.key.keysym.mod & KMOD_GUI, event.key.repeat, event.key.timestamp);
				switch (event.key.keysym.sym) {
				case SDLK_LALT:
				case SDLK_RALT:
					// for macos
					alt_key_pressed = false;
					//if (text_input_on) SDL_StartTextInput();
					//printf("alt key unpressed\n");
					break;
				default:
					break;
				}
				break;

			case SDL_TEXTINPUT:
				if (alt_key_pressed) break;
				if (input_just_started) {
					input_just_started = false;
					break;
				}
				if (lang->input_length < INPUT_SIZE - 8) {
					if (lang_index == 1) { // greek language
						char input[32];
						int diff;
						diff = convert_to_greek(input, event.text.text/*, lang->input_pos-2*/); // @fix
						for (char *p = &lang->input[lang->input_length]; p >= &lang->input[lang->input_pos]; p--) {
							*(p + diff) = *p;
						}
						for (int i = 0; i < diff; i++) {
							lang->input[lang->input_pos + i] = input[i];
						}
						//printf("text cursor = %d\n", text_cursor-2);
						lang->input_length += diff;
						lang->input_pos += diff;
						convert_sigmas_if_needed (lang->input+MIN_INPUT_LENGTH, lang->input_length-MIN_INPUT_LENGTH);
					} else {
						int diff;
						for (diff = 0; event.text.text[diff] != '\0'; diff++);
						for (char *p = &lang->input[lang->input_length]; p >= &lang->input[lang->input_pos]; p--) {
							*(p + diff) = *p;
						}
						for (int i = 0; i < diff; i++) {
							lang->input[lang->input_pos + i] = event.text.text[i];
						}
						lang->input_length += diff;
						lang->input_pos += diff;
					}
					input_updated = true;
					screen.refresh_count = 0;
				} else {
					warn("text cursor at position %d\n", lang->input_length);
				}
				timer.last_cursor_update = timer.now;
				break;

			case SDL_TEXTEDITING:
				//printf("edit: start = %d, length = %d, text = %s\n", event.edit.start, event.edit.length, event.edit.text);
				break;

			case SDL_WINDOWEVENT:
				switch (event.window.event) {
				case SDL_WINDOWEVENT_RESIZED:
					mark_current_first_lines(langs, arrsz(langs), &fonts, &screen);
					SDL_GetRendererOutputSize(screen.renderer, &screen.width, &screen.height);
					// new texture usage
					SDL_DestroyTexture(screen.texture);
					screen.texture = SDL_CreateTexture(screen.renderer, screen.texture_format, SDL_TEXTUREACCESS_TARGET, screen.width, screen.height);
					if (!screen.texture) err("failed to recreate screen texture (%dx%d): %s\n", screen.width, screen.height, SDL_GetError());
					//printf("renderer size is now: %dx%d\n", screen.width, screen.height);
					adapt_to_resizing(langs, arrsz(langs), &fonts, &screen);
					//init_color_selector(&color_selector, &screen, true);
					redraw_color_selector = true;
					timer.last_cursor_update = timer.now;
					screen.refresh_count = 0;
					break;
				case SDL_WINDOWEVENT_EXPOSED:
					refresh_screen = true;
					//timer.last_cursor_update = timer.now;
					break;
				case SDL_WINDOWEVENT_MOVED:
					screen.window_flags = SDL_GetWindowFlags(screen.window);
					if (screen.window_flags & SDL_WINDOW_BORDERLESS) {
						screen.refresh_count = 0;
						//timer.last_cursor_update = timer.now;
					}
					break;
				case SDL_WINDOWEVENT_FOCUS_GAINED:
					window_focused = true;
					focus_just_gained = true;
					timer.last_cursor_update = timer.now - 500;
					break;
				case SDL_WINDOWEVENT_FOCUS_LOST:
					window_focused = false;
					timer.last_cursor_update = timer.now;
					break;
				default:
					break;
				}
				//printf("window event %d\n", event.window.event);
			default:
				break;
			}
		}

		if (colors_changed) {
			SDL_SetRenderDrawColor(screen.renderer, screen.background_color.r, screen.background_color.g, screen.background_color.b, screen.background_color.a);
			for (int i = 0; i < arrsz(fonts.array); i++) {
				//fonts.array[i].bitmap.last_uploaded_x = fonts.array[i].bitmap.last_uploaded_y = 0;
				//render_font_texture(&fonts.array[i], &screen, true);
				SDL_SetTextureColorMod(fonts.array[i].texture, screen.foreground_color.r, screen.foreground_color.g, screen.foreground_color.b);
			}
			cursor.glyph_index = BLANK_CURSOR;
			colors_changed = false;
		}

		// fonts modification, if required
		if (fonts.reweight_amount || fonts.resize_amount || fonts.resqueeze_amount) {
			mark_current_first_lines(langs, arrsz(langs), &fonts, &screen);
			modify_fonts(&fonts, &cursor, &screen);
			adapt_to_resizing(langs, arrsz(langs), &fonts, &screen);
			redraw_color_selector = true;
		}

		if (change_fullscreeness) {
			mark_current_first_lines(langs, arrsz(langs), &fonts, &screen);
			screen.window_flags = SDL_GetWindowFlags(screen.window);
			screen.window_flags ^= SDL_WINDOW_FULLSCREEN_DESKTOP;
			SDL_SetWindowFullscreen(screen.window, screen.window_flags & SDL_WINDOW_FULLSCREEN_DESKTOP);
			// for Windows
			SDL_GetRendererOutputSize(screen.renderer, &screen.width, &screen.height);
			SDL_DestroyTexture(screen.texture);
			screen.texture = SDL_CreateTexture(screen.renderer, screen.texture_format, SDL_TEXTUREACCESS_TARGET, screen.width, screen.height);
			if (!screen.texture) err("failed to recreate screen texture (%dx%d): %s\n", screen.width, screen.height, SDL_GetError());
			//printf("renderer size is now: %dx%d\n", screen.width, screen.height);
			//
			adapt_to_resizing(langs, arrsz(langs), &fonts, &screen);
			redraw_color_selector = true;
			screen.refresh_count = 0;
			change_fullscreeness = false;
			show_signalizer = (screen.window_flags & SDL_WINDOW_FULLSCREEN_DESKTOP) ? 2 : -2;
		}

		bool cursor_swap = text_input_on && ((((((timer.now - timer.last_cursor_update)/500)%2) != (((timer.last_frame - timer.last_cursor_update)/500)%2)) && window_focused) || timer.now == timer.last_cursor_update || focus_just_gained);
		//  draw them all
		if (screen.refresh_count == 0/* || screen.refresh_count == 1 && cursor_swap*/) {
			// new texture usage
			SDL_SetRenderTarget(screen.renderer, screen.texture); //@switch only if necessary?
			SDL_RenderClear(screen.renderer);

			if (input_updated) {
				update_input(lang, &screen, text_input_on);
			}
			if (!text_input_on && selected_entry_index < 0) {
				selected_entry_index = min(first_entry_index, dict->num_entries-1);
			}
			render_dictionary_entries(dict, &fonts, &screen);
			render_text_input(lang, &fonts, &screen, &cursor);
			draw_selected_entry_marker(dict, &fonts, &screen);

			if (show_color_selector) {
				//printf("foreground: saturation = %f, lightness = %f\n", color_saturation(&screen.foreground_color), color_lightness(&screen.foreground_color));
				//printf("background: saturation = %f, lightness = %f\n", color_saturation(&screen.background_color), color_lightness(&screen.background_color));
				if (redraw_color_selector) {
					init_color_selector(&color_selector, &screen, true);
					draw_color_wheel(&color_selector); // @todo: find a suitable location
					if (!change_color_triangle) draw_color_triangle(&color_selector); // @todo: find a suitable location
					redraw_color_selector = false;
				}

				// draw wheel
				SDL_RenderCopyEx(screen.renderer, color_selector.wheel.texture, &color_selector.wheel.src_rect, &color_selector.wheel.dst_rect, color_selector.angle * 180.0f/PI, &color_selector.wheel.center, SDL_FLIP_NONE);
				if (change_color_triangle) {
					//color_selector.pure_color = pure_hue_color(screen.background_color);
					//color_selector.pure_color = color_from_wheel_angle(color_selector.angle);
					draw_color_triangle(&color_selector); // @todo: find a suitable location
					change_color_triangle = false;
				}
				// draw triangle
				SDL_RenderCopy(screen.renderer, color_selector.triangle.texture, &color_selector.triangle.src_rect, &color_selector.triangle.dst_rect);
				draw_color_triangle_cursor(&screen, &color_selector);
			}

			//printf("m{%d,%d,%d}, s{%d,%d,%d}, e{%d,%d,%d}, sel{%d}, i{%d}\n", dict->matched.entry_index, dict->matched.line_start, dict->matched.line_end, dict->selected.entry_index, dict->selected.line_start, dict->selected.line_end, dict->expanded.entry_index, dict->expanded.line_start, dict->expanded.line_end, dict->selected_expected_line, dict->input_line_number);
			refresh_screen = true;
			screen.refresh_count++;
			//printf("screen refresh count = %d\n", screen.refresh_count);

			/*
			if (font_index < arrsz(fonts.array)) {
				// @debugging tool to visualize font textures
				src = (SDL_Rect){ 0, 0, fonts.array[font_index].bitmap.width, min(fonts.array[font_index].bitmap.bottom_y, screen.height) };
				dst = (SDL_Rect){ screen.width - fonts.array[font_index].bitmap.width, 0, fonts.array[font_index].bitmap.width, min(fonts.array[font_index].bitmap.bottom_y, screen.height) };
				SDL_RenderCopy(screen.renderer, fonts.array[font_index].texture, &src, &dst);
			}
			*/
			// new texture usage
			SDL_SetRenderTarget(screen.renderer, NULL); // @switch only if necessary?
			SDL_RenderClear(screen.renderer);
			SDL_RenderCopy(screen.renderer, screen.texture, NULL, NULL);
		}
		if (cursor_swap) {
			//printf("CURSOR SWAP!\n");
			if (screen.refresh_count > 0) {
				SDL_RenderClear(screen.renderer);
				SDL_RenderCopy(screen.renderer, screen.texture, NULL, NULL);
			}
			draw_blinking_cursor(&cursor, &timer, &screen);
			refresh_screen = true;
		}

		if (show_signalizer) {
			if (!refresh_screen) {
				SDL_RenderClear(screen.renderer);
				SDL_RenderCopy(screen.renderer, screen.texture, NULL, NULL);
			}
			draw_signalizer(&screen, &fonts, show_signalizer);
			refresh_screen = true;
			show_signalizer /= 2;
		}


		// @todo: use SDL watch event, with the main thread waiting on it, to spend none of the cpu resources while idle
		if (refresh_screen) {
			//draw_signalizer(&screen, &fonts, screen.show_entry_horizontal_border);
			SDL_RenderPresent(screen.renderer);
			refresh_screen = false;
			//printf("screen refreshed\n");
		}/* else */{
			uint32_t delay = 32;
			if (!text_input_on || !window_focused) { // block till next event
#if defined(__LINUX__) || defined(__BSD__)
				if (is_x11) {
					XEvent xevent;
					//XWindowEvent(sys_wm.info.x11.display, sys_wm.info.x11.window, KeyPressMask | KeyReleaseMask | ButtonPressMask | ButtonReleaseMask | FocusChangeMask | ExposureMask | StructureNotifyMask | VisibilityChangeMask | ColormapChangeMask, &xevent);
					//XMaskEvent(sys_wm.info.x11.display, ~0L, &xevent);
					XNextEvent(sys_wm.info.x11.display, &xevent);
					if (xevent.type == GenericEvent || xevent.type == MotionNotify) {
						delay += 160;
					}
					XPutBackEvent(sys_wm.info.x11.display, &xevent);
					//printf("XEvent type = %d\n", xevent.type);
				}
#elif defined(__WINDOWS__)
				WaitMessage();
#endif
			}
			uint32_t delta_t = SDL_GetTicks() - timer.now;
			if (delta_t < delay) SDL_Delay(delay - delta_t);
			//if (delta_t > 1) printf("delta_t = %u\n", delta_t);
		}


		timer.last_frame = timer.now;
		timer.now = SDL_GetTicks();
		// in case of overflowing after running for 49 uninterrupted days -- may happen to someone anyway
		if (timer.last_frame > timer.now) {
			timer.last_frame = timer.last_cursor_update = timer.last_second = 0;
		}

		focus_just_gained = false;
		input_just_started = false;
	}

quitting_jump:
	SDL_DestroyTexture(screen.texture);
	SDL_DestroyRenderer(screen.renderer);
	SDL_DestroyWindow(screen.window);
	SDL_Quit();
	fclose(debug_file);
	return 0;
}


void load_dictionary (Dictionary *dict, const char *dict_filename) {
	int entries_size = 4096;
	char *text;

	dict->text_buffer = read_file(NULL, "dictionaries/", dict_filename);
	if (dict->text_buffer.count == 0) err("failed to read dictionary file '%s': %s\n", dict_filename, strerror(errno));

	if (dict->text_buffer.ch_ptr[dict->text_buffer.count-1] != '\n') {
		dict->text_buffer.ch_ptr[dict->text_buffer.count] = '\n';
		dict->text_buffer.ch_ptr[++dict->text_buffer.count] = '\0';
	}
	text = dict->text_buffer.ch_ptr;

	dict->entries = malloc(entries_size * sizeof(char *));
	if (!dict->entries) err("failed to allocate dictionary '%s' entries: %s\n", dict_filename, strerror(errno));
	dict->num_entries = 0;

	while (*text) {
		if (dict->num_entries >= entries_size) {
			entries_size *= 2;
			dict->entries = realloc(dict->entries, entries_size * sizeof(char *));
			if (!dict->entries) err("failed to reallocate dictionary '%s' entries: %s\n", dict_filename, strerror(errno));
			//printf("dictionary '%s' resized to %d\n", dict_filename, entries_size);
		}
		dict->entries[dict->num_entries++] = text;

		do {
			//*text = tolower(*text);
		} while (*text++ != ':');
		*(text-1) = '\0';

		while (*text++ != '\n'); // todo: check if it is less then dictionary limit
	}

	dict->filename = dict_filename;
	dict->input_line_number = 0;
	dict->matched = dict->selected = dict->expanded = (struct EIL) { 0 };
	dict->selected_expected_line = dict->matched.entry_index = dict->selected.entry_index = dict->expanded.entry_index = -1;
	dict->first_line_marker = NULL;
	dict->made_a_move = true;
	dict->first_line_number = -1;
	printd("dictionary '%s' has %d entries\n", dict->filename, dict->num_entries);
}


void font_from_ttf_file (Font *font, Screen *screen, const char *ttf_filename, int size, int squeezing, FontFlags flags) {
	int result;
	float squeeze = 0.01f * (squeezing + ((flags & MONOSPACED) ? 10 : 0));

	font->flags = flags;
	font->ttf_filename = ttf_filename;
	font->ttf_buffer = read_file(NULL, "fonts/", ttf_filename);
	if (font->ttf_buffer.count == 0) err("failed to read ttf file '%s': %s\n", ttf_filename, strerror(errno));

	font->bitmap = (Bitmap) {
		.x = 1,
		.y = 1,
		.width = INITIAL_BITMAP_WIDTH * screen->high_dpi_scaling.x,
		.height = INITIAL_BITMAP_HEIGHT * screen->high_dpi_scaling.y,
	};
	font->bitmap.data = malloc(font->bitmap.width * font->bitmap.height * sizeof(uint8_t));
	if (!font->bitmap.data) err("failed to allocate bitmap: %s\n", strerror(errno));
	memset(font->bitmap.data, 0, font->bitmap.width * font->bitmap.height);

	if (~font->flags & MONOSPACED) {
		// stb truetype
		font->info.userdata = NULL;
		result = stbtt_InitFont(&font->info, font->ttf_buffer.u8_ptr, stbtt_GetFontOffsetForIndex(font->ttf_buffer.u8_ptr, 0));
		if (!result) err("failed to get info from font '%s'\n", ttf_filename);
	} else {
		font->info = (stbtt_fontinfo) { 0 };
	}

	// freetype
	result = FT_New_Memory_Face(ttf_library, font->ttf_buffer.u8_ptr, font->ttf_buffer.count, 0, &font->face);
	if (result != 0) err("failed to load font '%s'\n", font->ttf_filename);
	//printf("hinter = %ld\n", font->face->face_flags & FT_FACE_FLAG_HINTER);

	font->baked_chars = (Buffer){ .count = 0, .size = INITIAL_BAKED_BUFFER_SIZE };
	font->baked_chars.baked_ptr = malloc(font->baked_chars.size * sizeof(BakedChar));
	if (!font->baked_chars.baked_ptr) err("failed to allocate baked chars for font '%s': %s\n", ttf_filename, strerror(errno));
	//printf("0: baked_buffer->ptr = %p\n", font->baked_chars.ptr);
	font->baked_tofu_index = TOFU_NOT_BAKED_YET;

	if (font->flags & MONOSPACED) {
		font->kerning = (Buffer){ .kern_ptr = NULL, .count = 0, .size = 0 };
	} else {
		font->kerning = (Buffer){ .count = 0, .size = INITIAL_KERNING_BUFFER_SIZE };
		font->kerning.kern_ptr = malloc(font->kerning.size * sizeof(Kerning));
		//printf("0: kerning->ptr = %p\n", font->kerning.ptr);
		if (!font->kerning.kern_ptr) err("failed to allocate kerning for font '%s': %s\n", ttf_filename, strerror(errno));
	}

	font->size = size;
	FT_Set_Pixel_Sizes(font->face, screen->high_dpi_scaling.x * font->size * squeeze, screen->high_dpi_scaling.y * font->size);
	font->scaling.x = font->face->size->metrics.x_scale;
	font->scaling.y = font->face->size->metrics.y_scale;

	//int ascent, descent, line_gap;
	//stbtt_GetFontVMetrics(&font->info, &ascent, &descent, &line_gap);
	font->ascent = font->face->size->metrics.ascender / 64.0f;
	font->descent = font->face->size->metrics.descender / 64.0f;
	//font->line_gap = 0;
	//printf("%s: ascent = %g, descent = %g\n", font->ttf_filename, font->ascent, font->descent);

	font->texture = NULL;
}


Buffer read_file (Buffer *buffer, const char *prefix, const char *filename) {
	char name_buffer[256]; // foldername + filename
	char *ptr = name_buffer, *temp_ptr;
	int64_t file_size;
	SDL_RWops *file;
	Buffer new_buffer;

	if (buffer == NULL) {
		new_buffer = (Buffer){};
		buffer = &new_buffer;
	}
	// append filename to prefix
	if (prefix) {
		while ((*ptr++ = *prefix++));
		ptr--;
	}
	while ((*ptr++ = *filename++));

	file = SDL_RWFromFile(name_buffer, "r");
	if (!file) goto file_open_error;
	file_size = SDL_RWsize(file);

	if (file_size < 0) { // could not get file size
		if (buffer->size == 0) {
			buffer->ch_ptr = malloc(1024);
			if (!buffer->ch_ptr) goto buffer_realloc_error;
			buffer->size = 1024;
		}
		buffer->count = SDL_RWread(file, buffer->ch_ptr, sizeof(char), buffer->size);
		while (buffer->count == buffer->size) {
			char *temp_buffer = realloc(buffer->ch_ptr, buffer->size * 2);
			if (!temp_buffer) goto buffer_realloc_error;
			*buffer = (Buffer){ .ch_ptr = temp_buffer, .count = buffer->count, .size = buffer->size * 2 };
			temp_ptr = buffer->ch_ptr + buffer->count;
			buffer->count += SDL_RWread(file, temp_ptr, sizeof(char), buffer->size/2);
		}
		if (buffer->size - buffer->count < 2) {
			char *temp_buffer = realloc(buffer->ch_ptr, buffer->count + 2);
			if (!temp_buffer) goto buffer_realloc_error;
			*buffer = (Buffer){ .ch_ptr = temp_buffer, .count = buffer->count, .size = buffer->count + 2 };
		}
	} else { // got file size
		if (file_size > buffer->size) {
			char *temp_buffer = realloc(buffer->ch_ptr, file_size + 2);
			if (!temp_buffer) goto buffer_realloc_error;
			*buffer = (Buffer){ .ch_ptr = temp_buffer, .count = buffer->count, .size = file_size + 2 };
		}
		buffer->count = SDL_RWread(file, buffer->ch_ptr, sizeof(char), buffer->size);
	}

	SDL_RWclose(file);
	buffer->ch_ptr[buffer->count] = '\0';
	return *buffer;

buffer_realloc_error:
	SDL_RWclose(file);
file_open_error:
	buffer->count = 0;
	return *buffer;
}


uint32_t utf8_to_unicode (char **string) {
	uint8_t *s = (uint8_t *)*string;
	uint32_t unicode;

	if ((*s & 0x80) == 0x00) {
		unicode = *s;
		*string += 1;
	} else if ((*s & 0xe0) == 0xc0) {
		unicode = ((*s & 0x3f) << 6) | (*(s+1) & 0x7f);
		*string += 2;
	} else if ((*s & 0xf0) == 0xe0) {
		unicode = ((*s & 0x1f) << 12) | ((*(s+1) & 0x7f) << 6) | (*(s+2) & 0x7f);
		*string += 3;
	} else if ((*s & 0xf8) == 0xf0) {
		unicode = ((*s & 0x0f) << 18) | ((*(s+1) & 0x7f) << 12) | ((*(s+2) & 0x7f) << 6) | (*(s+3) & 0x7f);
		*string += 4;
	}  else {
		unicode = 1;
		*string += 1;
		fprintf(stderr, "INCORRECT UTF-8: 0x%02x\n", *s);
	}

	return unicode;
}


float kern_advance (Font *font, int ch0_index, int ch1_index) {
	if (font->flags & MONOSPACED) return 0.0f;
	int left, right, middle;
	uint32_t char_index, adv_index, unicode;

	if (font->baked_chars.baked_ptr[ch0_index].kern_index > font->baked_chars.baked_ptr[ch1_index].kern_index) {
		char_index = ch0_index;
		adv_index = 0;
		unicode = font->baked_chars.baked_ptr[ch1_index].unicode;
	} else {
		char_index = ch1_index;
		adv_index = 1;
		unicode = font->baked_chars.baked_ptr[ch0_index].unicode;
	}
	left = font->baked_chars.baked_ptr[char_index].kern_index;
	right = left + font->baked_chars.baked_ptr[char_index].num_kerns;
	if (left == right) return 0.0f;

	while (left <= right) {
		middle = (left + right) / 2;
		if (unicode > font->kerning.kern_ptr[middle].unicode) {
			left = middle + 1;
		} else if (unicode < font->kerning.kern_ptr[middle].unicode) {
			right = middle - 1;
		} else {
			float kerning = font->kerning.kern_ptr[middle].advance[adv_index] * font->scaling.x / 4000000.0f;
			//printf("kerning = %f\n", kerning);
			return kerning;
		}
	}

	return 0.0f;
}


// @batch this to draw font texture at once?
void bake_char_glyph (Font *font, uint32_t unicode, int char_index, bool rebake) {
	int glyph_index, kern_index, num_kerns, width, height, result;
	BakedChar char_to_bake;

	if (rebake) {
		glyph_index = font->baked_chars.baked_ptr[char_index].glyph_index;
		kern_index = font->baked_chars.baked_ptr[char_index].kern_index;
		num_kerns = font->baked_chars.baked_ptr[char_index].num_kerns;
	} else {
		glyph_index = FT_Get_Char_Index(font->face, unicode);
		kern_index = font->kerning.count;
		num_kerns = 0;
	}

	if (glyph_index || font->baked_tofu_index == TOFU_NOT_BAKED_YET) {
		//stbtt_GetGlyphHMetrics(&font->info, glyph_index, &xadvance, &lsb);
		//result = stbtt_GetGlyphBox(&font->info, glyph_index, &x0, &y0, &x1, &y1);
		result = FT_Load_Glyph(font->face, glyph_index, FT_LOAD_RENDER | FT_LOAD_FORCE_AUTOHINT);
		if (result != 0) err("failed to load glyph %d from unicode %u\n", glyph_index, unicode);

		width = font->face->glyph->bitmap.width;
		height = font->face->glyph->bitmap.rows;

		if (font->bitmap.x + width + 1 >= font->bitmap.width) {
			font->bitmap.y = font->bitmap.bottom_y;
			font->bitmap.x = 1;
		}
		if (font->bitmap.y + height + 1 >= font->bitmap.height) {
			bool width_resize = false;
			font->bitmap.height *= 2;
			if (font->bitmap.height > max_texture_height) {
				font->bitmap.height = max_texture_height;
				font->bitmap.width *= 2;
				width_resize = true;
			}
			font->bitmap.data = realloc(font->bitmap.data, font->bitmap.width * font->bitmap.height * sizeof(uint8_t));
			if (!font->bitmap.data) err("failed to reallocate font '%s' bitmap to height %u: %s\n", font->ttf_filename, font->bitmap.height, strerror(errno));

			if (width_resize) {
				int range = rebake ? char_index : font->baked_chars.count;
				// erase bitmap
				memset(font->bitmap.data, 0, font->bitmap.height * font->bitmap.width * sizeof(uint8_t));
				font->bitmap.x = font->bitmap.y = 1;
				font->bitmap.last_uploaded_x = font->bitmap.last_uploaded_y = 0;
				font->bitmap.bottom_y = 0;

				// rebake the old chars
				font->baked_tofu_index = TOFU_NOT_BAKED_YET;
				for (int i = 0; i < range; i++) {
					bake_char_glyph(font, font->baked_chars.baked_ptr[i].unicode, i, true);
				}
			} else {
				memset(font->bitmap.data + font->bitmap.width * font->bitmap.bottom_y, 0, font->bitmap.width * (font->bitmap.height - font->bitmap.bottom_y));
			}
			//printf("font '%s' bitmap resized to %u height\n", font->ttf_filename, font->bitmap.height);
			SDL_DestroyTexture(font->texture);
			font->texture = NULL;
			//@check if the values above need offset correction : apparently not
		}
		//stbtt_MakeGlyphBitmap(&font->info, font->bitmap.data + font->bitmap.width * font->bitmap.y + font->bitmap.x, width, height, font->bitmap.width, font->scaling.x, font->scaling.y, glyph_index);
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				*(font->bitmap.data + font->bitmap.width * (font->bitmap.y + y) + font->bitmap.x + x) = *(font->face->glyph->bitmap.buffer + font->face->glyph->bitmap.pitch * y + x);
			}
		}

		char_to_bake = (BakedChar) {
			.unicode = unicode,
			.glyph_index = glyph_index,
			.kern_index = kern_index,
			.num_kerns = num_kerns,
			.x = font->bitmap.x,
			.y = font->bitmap.y,
			.xadvance = font->face->glyph->advance.x / 64.0f - font->face->glyph->bitmap_left, //(xadvance - lsb) * font->scaling.x,
			.xoff = font->face->glyph->bitmap_left, //lsb * font->scaling.x, // left-side bearing == x0
			.yoff = -font->face->glyph->bitmap_top, //floorf(y0 * font->scaling.y),
			.lsb_delta = font->face->glyph->lsb_delta,
			.rsb_delta = font->face->glyph->rsb_delta,
			.width = width,
			.height = height,
		};
		//printf("lsb = %d, rsb = %d\n", char_to_bake.lsb_delta, char_to_bake.rsb_delta);

		if (!glyph_index) {
			font->baked_tofu_index = char_index;
		}

		// advance bitmap's next char location
		font->bitmap.x += char_to_bake.width + 1;
		if (font->bitmap.y + char_to_bake.height + 1 > font->bitmap.bottom_y) {
			font->bitmap.bottom_y = font->bitmap.y + char_to_bake.height + 1;
		}
	} else {
		char_to_bake = font->baked_chars.baked_ptr[font->baked_tofu_index];
		char_to_bake.unicode = unicode;
		char_to_bake.kern_index = kern_index;
	}

	// insert new baked char in baked_chars
	if (!rebake) {
		resize_buffer_if_needed(&font->baked_chars, sizeof(BakedChar));
		//printf("count = %u, index = %d, size = %u\n", font->baked_chars.count, char_index, font->baked_chars.size);
		for (int i = font->baked_chars.count; i > char_index; i--) {
			font->baked_chars.baked_ptr[i] = font->baked_chars.baked_ptr[i-1];
		}
		font->baked_chars.count++;
	}
	font->baked_chars.baked_ptr[char_index] = char_to_bake;
	//printf("%p: ", font->baked_chars.baked_ptr);
	//for (int i = 0; i < font->baked_chars.size; i++) {
		//printf("%d, ", font->baked_chars.baked_ptr[i].unicode);
	//}
	//printf("\n");

	// new baked char kerning
	if (~font->flags & MONOSPACED && char_to_bake.glyph_index != 0 && !rebake) {
		int prev_curr_adv, curr_prev_adv;

		for (int i = 0; i < font->baked_chars.count; i++) {
			prev_curr_adv = stbtt_GetGlyphKernAdvance(&font->info, font->baked_chars.baked_ptr[i].glyph_index, char_to_bake.glyph_index);
			curr_prev_adv = stbtt_GetGlyphKernAdvance(&font->info, char_to_bake.glyph_index, font->baked_chars.baked_ptr[i].glyph_index);
			//result = FT_Get_Kerning(font->face, font->baked_chars.baked_ptr[i].glyph_index, char_to_bake.glyph_index, FT_KERNING_DEFAULT, &kerning);
			//if (result != 0) err("failed to retrive kerning\n");
			//prev_curr_adv = kerning.x;
			//printf("k1 = %ld %ld\n", kerning.x, kerning.y);
			//result = FT_Get_Kerning(font->face, char_to_bake.glyph_index, font->baked_chars.baked_ptr[i].glyph_index, FT_KERNING_DEFAULT, &kerning);
			//if (result != 0) err("failed to retrive kerning\n");
			//curr_prev_adv = kerning.x;
			//printf("k2 = %ld %ld\n", kerning.x, kerning.y);

			if (prev_curr_adv || curr_prev_adv) {
				resize_buffer_if_needed(&font->kerning, sizeof(Kerning));
				font->kerning.kern_ptr[font->kerning.count] = (Kerning) {
					.unicode = font->baked_chars.baked_ptr[i].unicode,
					.advance[0] = prev_curr_adv,
					.advance[1] = curr_prev_adv,
				};
				font->baked_chars.baked_ptr[char_index].num_kerns++;
				font->kerning.count++;
				//printf("kerning = %d, %d\n", prev_curr_adv, curr_prev_adv);
			}
		}
		//printf("font '%s' kerning count = %u\n", font->ttf_filename, font->kerning.count);
	}

}


void resize_buffer_if_needed (Buffer *buffer, uint32_t size) {
	if (buffer->count >= buffer->size) {
		buffer->size *= 2;
		//printf("1: buffer->ptr = %p\n", buffer->ptr);
		buffer->ptr = realloc(buffer->ptr, buffer->size * size);
		//printf("2: buffer->ptr = %p\n", buffer->ptr);
		if (!buffer->ptr) err("failed to reallocate buffer to size %u: %s\n", buffer->size * size, strerror(errno));
		//printf("buffer resized to %u * %u\n", buffer->size, size);
	}
}


int find_baked_char_index (BakedChar *baked_chars, int count, uint32_t unicode) {
	int left = 0, middle = 0, right = count-1;
	while (left <= right) {
		middle = (left + right) / 2;
		if (unicode > baked_chars[middle].unicode) {
			left = middle + 1;
		} else if (unicode < baked_chars[middle].unicode) {
			right = middle - 1;
		} else {
			return middle;
		}
	}
	return -max(right, left);
}


void render_font_texture (Font *font, Screen *screen, bool more_than_one_glyph) {
	SDL_Color *rgba_pixels;
	SDL_Rect change_rect;
	int result, pitch;

	//printf("x = %u | %u, y = %u | %u\n", font->bitmap.x, font->bitmap.last_uploaded_x, font->bitmap.y, font->bitmap.last_uploaded_y);
	if (!font->texture) {
		font->texture = SDL_CreateTexture(screen->renderer, SDL_PIXELFORMAT_RGBA32, SDL_TEXTUREACCESS_STREAMING, font->bitmap.width, font->bitmap.height);
		if (!font->texture) err("failed to create font '%s' texture: %s\n", font->ttf_filename, SDL_GetError());
		SDL_SetTextureBlendMode(font->texture, SDL_BLENDMODE_BLEND);

		font->bitmap.last_uploaded_x = 1;
		font->bitmap.last_uploaded_y = 1;
		more_than_one_glyph = true;
	}

	if (font->bitmap.y > font->bitmap.last_uploaded_y) {
		if (more_than_one_glyph) {
			change_rect = (SDL_Rect) {
				.x = 0,
				.y = font->bitmap.last_uploaded_y,
				.w = font->bitmap.width,
				.h = font->bitmap.bottom_y - font->bitmap.last_uploaded_y,
			};
		} else {
			change_rect = (SDL_Rect) {
				.x = 0,
				.y = font->bitmap.y,
				.w = font->bitmap.x,
				.h = font->bitmap.bottom_y - font->bitmap.y,
			};
		}
	} else if (font->bitmap.x > font->bitmap.last_uploaded_x) {
		change_rect = (SDL_Rect) {
			.x = font->bitmap.last_uploaded_x,
			.y = font->bitmap.last_uploaded_y,
			.w = font->bitmap.x - font->bitmap.last_uploaded_x,
			.h = font->bitmap.bottom_y - font->bitmap.y,
		};
	} else { // no new glyph drawn on bitmap to repass to texture
		return;
	}

	result = SDL_LockTexture(font->texture, &change_rect, (void **)&rgba_pixels, &pitch);
	if (result < 0) err("failed to access font '%s' texture: %s\n", font->ttf_filename, SDL_GetError());

	// @todo replace uint32_t by SDL_Color?
	//uint32_t color = screen->foreground_color.r | screen->foreground_color.g << 8 | screen->foreground_color.b << 16;
	for (int y = 0; y < change_rect.h; y++) {
		uint8_t *bm = &font->bitmap.data[change_rect.x + (change_rect.y + y) * font->bitmap.width];
		SDL_Color *px = &rgba_pixels[y * pitch/sizeof(SDL_Color)];

		for (int x = 0; x < change_rect.w; x++) {
			// @find a way to change colors
			px[x] = (SDL_Color){ .r = 0xff, .g = 0xff, .b = 0xff, .a = bm[x] };
		}
	}
	//printf("texture: x = %u, y = %u, w = %u, h = %u\n", change_rect.x, change_rect.y, change_rect.w, change_rect.h);
	font->bitmap.last_uploaded_x = font->bitmap.x;
	font->bitmap.last_uploaded_y = font->bitmap.y;
	SDL_UnlockTexture(font->texture);

	SDL_SetTextureColorMod(font->texture, screen->foreground_color.r, screen->foreground_color.g, screen->foreground_color.b);
}


void resize_font (Font *font, Screen *screen, int new_size, int new_squeezing) {
	float squeeze = 0.01f * (new_squeezing + ((font->flags & MONOSPACED) ? 10 : 0)); // percentage
	font->size = new_size;
	FT_Set_Pixel_Sizes(font->face, max(screen->high_dpi_scaling.x * font->size * squeeze, 1), screen->high_dpi_scaling.y * font->size);
	font->scaling.x = font->face->size->metrics.x_scale;
	font->scaling.y = font->face->size->metrics.y_scale;
	//font->scaling.x = stbtt_ScaleForPixelHeight(&font->info, screen->high_dpi_scaling.x * font->size * squeeze);
	//font->scaling.y = stbtt_ScaleForPixelHeight(&font->info, screen->high_dpi_scaling.y * font->size);
	font->ascent = font->face->size->metrics.ascender / 64.0f;
	font->descent = font->face->size->metrics.descender / 64.0f;
	//font->line_gap = 0;

	//int ascent, descent, line_gap;
	//stbtt_GetFontVMetrics(&font->info, &ascent, &descent, &line_gap);
	//font->ascent = ascent * font->scaling.y;
	//font->descent = descent * font->scaling.y;
	//font->line_gap = line_gap * font->scaling.y;

	// erase bitmap
	memset(font->bitmap.data, 0, font->bitmap.height * font->bitmap.width * sizeof(uint8_t));
	font->bitmap.x = font->bitmap.y = 1;
	font->bitmap.last_uploaded_x = font->bitmap.last_uploaded_y = 0;
	font->bitmap.bottom_y = 0;

	// rebake the old chars
	font->baked_tofu_index = TOFU_NOT_BAKED_YET;
	for (uint32_t i = 0; i < font->baked_chars.count; i++) {
		bake_char_glyph(font, font->baked_chars.baked_ptr[i].unicode, i, true);
	}

	// redraw texture
	render_font_texture(font, screen, true);
}


int search_for_entry_index (Dictionary *dict, int (*strcmp)(char*,char*), char *entry) {
	int left = 0, middle = 0, right = dict->num_entries-1, result;

	while (left <= right) {
		middle = (left + right) / 2;
		result = strcmp(entry, dict->entries[middle]);
		if (result > 0) {
			left = middle + 1;
		} else if (result < 0) {
			right = middle - 1;
		} else {
			return middle;
		}
	}

	return max(left, right);
}


int latin_strcmp (char *str1, char *str2) {
	uint8_t *s1 = (uint8_t *)str1;
	uint8_t *s2 = (uint8_t *)str2;
	int c1, c2;
	do {
		c1 = (*s1 >= 'A' && *s1 <= 'Z') ? *s1 + ('a'-'A') : *s1;
		c2 = (*s2 >= 'A' && *s2 <= 'Z') ? *s2 + ('a'-'A') : *s2;
		s1++; s2++;
	} while (c1 == c2 && c1 && c2);

	return c1 - c2;
}


char *noto_font_filename (FontWeight weight, FontFlags flags) {
	static char filenames[3][64];
	int name_index = (flags & BOLDER) ? 2 : ((flags & ITALIC) ? 1 : 0);
	char *weight_name = NULL;

	switch (weight) {
	case THIN:
		weight_name = "Thin";
		break;
	case EXTRA_LIGHT:
		weight_name = "ExtraLight";
		break;
	case LIGHT:
		weight_name = "Light";
		break;
	case REGULAR:
		weight_name = (~flags & ITALIC) ? "Regular" : "";
		break;
	case MEDIUM:
		weight_name = "Medium";
		break;
	case SEMI_BOLD:
		weight_name = "SemiBold";
		break;
	case BOLD:
		weight_name = "Bold";
		break;
	case BLACK:
		weight_name = "Black";
		break;
	default:
		err("invalid weight value %d for font\n", weight);
		break;
	}

	sprintf(filenames[name_index], "NotoSerif-%s%s.ttf", weight_name, (flags & ITALIC) ? "Italic" : "");
	return &filenames[name_index][0];
}


void replace_font (Font *font, Screen *screen, const char *ttf_filename, int size, int squeezing) {
	int result;
	float squeeze = 0.01f * (squeezing + ((font->flags & MONOSPACED) ? 10 : 0));

	font->ttf_filename = ttf_filename;
	font->ttf_buffer = read_file(&font->ttf_buffer, "fonts/", ttf_filename);
	if (font->ttf_buffer.count == 0) err("failed to read ttf file '%s': %s\n", ttf_filename, strerror(errno));

	memset(font->bitmap.data, 0, font->bitmap.height * font->bitmap.width * sizeof(uint8_t));
	font->bitmap.x = font->bitmap.y = 1;
	font->bitmap.last_uploaded_x = font->bitmap.last_uploaded_y = 0;
	font->bitmap.bottom_y = 0;

	if (~font->flags & MONOSPACED) {
		font->info.userdata = NULL;
		result = stbtt_InitFont(&font->info, font->ttf_buffer.u8_ptr, stbtt_GetFontOffsetForIndex(font->ttf_buffer.u8_ptr, 0));
		if (!result) err("failed to retrieve font info for '%s'\n", ttf_filename);
	}

	FT_Done_Face(font->face);
	result = FT_New_Memory_Face(ttf_library, font->ttf_buffer.u8_ptr, font->ttf_buffer.count, 0, &font->face);
	if (result != 0) err("failed to load font '%s'\n", ttf_filename);

	font->baked_chars.count = 0;
	font->baked_tofu_index = TOFU_NOT_BAKED_YET;
	font->kerning.count = 0;

	font->size = size;
	FT_Set_Pixel_Sizes(font->face, screen->high_dpi_scaling.x * font->size * squeeze, screen->high_dpi_scaling.y * font->size);
	font->scaling.x = font->face->size->metrics.x_scale;
	font->scaling.y = font->face->size->metrics.y_scale;
	//font->scaling.x = stbtt_ScaleForPixelHeight(&font->info, screen->high_dpi_scaling.x * font->size * squeeze);
	//font->scaling.y = stbtt_ScaleForPixelHeight(&font->info, screen->high_dpi_scaling.y * font->size);

//	int ascent, descent, line_gap;
//	stbtt_GetFontVMetrics(&font->info, &ascent, &descent, &line_gap);
//	font->ascent = ascent * font->scaling.y;
//	font->descent = descent * font->scaling.y;
//	font->line_gap = line_gap * font->scaling.y;
	font->ascent = font->face->size->metrics.ascender / 64.0f;
	font->descent = font->face->size->metrics.descender / 64.0f;
	//font->line_gap = 0;
	//printf("%s: ascent = %g, descent = %g\n", font->ttf_filename, font->ascent, font->descent);

}


char *previous_char (char *current, int num_backwards) {
	for (int i = 0; i < num_backwards; i++) {
		while ((*--current & 0xc0) == 0x80);
		while (*current == '`' || *current == '*') current--;
	}
	return current;
}


void render_dictionary_entries (Dictionary *dict, Fonts *fonts, Screen *screen) {
	int expanded_entry_upper_border, expanded_entry_lower_border;
	if (dict->expanded.entry_index > -1 && dict->expanded.line_start < screen->num_lines_on_screen && dict->expanded.line_end >= 0) {
		 int thickness = (fonts->regular.ascent - fonts->regular.descent + screen->line_gap + 0.5f);
		 expanded_entry_upper_border = thickness/2;
		 expanded_entry_lower_border = thickness/2 + (thickness & 1);
	} else {
		expanded_entry_upper_border = expanded_entry_lower_border = 0;
	}

	int baseline = (screen->border_height < 0 ? 3 : 1) * screen->border_height/3 + fonts->regular.ascent;
	float prev_char_xadvance = 0.0f;
	uint32_t unicode;
	int curr_char_index = 0, prev_char_index = 0, prev_char_unicode = 0;
	BakedChar *bcp;
	SDL_Rect texture_rect;
	Font *font = &fonts->regular;
	char *ptr = NULL;
	int new_chars_count[arrsz(fonts->array)] = {0};

	int entry_index;
	if (dict->selected.entry_index < 0) {
		int expansion_displacement = (dict->expanded.entry_index >= 0) ? max(min(dict->expanded.line_end, screen->num_lines_on_screen) - max(dict->expanded.line_start, 0), 0) : 0;
		entry_index = dict->matched.entry_index - dict->matched.line_start + ((dict->matched.line_start > dict->input_line_number && dict->input_line_number >= 0) ? 1 : 0) + ((dict->matched.line_start > dict->expanded.line_start) ? expansion_displacement : 0);
		//printf("entry index = %d = %d + %d + %d\n", entry_index, dict->selected.entry_index - dict->selected.line_start, ((dict->selected.line_start > dict->input_line_number && dict->input_line_number >= 0) ? 1 : 0), ((dict->selected.line_start > dict->expanded.line_start) ? expansion_displacement : 0));
	} else if (dict->selected.line_start < 0) {
		entry_index = dict->selected.entry_index;
	} else {
		int expansion_displacement = (dict->expanded.entry_index >= 0) ? max(min(dict->expanded.line_end, screen->num_lines_on_screen) - max(dict->expanded.line_start, 0), 0) : 0;
		entry_index = dict->selected.entry_index - dict->selected.line_start + ((dict->selected.line_start > dict->input_line_number && dict->input_line_number >= 0) ? 1 : 0) + ((dict->selected.line_start > dict->expanded.line_start) ? expansion_displacement : 0);
		//printf("entry index = %d = %d + %d + %d\n", entry_index, dict->selected.entry_index - dict->selected.line_start, ((dict->selected.line_start > dict->input_line_number && dict->input_line_number >= 0) ? 1 : 0), ((dict->selected.line_start > dict->expanded.line_start) ? expansion_displacement : 0));
	}


	int line_number = (dict->expanded.entry_index >= 0 && dict->expanded.line_end >= 0) ? min(dict->expanded.line_start, 0) : 0;
	baseline += line_number * (int)(fonts->regular.ascent - fonts->regular.descent + screen->line_gap + 0.5f);
	SDL_Rect screen_rect = { .x = screen->border_width + screen->excess_width, .y = baseline };

	Font *endline_font = font;

	for ( ; line_number < screen->num_lines_on_screen && (entry_index <= dict->num_entries); line_number++) {
		if (line_number == 0) baseline = (screen->border_height < 0 ? 3 : 1) * screen->border_height/3 + fonts->regular.ascent + ((dict->expanded.line_start < 0 && dict->expanded.line_end >= 0) ? expanded_entry_upper_border : 0)/* - expanded_entry_upper_border*/;
		// to better demark selected entry
		if (dict->expanded.entry_index >= 0) {
			if (dict->expanded.line_start == line_number) {
				baseline += expanded_entry_upper_border;
			}
			if (dict->expanded.line_end == line_number) {
				baseline += expanded_entry_lower_border;
				continue;
			}
		}
		// this part is handled by render_text_input
		if (line_number == dict->input_line_number) {
			baseline += fonts->regular.ascent - fonts->regular.descent + screen->line_gap + 0.5f;
			continue;
		}

		if (entry_index >= 0) {
			if (entry_index-1 == dict->expanded.entry_index && ptr != NULL && *ptr != '\n') {
				// ident lines of expanded entry from the second one onward
				screen_rect.x += fonts->regular.baked_chars.baked_ptr['m'].xadvance + 0.5f;
				//printf("ptr = \"%c%c%c%c\"\n", *(ptr-2), *(ptr-1), *ptr, *(ptr+1));
				//printf(">>> line number = %d | %d %d %d\n", line_number, *(ptr-1), *(ptr), *(ptr+1));
			} else if (entry_index < dict->num_entries) {
				font = &fonts->regular;
				ptr = dict->entries[entry_index++];
				while (*ptr++);
				ptr++;
			} else { // to allow entry_index == dict->num_entries, otherwise...
				break; // ... the last entry shows only first line when expanded
			}
		} else {
			baseline += font->ascent - font->descent + screen->line_gap + 0.5f;
			entry_index++;
			continue;
		}

		char *line_start = ptr, *line_end = NULL;
		Font *font_start = font, *prev_font = font;
		int num_spaces = -1, line_length = 0, space_iterator;
		int screen_rect_start = screen_rect.x;
		enum { FITTING_TEST, ACTUAL_PLACEMENT };
		bool text_adjust = false;
		bool reached_end_of_line;

		for (int it_type = FITTING_TEST; it_type <= ACTUAL_PLACEMENT; it_type++) {
			//if (line_number == 1 && it_type == FITTING_TEST) printf("f ");
			//if (line_number == 1 && it_type == ACTUAL_PLACEMENT) printf("p ");
			// there are only simulations before the start of the screen lines
			if (it_type == ACTUAL_PLACEMENT && line_number < 0) break;

			ptr = line_start;
			font = font_start;
			prev_font = font;
			screen_rect.x = screen_rect_start;
			prev_char_index = 0;
			prev_char_unicode = 0;
			prev_char_xadvance = 0;
			space_iterator = 0;
			reached_end_of_line = false;

			while (*ptr != '\n' && !reached_end_of_line) { // and while there is space in line
				// many lines in this next loop are just to correct some mistakes in the dictionaries
				while (*ptr == '*' || *ptr == '`') {
					if (*ptr == '*') { // make italic
						int surrounded = 0;
						if (*(ptr-1) == ' ') surrounded++;
						font = (font != &fonts->italic) ? &fonts->italic : &fonts->regular;
						if (*++ptr == '*') ptr++;
						if (*ptr == ' ') surrounded++;
						if (surrounded == 2) {
							//num_spaces--;
							ptr++;
						}
					} else if (*ptr == '`') { // make bold
						if (*(ptr-1) == ' ' && *(ptr+1) == ' ') {
							num_spaces--;
							ptr++;
						}
						font = (font != &fonts->bold) ? &fonts->bold : &fonts->regular;
						ptr++;
					}
				}
				if (*ptr == '\n') break;
				unicode = utf8_to_unicode(&ptr);
				//int boi = 0;

				if (unicode < 128) {
					curr_char_index = unicode;
				} else {
					curr_char_index = find_baked_char_index(font->baked_chars.baked_ptr + 127, font->baked_chars.count - 127, unicode);
					if (curr_char_index < 0) {
						curr_char_index = 127 - curr_char_index;
						bake_char_glyph(font, unicode, curr_char_index, false);
						new_chars_count[((uintptr_t)font - (uintptr_t)fonts->array) / sizeof(Font)]++;
						//printf("new char: unicode = %d, index = %u; char count = %u, font kerning count = %u\n", unicode, curr_char_index, font->baked_chars.count, font->kerning.count);
						//boi = prev_char_index;
						// get updated index
						prev_char_index = prev_char_unicode < 128 ? prev_char_unicode : 127 + find_baked_char_index(prev_font->baked_chars.baked_ptr + 127, prev_font->baked_chars.count - 127, prev_char_unicode);
					} else {
						curr_char_index += 127;
					}
				}
				bcp = &font->baked_chars.baked_ptr[curr_char_index];

				if (it_type == FITTING_TEST) {
					if (unicode == ' ' && prev_char_index != ' ') {
						// a space means a possible location to break line
						num_spaces++;
						line_end = previous_char(ptr, 1);
						line_length = screen_rect.x + prev_font->baked_chars.baked_ptr[prev_char_index].width + 0.5f;
						endline_font = font;
					}
					//if (line_number == 1 && prev_char_unicode == 964 && unicode == 972) { printf("here!\n"); }
					if (unicode < 0x0300 || unicode > 0x036f) { // not a combining diacritic
						int xadvance = prev_char_xadvance + bcp->xoff + (prev_font == font ? kern_advance(font, prev_char_index, curr_char_index) : 0.0f) + 0.5f;
						//if (line_number == 1) printf("%d:%d:%d:%d:%d:%d:%d:%g  ", boi, prev_char_index, prev_char_unicode, curr_char_index, unicode, font->baked_chars.baked_ptr[prev_char_index].kern_index, font->baked_chars.baked_ptr[curr_char_index].kern_index, (prev_font == font ? kern_advance(font, prev_char_index, curr_char_index) : 0.0f));
						if (screen_rect.x + xadvance + bcp->width >= screen->width - (screen->border_width + screen->excess_width)) { // no more line space
							if (line_end != NULL) {
								ptr = line_end + 1;
							} else {
								line_end = previous_char(ptr, 1);
							}
							reached_end_of_line = true;
							font = endline_font;
							break;
						}

						screen_rect.x += xadvance;
					}

				} else { // it_type == ACTUAL_PLACEMENT
					if (ptr > line_end) {
						reached_end_of_line = true;
						break;
					}
					// draw glyph from texture to screen
					texture_rect = (SDL_Rect) {
						.x = bcp->x,
						.y = bcp->y,
						.w = bcp->width,
						.h = bcp->height,
					};

					if (unicode >= 0x0300 && unicode <= 0x036f) { // place the combining diacritic above the character
						SDL_Rect diacritic_screen_rect = {
							.x = screen_rect.x + bcp->xoff + texture_rect.w/* + (screen_rect.w - texture_rect.w)/2*/,
							.y = baseline + bcp->yoff + 0.5f,
							.w = texture_rect.w,
							.h = texture_rect.h,
						};
						switch (unicode) {
						case 0x0300:
						case 0x0301:
							diacritic_screen_rect.x = screen_rect.x + (screen_rect.w - texture_rect.w) / 2;
							diacritic_screen_rect.y = screen_rect.y - 2*texture_rect.h/3;
						default: break;
						}
						SDL_RenderCopy(screen->renderer, font->texture, &texture_rect, &diacritic_screen_rect);
					} else {
						screen_rect = (SDL_Rect) {
							.x = screen_rect.x + prev_char_xadvance + bcp->xoff + (prev_font == font ? kern_advance(font, prev_char_index, curr_char_index) : 0.0f) + 0.5f,
							.y = baseline + bcp->yoff + 0.5f,
							.w = texture_rect.w,
							.h = texture_rect.h,
						};
						//if (line_number == 1) printf("%s%d ", (unicode == ' ' ? "s" : ""), screen_rect.x);
						if (text_adjust && unicode == ' ' && prev_char_unicode != ' ') {
							int extra_space = space_iterator++ < (((screen->width - (screen->border_width + screen->excess_width)) - line_length) % num_spaces);
							screen_rect.x += ((screen->width - (screen->border_width + screen->excess_width)) - line_length) / num_spaces + extra_space;
						}
						SDL_RenderCopy(screen->renderer, font->texture, &texture_rect, &screen_rect);
					}
				}

				if (unicode < 0x0300 || unicode > 0x036f) { // keep the previous char's prev_* if this char was only a diacritic
					prev_char_xadvance = bcp->xadvance;
					prev_char_index = curr_char_index;
					prev_char_unicode = unicode;
					prev_font = font;
				}

			}

			/*
			if (it_type == ACTUAL_PLACEMENT && screen_rect.x + screen_rect.w > screen->width - (screen->border_width + screen->excess_width)) {
				// @debug
				printf("line %d length = %d, width = %d, written = %d, spaces = %d, ta = %d, %p : %p, %c, %c\n", -1, line_length, screen->width - (screen->border_width + screen->excess_width), screen_rect.x + screen_rect.w, num_spaces, text_adjust, line_end, ptr, *line_end, *ptr);
			}*/
			while (*ptr == ' ') ptr++;
			if (*ptr == '\r') ptr++;
			text_adjust = (*ptr != '\n');

			if (it_type == FITTING_TEST) {
				for (int i = 0; i < arrsz(fonts->array); i++) {
					if (new_chars_count[i]) {
						render_font_texture(&fonts->array[i], screen, new_chars_count[i] > 1);
						new_chars_count[i] = 0;
					}
				}

				if (*ptr == '\n') {
					line_end = ptr;
					endline_font = font;
				}
			}
			//if (line_number == 1) printf("| %d %d\n", num_spaces, line_length);
		}

		baseline += font->ascent - font->descent + screen->line_gap + 0.5f;
		screen_rect.x = screen->border_width + screen->excess_width;
	}
}


// @todo: also draw the cursor above input letters: done
bool draw_blinking_cursor (Cursor *cursor, Timer *timer, Screen *screen) {
	//static bool cursor_shown = false;
	bool show_cursor = !(((timer->now - timer->last_cursor_update)/500)%2);
	//printf("show curosr = %s : %u, %u\n", show_cursor ? "TRUE" : "FALSE", timer->now, timer->last_cursor_update);
	if (!show_cursor) return false;

	//if (timer->now == timer->last_cursor_update || show_cursor != cursor_shown) { // this is now checked outside this function
		//cursor_shown = show_cursor;
		SDL_Color fg = screen->foreground_color;
		SDL_Color bg = screen->background_color;
		SDL_SetRenderDrawColor(screen->renderer, fg.r, fg.g, fg.b, fg.a);
		SDL_RenderFillRect(screen->renderer, &cursor->slab_dst);
		SDL_SetRenderDrawColor(screen->renderer, bg.r, bg.g, bg.b, bg.a);

		if (cursor->glyph_index != BLANK_CURSOR) {
			SDL_RenderCopy(screen->renderer, cursor->texture, &cursor->char_src, &cursor->char_dst);
		}
		return true;
	//}
	// returns if the cursor blinked
	return false;
}

bool draw_selected_entry_marker (Dictionary *dict, Fonts *fonts, Screen *screen) {
	int expanded_entry_delta_border = 0;
	if (dict->expanded.entry_index > -1 && dict->expanded.line_start < screen->num_lines_on_screen && dict->expanded.line_end >= 0) {
		 int thickness = (fonts->regular.ascent - fonts->regular.descent + screen->line_gap + 0.5f);
		 if (dict->selected.entry_index == dict->expanded.entry_index) {
			 expanded_entry_delta_border = thickness/2;
		 }
	}

	if (dict->selected.entry_index >= 0) {
		int lines_spawn = dict->selected.line_end - dict->selected.line_start + (dict->selected.entry_index != dict->expanded.entry_index);
		//printf("lines spawn = %d\n", lines_spawn);
		SDL_Color fg = screen->foreground_color, bg = screen->background_color;
		SDL_SetRenderDrawColor(screen->renderer, fg.r, fg.g, fg.b, fg.a);
		SDL_Rect bold_rect = {
			.x = screen->excess_width,
			.y = (screen->border_height < 0 ? 3 : 1) * screen->border_height/3 + (fonts->regular.ascent - fonts->regular.descent + screen->line_gap) * dict->selected.line_start - screen->line_gap/2 + expanded_entry_delta_border + 0.5f,
			.w = screen->border_width / 2.0f + 0.5f,
			.h = (fonts->regular.ascent - fonts->regular.descent) + screen->line_gap + 0.5f,
		};
		/*
		if (dict->input_line_number >= 0 && dict->selected.line_start > dict->input_line_number) {
			bold_rect.y += (fonts->monospaced.ascent - fonts->monospaced.descent) - (fonts->regular.ascent - fonts->regular.descent) + 0.5f;
		}*/
		int thin_width = max(bold_rect.w * (0.22f + (fonts->weight - 3) * 0.07f) + 0.5f, 1);
		SDL_Rect thin_rect = {
			.x = screen->excess_width + bold_rect.w - thin_width,
			.y = bold_rect.y,
			.w = thin_width,
			.h = (fonts->regular.ascent - fonts->regular.descent) * (lines_spawn) + screen->line_gap * (lines_spawn - 1) + screen->line_gap + 0.5f,
		};
		if (dict->selected.entry_index != dict->expanded.entry_index) {
			SDL_Rect horizontal = { .x = bold_rect.x, .y = bold_rect.y, .w = screen->width - 2*screen->excess_width, .h = max(thin_rect.w/2, 1) };
			if (screen->show_entry_horizontal_border) {
				SDL_RenderFillRect(screen->renderer, &horizontal);
			}
			SDL_RenderFillRect(screen->renderer, &bold_rect);
			horizontal.y += bold_rect.h;
			bold_rect.x = screen->width - (bold_rect.w + screen->excess_width);
			if (screen->show_entry_horizontal_border) {
				SDL_RenderFillRect(screen->renderer, &horizontal);
			}
			SDL_RenderFillRect(screen->renderer, &bold_rect);
		} else {
			bold_rect.x = screen->width - (bold_rect.w + screen->excess_width);
			SDL_Rect horizontal = { .x = thin_rect.x, .y = thin_rect.y, .w = thin_rect.w + bold_rect.x - thin_rect.x, .h = max(thin_rect.w/2, 1) };
			if (screen->show_entry_horizontal_border) {
				SDL_RenderFillRect(screen->renderer, &horizontal);
			}
			SDL_RenderFillRect(screen->renderer, &thin_rect);
			horizontal.y += thin_rect.h;
			thin_rect.x = bold_rect.x;
			if (screen->show_entry_horizontal_border) {
				SDL_RenderFillRect(screen->renderer, &horizontal);
			}
			SDL_RenderFillRect(screen->renderer, &thin_rect);
		}
		/*
		bold_rect.y = thin_rect.y + thin_rect.h;
		SDL_RenderFillRect(screen->renderer, &bold_rect);
		bold_rect.x = 0;
		SDL_RenderFillRect(screen->renderer, &bold_rect);
		*/

		SDL_SetRenderDrawColor(screen->renderer, bg.r, bg.g, bg.b, bg.a);
		return true;
	}
	// returns if it was drawn
	return false;
}


void render_text_input (Language *lang, Fonts *fonts, Screen *screen, Cursor *cursor) {
	Dictionary *dict = &lang->dicts[lang->dict_index];
	if (dict->input_line_number < 0 || dict->input_line_number >= screen->num_lines_on_screen) return;

	Font *font = &fonts->monospaced;
	char *ptr = lang->input, *cursor_ptr = &lang->input[lang->input_pos];
	float prev_char_xadvance = 0.0f;
	int curr_char_index = 0/*, prev_char_index = 0*/;
	uint32_t unicode;
	BakedChar *bcp;
	int baseline = (screen->border_height < 0 ? 3 : 1) * screen->border_height/3 + floorf(fonts->regular.ascent - fonts->regular.descent + screen->line_gap + 0.5f) * dict->input_line_number + font->ascent + 0.5f;

	/*
	if (dict->expanded.entry_index > -1 && dict->expanded.line_start < screen->num_lines_on_screen && dict->expanded.line_end >= 0) {
		int thickness = (fonts->regular.ascent - fonts->regular.descent + screen->line_gap + 0.5f);
		if (dict->expanded.line_end < dict->input_line_number) {
			baseline += thickness;
		}
	}
	*/

	SDL_Rect screen_rect = { .x = screen->border_width + screen->excess_width, .y = baseline };
	SDL_Rect texture_rect;
	bool redraw_cursor = false;

	while (*ptr) {
		// retrive glyph from char's unicode
		unicode = utf8_to_unicode(&ptr);
		if (unicode < 128) {
			curr_char_index = unicode;
		} else {
			curr_char_index = find_baked_char_index(font->baked_chars.baked_ptr + 127, font->baked_chars.count - 127, unicode);
			if (curr_char_index < 0) {
				curr_char_index = 127 - curr_char_index;
				bake_char_glyph(font, unicode, curr_char_index, false);
				render_font_texture(font, screen, false);
				//printf("new char: unicode = %d, index = %u; font kerning count = %u\n", unicode, curr_char_index, font->kerning.count);
				//printf("new char: unicode = %d, index = %u; char count = %u, font kerning count = %u\n", unicode, curr_char_index, font->baked_chars.count, font->kerning.count);
			} else {
				curr_char_index += 127;
			}
		}
		bcp = &font->baked_chars.baked_ptr[curr_char_index];

		// draw glyph from texture to screen
		texture_rect = (SDL_Rect) {
			.x = bcp->x,
			.y = bcp->y,
			.w = bcp->width,
			.h = bcp->height,
		};
		screen_rect = (SDL_Rect) {
			.x = screen_rect.x + prev_char_xadvance + bcp->xoff /*+ kern_advance(font, prev_char_index, curr_char_index)*/ + 0.5f,
			.y = baseline + bcp->yoff + 0.5f,
			.w = texture_rect.w,
			.h = texture_rect.h,
		};
		SDL_RenderCopy(screen->renderer, font->texture, &texture_rect, &screen_rect);
		prev_char_xadvance = bcp->xadvance;
		//prev_char_index = curr_char_index;

		if (redraw_cursor) {
			cursor->char_dst = screen_rect;
			cursor->char_src = (SDL_Rect) {
				.x = 0,
				.y = 0,
				.w = cursor->char_dst.w,
				.h = cursor->char_dst.h,
			};

			if (bcp->glyph_index != cursor->glyph_index) {
				int result, pitch;
				uint32_t *rgba_pixels, color;
				/*
				if (cursor->glyph_index != BLANK_CURSOR) {
					memset(cursor->bitmap, 0x0, cursor->src.w * cursor->src.h);
				}
				*/

				/*
				int begin = bcp->xoff < 0 ? -bcp->xoff : 0;
				//int end = bcp->width > cursor->src.w ? cursor->src.w : bcp->width;

				for (int y = 0; y < bcp->height; y++) {
					for (int x = begin; x < bcp->width; x++) {
						if (bcp->xoff + x >= cursor->src.w) break; // @todo: make this more efficient
						cursor->bitmap[cursor->src.w * ((int)(bcp->yoff + font->ascent) + y) + (int)(bcp->xoff) + x] = font->bitmap.data[font->bitmap.width * (bcp->y + y) + (bcp->x + x)];
					}
				}
				*/

				if (cursor->char_src.w > 0 && cursor->char_src.h > 0) {
					result = SDL_LockTexture(cursor->texture, &cursor->char_src, (void **)&rgba_pixels, &pitch);
					if (result < 0) err("failed to lock cursor texture: %s\n", SDL_GetError());
					pitch /= sizeof(uint32_t);

					color = screen->background_color.r | screen->background_color.g << 8 | screen->background_color.b << 16;
					for (int y = 0; y < cursor->char_src.h; y++) {
						uint8_t *src = &font->bitmap.data[font->bitmap.width * (bcp->y + y) + bcp->x];
						uint32_t *dst = &rgba_pixels[pitch * y];

						for (int x = 0; x < cursor->char_src.w; x++) {
							dst[x] = color | src[x] << 24;
						}
					}
					SDL_UnlockTexture(cursor->texture);
				}

				cursor->glyph_index = bcp->glyph_index;
			}
			redraw_cursor = false;
		}

		if (ptr == cursor_ptr) {
			// update text input cursor
			cursor->slab_dst.x = screen_rect.x + prev_char_xadvance + font->baked_chars.baked_ptr[' '].xoff + 0.5f;
			cursor->slab_dst.y = baseline - font->ascent + 0.5f;

			redraw_cursor = true;
		}


	}

	if (ptr == cursor_ptr) {
		// update text input cursor
		cursor->slab_dst.x = screen_rect.x + prev_char_xadvance + font->baked_chars.baked_ptr[' '].xoff + 0.5f;
		cursor->slab_dst.y = baseline - font->ascent + 0.5f;

		cursor->glyph_index = BLANK_CURSOR;
		/*
		if (cursor->glyph_index != BLANK_CURSOR) {
			memset(cursor->bitmap, 0x0, cursor->src.w * cursor->src.h);
			cursor->glyph_index = BLANK_CURSOR;
		}
		*/
	}
}


void displace_lines_below (Dictionary *dict, int threshold, int displacement) {
	if (dict->selected.line_start > threshold) dict->selected.line_start += displacement;
	if (dict->selected.line_end   > threshold) dict->selected.line_end   += displacement;
	if (dict->matched.line_start  > threshold) dict->matched.line_start  += displacement;
	if (dict->matched.line_end    > threshold) dict->matched.line_end    += displacement;
	if (dict->expanded.line_start > threshold) dict->expanded.line_start += displacement;
	if (dict->expanded.line_end   > threshold) dict->expanded.line_end   += displacement;
	if (dict->input_line_number   > threshold) dict->input_line_number   += displacement;
}

void displace_lines_above (Dictionary *dict, int threshold, int displacement) {
	if (dict->selected.line_start < threshold) dict->selected.line_start += displacement;
	if (dict->selected.line_end   < threshold) dict->selected.line_end   += displacement;
	if (dict->matched.line_start  < threshold) dict->matched.line_start  += displacement;
	if (dict->matched.line_end    < threshold) dict->matched.line_end    += displacement;
	if (dict->expanded.line_start < threshold) dict->expanded.line_start += displacement;
	if (dict->expanded.line_end   < threshold) dict->expanded.line_end   += displacement;
	if (dict->input_line_number   < threshold) dict->input_line_number   += displacement;
}

void displace_lines (Dictionary *dict, int displacement) {
	dict->selected.line_start += displacement;
	dict->selected.line_end   += displacement;
	dict->matched.line_start  += displacement;
	dict->matched.line_end    += displacement;
	dict->expanded.line_start += displacement;
	dict->expanded.line_end   += displacement;
	dict->input_line_number   += displacement;
}


int num_lines_from_expanded_entry (Dictionary *dict, Fonts *fonts, Screen *screen, bool mark_first_line) {
	float prev_char_xadvance = 0.0f;
	uint32_t unicode;
	int curr_char_index = 0, prev_char_index = 0, prev_char_unicode = 0;
	BakedChar *bcp;
	Font *font = &fonts->regular;
	Font *prev_font = font;
	char *ptr = dict->entries[dict->expanded.entry_index];
	int new_chars_count[arrsz(fonts->array)] = {0};
	int first_line = (dict->expanded.line_start <= 0 && dict->expanded.line_end >= 0) ? -dict->expanded.line_start : -1;
	if (mark_first_line) dict->first_line_marker = NULL;

	SDL_Rect screen_rect = { .x = screen->border_width + screen->excess_width };

	Font *endline_font = font;
	int num_lines;

	for (num_lines = 0; *ptr != '\n'; num_lines++) {

		if (num_lines != 0) {
			// ident lines of expanded entry from the second one onward
			screen_rect.x += fonts->regular.baked_chars.baked_ptr['m'].xadvance + 0.5f;
			//if (num_lines == 0) printf("2ptr = \"%c%c%c%c\"\n", *(ptr-2), *(ptr-1), *ptr, *(ptr+1));
			//if (num_lines == 1) printf("2ptr = \"%c%c%c%c\"\n", *(ptr-2), *(ptr-1), *ptr, *(ptr+1));
		} else {
			font = &fonts->regular;
			while (*ptr++);
			ptr++;
		}
		if (mark_first_line && num_lines == first_line) {
			dict->first_line_marker = ptr;
		}

		// @todo: make DASH a separator and don't skip the final letter of a one-line word: maybe put a '-' after it
		char *line_end = NULL;
		bool reached_end_of_line = false;
		prev_char_index = 0;
		prev_char_unicode = 0;
		prev_char_xadvance = 0;
		reached_end_of_line = false;

		while (*ptr != '\n' && !reached_end_of_line) { // and while there is space in line
			// many lines in this next loop are just to correct some mistakes in the dictionaries
			if (dict->first_line_marker == ptr) {
				dict->first_line_number = num_lines;
				//printf(">>>> first_line_number = %d: %p\n", dict->first_line_number, dict->first_line_marker);
			}
			while (*ptr == '*' || *ptr == '`') {
				if (*ptr == '*') { // make italic
					int surrounded = 0;
					if (*(ptr-1) == ' ') surrounded++;
					font = (font != &fonts->italic) ? &fonts->italic : &fonts->regular;
					if (*++ptr == '*') ptr++;
					if (*ptr == ' ') surrounded++;
					if (surrounded == 2) {
						ptr++;
					}
				} else if (*ptr == '`') { // make bold
					if (*(ptr-1) == ' ' && *(ptr+1) == ' ') {
						ptr++;
					}
					font = (font != &fonts->bold) ? &fonts->bold : &fonts->regular;
					ptr++;
				}
			}
			if (*ptr == '\n') break;
			unicode = utf8_to_unicode(&ptr);

			if (unicode < 128) {
				curr_char_index = unicode;
			} else {
				curr_char_index = find_baked_char_index(font->baked_chars.baked_ptr + 127, font->baked_chars.count - 127, unicode);
				if (curr_char_index < 0) {
					curr_char_index = 127 - curr_char_index;
					bake_char_glyph(font, unicode, curr_char_index, false);
					new_chars_count[((uintptr_t)font - (uintptr_t)fonts->array) / sizeof(Font)]++;
					//printf("arrsz = %d, index = %lu\n", arrsz(fonts->array), ((uintptr_t)font - (uintptr_t)fonts->array) / sizeof(Font));
					//printf("new char: unicode = %d, index = %u; font kerning count = %u\n", unicode, curr_char_index, font->kerning.count);
					//printf("new char: unicode = %d, index = %u; char count = %u, font kerning count = %u\n", unicode, curr_char_index, font->baked_chars.count, font->kerning.count);
					// get updated index
					prev_char_index = prev_char_unicode < 128 ? prev_char_unicode : 127 + find_baked_char_index(prev_font->baked_chars.baked_ptr + 127, prev_font->baked_chars.count - 127, prev_char_unicode);
				} else {
					curr_char_index += 127;
				}
			}
			bcp = &font->baked_chars.baked_ptr[curr_char_index];

			if (unicode < 0x0300 || unicode > 0x036f) {
				if (unicode == ' ' && prev_char_index != ' ') {
					// a space means a possible location to break line
					line_end = previous_char(ptr, 1);
					endline_font = font;
				}

				int xadvance = prev_char_xadvance + bcp->xoff + (prev_font == font ? kern_advance(font, prev_char_index, curr_char_index) : 0.0f) + 0.5f;
				if (screen_rect.x + xadvance + bcp->width >= screen->width - (screen->border_width + screen->excess_width)) { // no more line space
					if (line_end != NULL) {
						ptr = line_end + 1;
					}
					reached_end_of_line = true;
					font = endline_font;
					break;
				}

				screen_rect.x += xadvance;

				prev_char_xadvance = bcp->xadvance;
				prev_char_index = curr_char_index;
				prev_char_unicode = unicode;
				prev_font = font;
			}
		}

		while (*ptr == ' ') ptr++;
		screen_rect.x = screen->border_width + screen->excess_width;
	}

	for (int i = 0; i < arrsz(fonts->array); i++) {
		if (new_chars_count[i]) {
			render_font_texture(&fonts->array[i], screen, new_chars_count[i] > 1);
		}
	}

	return num_lines;
}

void adapt_to_resizing (Language *langs, int num_langs, Fonts *fonts, Screen *screen) {
	int input_displacement = 0;

	screen->line_gap = 0.05f * screen->relative_line_gap * (fonts->regular.ascent - fonts->regular.descent) + 0.5f;
	screen->border_width = border_width_updated(fonts);
	screen->num_lines_on_screen = num_lines_on_screen_updated(screen, fonts);
	screen->border_height = border_height_updated(screen, fonts);
	screen->excess_width = max((screen->width - screen->max_width) / 2, 0);
	//printf("excess width = %d\n", screen->excess_width);

	if (screen->num_lines_before_input >= screen->num_lines_on_screen - 1) {
		//printf("before = %d, on screen = %d\n", screen->num_lines_before_input, screen->num_lines_on_screen);
		input_displacement =  screen->num_lines_on_screen - (screen->num_lines_before_input + 1);
		screen->num_lines_before_input += input_displacement;
	}

	for (int j = 0; j < num_langs; j++) {
		for (int i = 0; i < langs[j].num_dicts; i++) {
			Dictionary *dict = &langs[j].dicts[i];

			if (dict->expanded.entry_index >= 0) {
				int expansion_displacement = num_lines_from_expanded_entry(dict, fonts, screen, false)/* - 1*/;
				int delta_displacement = expansion_displacement - (dict->expanded.line_end - dict->expanded.line_start);

				if (dict->expanded.line_end >= 0) {
					displace_lines_below(dict, dict->expanded.line_start, delta_displacement);
					dict->expanded.line_end = dict->expanded.line_start + expansion_displacement;
					if (dict->expanded.line_end < 0) {
						displace_lines(dict, -dict->expanded.line_end);
					}
				} else {
					displace_lines_above(dict, dict->expanded.line_end, -delta_displacement);
					dict->expanded.line_start = dict->expanded.line_end - expansion_displacement;
				}

				if (dict->selected.entry_index == dict->expanded.entry_index) {
					dict->selected = dict->expanded;
				}
				// @todo: done
				//printf("%d, %d\n", dict->expanded.line_start, dict->expanded.line_end);
				if (dict->expanded.line_start <= 0 && dict->expanded.line_end >= 0) {
					// use the char* to the first character in line
					//printf("displaced by: %d: -(%d + %d): %p\n", -(dict->expanded.line_start + dict->first_line_number), dict->expanded.line_start, dict->first_line_number, dict->first_line_marker);
					displace_lines(dict, -(dict->expanded.line_start + dict->first_line_number));
				}/* else if (dict->expanded.line_end < 0) {
					displace_lines(dict, -delta_displacement);
				}*/

				//if (dict->expanded.line_start < 0) {
					//displace_lines(dict, -delta_displacement);
				//}
			}
			if (dict->selected.entry_index >= 0) {
				if (dict->selected.line_end < 0) {
					//printf("DISPLACE BY: %d\n", -dict->selected.line_end);
					displace_lines(dict, -dict->selected.line_end);
				} else if (dict->selected.line_start >= screen->num_lines_on_screen) {
					displace_lines(dict, -(dict->selected.line_start - screen->num_lines_on_screen + 1));
				}
			} else { // text input on
				displace_lines(dict, screen->num_lines_before_input - (dict->input_line_number + input_displacement));
			}

			dict->made_a_move = false;
		}
	}

}


int num_lines_on_screen_updated (Screen *screen, Fonts *fonts) {
	//printf("screen line gap = %d\n", screen->line_gap);
	return (screen->height - (((fonts->monospaced.ascent - fonts->monospaced.descent)) + max(screen->line_gap, 0))/2) / (fonts->regular.ascent - fonts->regular.descent + screen->line_gap);
}
/*
	int numerator = screen->height - 2 * ((fonts->monospaced.ascent - fonts->monospaced.descent) - (fonts->regular.ascent - fonts->regular.descent));
	int denominator = fonts->regular.ascent - fonts->regular.descent + screen->line_gap;
	int quotient = numerator / denominator;
	int remainder = (numerator + screen->line_gap) % ;
*/

int border_width_updated (Fonts *fonts) {
	return 0.6f * fonts->regular.baked_chars.baked_ptr['m'].width + 0.35f * fonts->regular.ascent + 0.5f;
}

int border_height_updated (Screen *screen, Fonts *fonts) {
	int border_height = (screen->height - (fonts->regular.ascent - fonts->regular.descent + screen->line_gap) * screen->num_lines_on_screen + screen->line_gap) / 2;
	//printf("border height = %d\n", border_height);
	return border_height;
}


int greek_strcmp (char *str1, char *str2) {
	char *str[2] = { str1, str2 };
	int unicode[2];

	do {
		for (int i = 0; i < 2; i++) {
			unicode[i] = utf8_to_unicode(&str[i]);

			if (unicode[i] >= ALPHA_MINUS && unicode[i] <= OMEGA_MINUS) {
			avoid_testing_again:
				if (unicode[i] >= ZETA_MINUS && unicode[i] <= SIGMA_FINAL) {
					// make SIGMA_FINAL = SIGMA_COMMON and give space to DIGAMMA_MINUS
					unicode[i]++;
				}
			} else if (unicode[i] >= ALPHA_MAIUS && unicode[i] <= OMEGA_MAIUS) {
				unicode[i] += ALPHA_MINUS - ALPHA_MAIUS;
				goto avoid_testing_again;
			} else if (unicode[i] == DIGAMMA_MAIUS || unicode[i] == DIGAMMA_MINUS) {
				// put DIGAMMA_MINUS in its ancient place
				unicode[i] = ZETA_MINUS;
			} else if (unicode[i] == YOT_MAIUS) {
				unicode[i] = YOT_MINUS;
			}
		}

	} while (unicode[0] == unicode[1] && unicode[0] != '\0' && unicode[1] != '\0');

	return unicode[0] - unicode[1];
}


int convert_to_greek (char *dst, char *src/*, int input_length*/) {
	if (!*src) return 0;
	//printf("input length = %d\n", input_length);

	const char *greek_maius = "ΑΒΞΔΕΦΓΗΙͿΚΛΜΝΟΠΘΡΣΤΥϜΩΧΨΖ";
	const char *greek_minus = "αβξδεφγηιϳκλμνοπθρςτυϝωχψζ";
	//const char *sigma = "σ";

	//const uint16_t *sigma_final = ((uint16_t *)greek_minus + 's' - 'a');
	//const uint16_t *sigma_common = (uint16_t *)sigma;

	uint16_t *greek_dst16 = (uint16_t *)dst;
	uint16_t *greek_maius16 = (uint16_t *)greek_maius;
	uint16_t *greek_minus16 = (uint16_t *)greek_minus;

	//if (input_length > 0 && *(greek_dst16 - 1) == *sigma_final) {
		//uint8_t next = *(uint8_t *)src;
		//if (!(next < 'A' || (next > 'Z' && next < 'a') || (next > 'z' && next < 128))) {
			//*(greek_dst16 - 1) = *sigma_common;
		//}
	//}

	do {
		if (*src >= 'a' && *src <= 'z') {
			//if (*src == 's') {
				//uint8_t prev = *(uint8_t *)(greek_dst16 - 1);
				//if (input_length == 0 || (prev < 'A' || (prev > 'Z' && prev < 'a') || (prev > 'z' && prev < 128))) {
					//*greek_dst16 = *sigma_common;
				//} else {
					//*greek_dst16 = *sigma_final;
				//}
			//} else {
				*greek_dst16 = *(greek_minus16 + *src - 'a');
			//}
			src++; greek_dst16++;
		} else if (*src >= 'A' && *src <= 'Z') {
			*greek_dst16 = *(greek_maius16 + *src - 'A');
			src++; greek_dst16++;
		} else {
			char *greek_copy = (char *)greek_dst16;
			do {
				*greek_copy++ = *src++;
			} while (*src & 0x80);
			greek_dst16 = (uint16_t *)greek_copy;
		}
	} while (*src != '\0');

	*greek_dst16 = 0;

	return (int)((uintptr_t)greek_dst16 - (uintptr_t)dst);
}


void convert_sigmas_if_needed (char *str, int strlen) {
	const char *sigma_common = "σ";
	const char *sigma_final = "ς";

	/*
	const uint16_t *sigma_common16 = (uint16_t *)sigma_common;
	const uint16_t *sigma_final16 = (uint16_t *)sigma_final;

	uint16_t *back16 = (uint16_t *)str - 1;

	if (strlen > 2 && *back16 == *sigma_common16) {
		uint8_t prev = *((uint8_t *)back16 - 1);
		if (!(prev < 'A' || (prev > 'Z' && prev < 'a') || (prev > 'z' && prev < 128))) {
			*back16 = *sigma_final16;
		}
	}
	*/

	////////////// new /////////
#define is_a_separator(x) ((x) < 'A' || ((x) > 'Z' && (x) < 'a') || ((x) > 'z' && (x) < 128))

	char *end = str + strlen;
	char *next_str = str, *prev_str = str;
	uint32_t unicode, prev_unicode = 0, prev_prev_unicode = 0;

	unicode = utf8_to_unicode(&next_str);
	if (unicode == SIGMA_FINAL) {
		memcpy(str, sigma_common, 2);
		unicode = SIGMA_COMMON;
	}

	while (next_str <= end) {
		prev_prev_unicode = prev_unicode;
		prev_unicode = unicode;
		prev_str = str;
		str = next_str;

		unicode = utf8_to_unicode(&next_str);
		if (prev_unicode == SIGMA_COMMON && (is_a_separator(unicode) && !is_a_separator(prev_prev_unicode))) {
			memcpy(prev_str, sigma_final, 2);
		} else if (prev_unicode == SIGMA_FINAL && (!is_a_separator(unicode) || is_a_separator(prev_prev_unicode))) {
			memcpy(prev_str, sigma_common, 2);
		}
	}

#undef is_a_separator
}

void new_cursor (Cursor *cursor, Font *font, Screen *screen, bool remake) {
	//uint32_t *rgba_pixels, color;
	//int result, pitch;

	if (remake) {
		//free(cursor->bitmap);
		SDL_DestroyTexture(cursor->texture);
	}

	cursor->glyph_index = BLANK_CURSOR;
	cursor->char_dst = cursor->char_src = (SDL_Rect){ 0 };

	cursor->slab_dst = (SDL_Rect) {
		.w = font->baked_chars.baked_ptr[' '].xadvance + 0.5f,
		.h = font->ascent - font->descent - 0.5f,
	};
	/*
	cursor->bitmap = malloc(cursor->src.w * cursor->src.h * sizeof(uint8_t));
	if (!cursor->bitmap) err("failed to allocate cursor bitmap: %s\n", strerror(errno));
	memset(cursor->bitmap, 0x00, cursor->src.w * cursor->src.h);
	*/


	cursor->texture = SDL_CreateTexture(screen->renderer, SDL_PIXELFORMAT_RGBA32, SDL_TEXTUREACCESS_STREAMING, cursor->slab_dst.w * 2, cursor->slab_dst.h);
	if (!cursor->texture) err("failed to allocate cursor texture: %s\n", SDL_GetError());
	SDL_SetTextureBlendMode(cursor->texture, SDL_BLENDMODE_BLEND);

	/*
	result = SDL_LockTexture(cursor->texture, &cursor->src, (void **)&rgba_pixels, &pitch);
	if (result < 0) err("failed to access cursor texture: %s\n", SDL_GetError());
	pitch /= sizeof(uint32_t);

	color = screen->background_color.r | screen->background_color.g << 8 | screen->background_color.b << 16;
	for (int y = 0; y < cursor->src.h; y++) {
		uint8_t *src = cursor->bitmap + cursor->src.w * y;
		uint32_t *dst = rgba_pixels + pitch * y;

		for (int x = 0; x < cursor->src.w; x++) {
			dst[x] = color | src[x] << 24;
		}
	}

	SDL_UnlockTexture(cursor->texture);
	*/
}


/*
void change_cursor (Cursor *cursor, Language *lang, Font *font, Screen *screen) {
	char *ptr = lang->input[lang->input_pos];
	uint32_t unicode = utf8_to_unicode(&ptr);

	if (cursor->glyph_index != 0) {
		memset(cursor->bitmap, 0x00, cursor->src.w * cursor->src.h);
	}
	if (unicode == 0) return;


}
*/

//ΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩ
//αβγδεζηθικλμνξοπρσςτυφχψω


//@todo see if this works when completed: it does

void init_color_selector (ColorSelector *cs, Screen *screen, bool reinit) {
	int side_length, width, height;

	width = screen->width - 2 * (screen->border_width + screen->excess_width) ;
	height = screen->height - 2 * screen->border_height;
	//printf("width = %d, screen_width = %d, border_width = %d, excess_width = %d\n", width, screen->width, screen->border_width, screen->excess_width);

	// wheel
	if (width < height) {
		side_length = max((width % 2) ? width : width + 1, 13);
		cs->wheel.dst_rect = (SDL_Rect){ .x = screen->border_width + screen->excess_width, .y = (screen->height - side_length) / 2, .w = side_length, .h = side_length };
	} else {
		side_length = max((height % 2) ? height : height + 1, 13);
		cs->wheel.dst_rect = (SDL_Rect){ .x = (screen->width - side_length) / 2, .y = screen->border_height, .w = side_length, .h = side_length };
	}

	if (reinit) {
		SDL_DestroyTexture(cs->wheel.texture);
		SDL_DestroyTexture(cs->triangle.texture);
	}

	cs->wheel.src_rect = (SDL_Rect){ .x = 0, .y = 0, .w = side_length, .h = side_length };

	cs->wheel.center = (SDL_Point) { .x = side_length/2, side_length/2 };
	cs->wheel.inner_radius = side_length * 0.4f;
	cs->wheel.outer_radius = side_length * 0.5f;

	cs->wheel.texture = SDL_CreateTexture(screen->renderer, SDL_PIXELFORMAT_RGBA32, SDL_TEXTUREACCESS_STREAMING, side_length, side_length);
	if (!cs->wheel.texture) err("failed to create color selector wheel texture: %s\n", SDL_GetError());
	SDL_SetTextureBlendMode(cs->wheel.texture, SDL_BLENDMODE_BLEND);

	// triangle
	width = cs->wheel.inner_radius * 3/2;
	height = cs->wheel.inner_radius * sqrtf(3) + 1;

	cs->triangle.src_rect = (SDL_Rect){ .x = 0, .y = 0, .w = width, .h = height };
	cs->triangle.dst_rect = (SDL_Rect){ .x = cs->wheel.dst_rect.x + cs->wheel.center.x - cs->wheel.inner_radius/2, .y = cs->wheel.dst_rect.y + cs->wheel.center.y - cs->wheel.inner_radius * sqrtf(3)/2, .w = width, .h = height };

	cs->triangle.white_vertex = (SDL_Point){ .x = 0, .y = 0 };
	cs->triangle.black_vertex = (SDL_Point){ .x = 0, .y = height };
	cs->triangle.hued_vertex  = (SDL_Point){ .x = width, .y = height/2 };

	cs->triangle.texture = SDL_CreateTexture(screen->renderer, SDL_PIXELFORMAT_RGBA32, SDL_TEXTUREACCESS_STREAMING, width, height);
	if (!cs->triangle.texture) err("failed to create color selector triangle texture: %s\n", SDL_GetError());
	SDL_SetTextureBlendMode(cs->triangle.texture, SDL_BLENDMODE_BLEND);

	if (!reinit) {
		cs->angle = 0;
		cs->saturation = 0;
		cs->lightness = 0;
		cs->pure_color = (SDL_Color){ .r = 0xff, .g = 0x00, .b = 0x00, .a = 0xff };
	}
}


/*
#define closeness(ax, ay, b) (1.0f / ((ax - b.x) * (ax - b.x) + (ay - b.y) * (ay - b.y)))

SDL_Color color_from_triangle_position (ColorSelector *cs, int x, int y) {
	float hued_closeness, white_closeness, black_closeness, closeness_sum_inverse, gray_influence;
	SDL_Color color;

	hued_closeness = closeness(x, y, cs->triangle.hued_vertex);
	white_closeness = closeness(x, y, cs->triangle.white_vertex);
	black_closeness = closeness(x, y, cs->triangle.black_vertex);
	closeness_sum_inverse = 1.0f / (hued_closeness + white_closeness + black_closeness);
	gray_influence = 0xff * white_closeness;

	color = (SDL_Color) {
		.r = (cs->pure_color.r * hued_closeness + gray_influence) * closeness_sum_inverse,
		.g = (cs->pure_color.g * hued_closeness + gray_influence) * closeness_sum_inverse,
		.b = (cs->pure_color.b * hued_closeness + gray_influence) * closeness_sum_inverse,
		.a = 0xff,
	};

	return color;
}
*/

SDL_Color selected_color (ColorSelector *cs) {
	SDL_Color color = {
		.r = cs->saturation * cs->pure_color.r + cs->lightness * 0xff,
		.g = cs->saturation * cs->pure_color.g + cs->lightness * 0xff,
		.b = cs->saturation * cs->pure_color.b + cs->lightness * 0xff,
		.a = 0xff,
	};

	return color;
}


#define TWO_DIV_SQRT_THREE 1.154700538f

SDL_Color color_from_triangle_position (ColorSelector *cs, int x, int y) {
	SDL_Color color;
	float saturation, lightness, delta_x, delta_y, gray_influence;

	delta_x = x / (float)cs->triangle.hued_vertex.x;
	delta_y = (cs->triangle.black_vertex.y - y) / (float)cs->triangle.black_vertex.y;

	saturation = delta_x;
	lightness = delta_y - 0.5f * saturation;
	gray_influence = lightness * 0xff;

	color = (SDL_Color) {
		.r = saturation * cs->pure_color.r + gray_influence,
		.g = saturation * cs->pure_color.g + gray_influence,
		.b = saturation * cs->pure_color.b + gray_influence,
		.a = 0xff,
	};

	return color;
}

float point_to_line_distance (SDL_Point *a, SDL_Point *b, int x, int y) {
	float dist = ((b->y - a->y) * x - (b->x - a->x) * y + (b->x * a->y) - (b->y * a->x)) / sqrtf((b->y - a->y) * (b->y - a->y) + (b->x - a->x) * (b->x - a->x));
	dist += 0.5f;
	//printf("dist = %f\n", dist);
	return (dist > 0) ? 1.0f - dist / 1.7f : 1.0f;
}


void draw_color_triangle (ColorSelector *cs) {
	float y_slope, x0;
	struct Triangle *t = &cs->triangle;
	SDL_Color *color_map;
	int result, pitch, initial_x, final_x, final_y;

	SDL_Rect rect = {
		.x = t->white_vertex.x,
		.y = t->white_vertex.y,
		.w = t->hued_vertex.x - t->white_vertex.x,
		.h = t->black_vertex.y - t->white_vertex.y,
	};
	result = SDL_LockTexture(cs->triangle.texture, &rect, (void **)&color_map, &pitch);
	if (result < 0) err("failed to lock color selector triangle texture: %s\n", SDL_GetError());
	pitch /= sizeof(SDL_Color);

	// upper triangle
	y_slope = (t->white_vertex.x - t->hued_vertex.x) / (float)(t->white_vertex.y - t->hued_vertex.y);
	x0 = 0;

	final_y = t->hued_vertex.y - t->white_vertex.y;
	for (int y = 0; y < final_y; y++) {
		// opaque draw
		final_x = y_slope * (y - 0.5f) + x0;
		for (int x = 0; x < final_x; x++) {
			color_map[pitch * y + x] = color_from_triangle_position(cs, x, y);
		}
		// semi-transparent draw
		initial_x = final_x;
		final_x = ceil(y_slope * (y + 0.5f) + x0);
		for (int x = initial_x; x < final_x; x++) {
			color_map[pitch * y + x] = color_from_triangle_position(cs, x, y);
			color_map[pitch * y + x].a = 0xff * point_to_line_distance(&cs->triangle.white_vertex, &cs->triangle.hued_vertex, x, y);
		}

		for (int x = final_x + 1; x < rect.w; x++) {
			color_map[pitch * y + x] = (SDL_Color) { .r = 0, .g = 0, .b = 0, .a = 0 };
		}
	}

	// lower triangle
	y_slope = (t->black_vertex.x - t->hued_vertex.x) / (float)(t->black_vertex.y - t->hued_vertex.y);
	x0 = - y_slope * rect.h;

	final_y = t->black_vertex.y - t->white_vertex.y;
	for (int y = t->hued_vertex.y - t->white_vertex.y; y < final_y; y++) {
		//int lightness = (y - t->black_vertex.y) * 0xff / (t->white_vertex.y - t->black_vertex.y);
		// opaque draw
		final_x = y_slope * (y + 0.5f) + x0;
		for (int x = 0; x < final_x; x++) {
			color_map[pitch * y + x] = color_from_triangle_position(cs, x, y);
		}
		// semi-transparent draw
		initial_x = final_x;
		final_x = ceil(y_slope * (y - 0.5f) + x0);
		for (int x = initial_x; x < final_x; x++) {
			color_map[pitch * y + x] = color_from_triangle_position(cs, x, y);
			color_map[pitch * y + x].a = 0xff * point_to_line_distance(&cs->triangle.hued_vertex, &cs->triangle.black_vertex, x, y);
		}

		for (int x = final_x + 1; x < rect.w; x++) {
			color_map[pitch * y + x] = (SDL_Color) { .r = 0, .g = 0, .b = 0, .a = 0 };
		}
	}

	SDL_UnlockTexture(cs->triangle.texture);

	// @todo: separate the cpu drawing from the gpu drawing -> draw with the color wheel
	//SDL_RenderCopy(screen->renderer, cs->triangle.texture, &cs->triangle.src_rect, &cs->triangle.dst_rect);
}

float cathetus (float hypotenuse, float other_cathetus) {
	return sqrtf((hypotenuse * hypotenuse) - (other_cathetus * other_cathetus));
}

SDL_Color color_from_wheel_position (int delta_x, int delta_y, ColorSelector *cs) {
	SDL_Color color;
	int half_color;
	float angle = atan2f(delta_y, -delta_x) + PI;
	uint8_t alpha;
	float pixel_radius = sqrtf((delta_y * delta_y) + (delta_x * delta_x));

	if (pixel_radius - (cs->wheel.outer_radius - cs->wheel.inner_radius) / 2 > cs->wheel.inner_radius) { // outer radius
		float radii_diff = pixel_radius + 0.5f - cs->wheel.outer_radius;
		alpha = (radii_diff > 0) ? 0xff - 0xff * radii_diff / 1.7f : 0xff;
	} else { // inner radius
		float radii_diff = cs->wheel.inner_radius - (pixel_radius - 0.5f);
		alpha = (radii_diff > 0) ? 0xff - 0xff * radii_diff / 1.7f : 0xff;
		//if (radii_diff > 0) printf("radii diff = %f\n", radii_diff);
	}

	if (angle < PI2_3) { // red-blue
		half_color = 2 * 0xff * angle / PI2_3;
		if (half_color <= 0xff) {
			color = (SDL_Color){ .b = half_color, .r = 0xff, .g = 0, .a = alpha };
		} else {
			half_color -= 0xff;
			color = (SDL_Color){ .b = 0xff, .r = 0xff - half_color, .g = 0, .a = alpha };
		}
	} else if (angle >= PI4_3) { // green-red
		half_color = 2 * 0xff * (angle - PI4_3) / PI2_3;
		if (half_color <= 0xff) {
			color = (SDL_Color){ .r = half_color, .g = 0xff, .b = 0, .a = alpha };
		} else {
			half_color -= 0xff;
			color = (SDL_Color){ .r = 0xff, .g = 0xff - half_color, .b = 0, .a = alpha };
		}
	} else { // blue-green
		half_color = 2 * 0xff * (angle - PI2_3) / PI2_3;
		if (half_color <= 0xff) {
			color = (SDL_Color){ .g = half_color, .b = 0xff, .r = 0, .a = alpha };
		} else {
			half_color -= 0xff;
			color = (SDL_Color){ .g = 0xff, .b = 0xff - half_color, .r = 0, .a = alpha };
		}
	}

	return color;
}

SDL_Color color_from_wheel_angle (float angle) {
	SDL_Color color;
	int half_color;

	if (angle < PI2_3) { // red-blue
		half_color = 2 * 0xff * angle / PI2_3;
		if (half_color <= 0xff) {
			color = (SDL_Color){ .b = half_color, .r = 0xff, .g = 0, .a = 0xff };
		} else {
			half_color -= 0xff;
			color = (SDL_Color){ .b = 0xff, .r = 0xff - half_color, .g = 0, .a = 0xff };
		}
	} else if (angle >= PI4_3) { // green-red
		half_color = 2 * 0xff * (angle - PI4_3) / PI2_3;
		if (half_color <= 0xff) {
			color = (SDL_Color){ .r = half_color, .g = 0xff, .b = 0, .a = 0xff };
		} else {
			half_color -= 0xff;
			color = (SDL_Color){ .r = 0xff, .g = 0xff - half_color, .b = 0, .a = 0xff };
		}
	} else { // blue-green
		half_color = 2 * 0xff * (angle - PI2_3) / PI2_3;
		if (half_color <= 0xff) {
			color = (SDL_Color){ .g = half_color, .b = 0xff, .r = 0, .a = 0xff };
		} else {
			half_color -= 0xff;
			color = (SDL_Color){ .g = 0xff, .b = 0xff - half_color, .r = 0, .a = 0xff };
		}
	}

	return color;
}


SDL_Color pure_hue_color (SDL_Color color) {
	uint8_t max_value, min_value;

	min_value = min(color.r, min(color.g, color.b));
	color.r -= min_value;
	color.g -= min_value;
	color.b -= min_value;

	max_value = max(color.r, max(color.g, color.b));
	color.r = (color.r / (float)max_value) * 0xff;
	color.g = (color.g / (float)max_value) * 0xff;
	color.b = (color.b / (float)max_value) * 0xff;

	return color;
}


float wheel_angle_from_color (SDL_Color color) {
	float angle;
	color = pure_hue_color(color);

	if (color.r >= color.g && color.r >= color.b) {
		if (color.g >= color.b) {
			angle = (0xff - color.g) / (float)0xff * (PI/3) + (5*PI/3);
		} else {
			angle = color.b / (float)0xff * (PI/3);
		}
	} else if (color.b >= color.g) {
		if (color.r >= color.g) {
			angle = (0xff - color.r) / (float)0xff * (PI/3) + (PI/3);
		} else {
			angle = color.g / (float)0xff * (PI/3) + (2*PI/3);
		}
	} else {
		if (color.b >= color.r) {
			angle = (0xff - color.b) / (float)0xff * (PI/3) + PI;
		} else {
			angle = color.r / (float)0xff * (PI/3) + (4*PI/3);
		}
	}

	return angle;
}


void draw_color_wheel (ColorSelector *cs) {
	SDL_Color *color_map;
	int result, pitch;
	int initial_x, final_x, initial_y, final_y;

	result = SDL_LockTexture(cs->wheel.texture, NULL, (void **)&color_map, &pitch);
	if (result < 0) err("failed to lock color selector texture: %s\n", SDL_GetError());
	pitch /= sizeof(SDL_Color);

	// half wheel @do two iterations to complete wheel

	// first: the upper part, where there are no gaps
	initial_y = 0;
	final_y = cs->wheel.center.y - cs->wheel.inner_radius + 1;

	for (int y = initial_y; y < final_y; y++) {
		int delta_x = ceil(cathetus(cs->wheel.outer_radius, cs->wheel.center.y - (y + 0.5f)));
		initial_x = cs->wheel.center.x - delta_x;
		final_x = cs->wheel.center.x + delta_x;

		// outside the wheel
		for (int x = 0; x < initial_x; x++) {
			color_map[pitch * y + x] = (SDL_Color){ .r = 0, .g = 0, .b = 0, .a = 0 };
		}
		// inside the wheel
		for (int x = initial_x; x < final_x; x++) {
			color_map[pitch * y + x] = color_from_wheel_position(x - cs->wheel.center.x, y - cs->wheel.center.y, cs);
		}
		// outside the wheel
		for (int x = final_x; x < cs->wheel.src_rect.w; x++) {
			color_map[pitch * y + x] = (SDL_Color){ .r = 0, .g = 0, .b = 0, .a = 0 };
		}
	}

	// second: the middle part, where there is a gap
	initial_y = final_y;
	final_y = cs->wheel.center.y + cs->wheel.inner_radius;

	for (int y = initial_y; y < final_y; y++) {
		int outer_delta_x = ceil(fmaxf(cathetus(cs->wheel.outer_radius, cs->wheel.center.y - (y + 0.5f)), cathetus(cs->wheel.outer_radius, cs->wheel.center.y - (y - 0.5f))));
		int inner_delta_x = fminf(cathetus(cs->wheel.inner_radius, cs->wheel.center.y - (y + 0.5f)), cathetus(cs->wheel.inner_radius, cs->wheel.center.y - (y - 0.5f)));

		initial_x = cs->wheel.center.x - outer_delta_x;
		final_x = cs->wheel.center.x - (inner_delta_x/* - 1*/);

		// outside the wheel
		for (int x = 0; x < initial_x; x++) {
			color_map[pitch * y + x] = (SDL_Color){ .r = 0, .g = 0, .b = 0, .a = 0 };
		}

		// inside the wheel
		for (int x = initial_x; x < final_x; x++) {
			color_map[pitch * y + x] = color_from_wheel_position(x - cs->wheel.center.x, y - cs->wheel.center.y, cs);
		}

		// in the wheel's gap
		initial_x = final_x;
		final_x = cs->wheel.center.x + inner_delta_x;

		for (int x = final_x; x < cs->wheel.src_rect.w; x++) {
			color_map[pitch * y + x] = (SDL_Color){ .r = 0, .g = 0, .b = 0, .a = 0 };
		}

		// inside the wheel again
		initial_x = final_x;
		final_x = cs->wheel.center.x + outer_delta_x + 1;

		for (int x = initial_x; x < final_x; x++) {
			color_map[pitch * y + x] = color_from_wheel_position(x - cs->wheel.center.x, y - cs->wheel.center.y, cs);
		}

		// outside the wheel
		for (int x = final_x; x < cs->wheel.src_rect.w; x++) {
			color_map[pitch * y + x] = (SDL_Color){ .r = 0, .g = 0, .b = 0, .a = 0 };
		}
	}

	// third: the lower part, where there is no gap again
	initial_y = final_y;
	final_y = cs->wheel.src_rect.h;

	for (int y = initial_y; y < final_y; y++) {
		int delta_x = ceil(cathetus(cs->wheel.outer_radius, cs->wheel.center.y - (y - 0.5f)));
		initial_x = cs->wheel.center.x - delta_x;
		final_x = cs->wheel.center.x + delta_x;

		// outside the wheel
		for (int x = 0; x < initial_x; x++) {
			color_map[pitch * y + x] = (SDL_Color){ .r = 0, .g = 0, .b = 0, .a = 0 };
		}
		// inside the wheel
		for (int x = initial_x; x < final_x; x++) {
			color_map[pitch * y + x] = color_from_wheel_position(x - cs->wheel.center.x, y - cs->wheel.center.y, cs);
		}
		// outside the wheel
		for (int x = final_x; x < cs->wheel.src_rect.w; x++) {
			color_map[pitch * y + x] = (SDL_Color){ .r = 0, .g = 0, .b = 0, .a = 0 };
		}
	}

	SDL_UnlockTexture(cs->wheel.texture);

	// @todo: separate the cpu drawing from the gpu drawing -> draw with the color wheel
	//SDL_Rect rect = { .x = 0, .y = 0, .w = cs->wheel.texture_sides_length, .h = cs->wheel.texture_sides_length };
	//SDL_RenderCopyEx(screen->renderer, cs->wheel.texture, &cs->wheel.src_rect, &cs->wheel.dst_rect, SDL_GetTicks()/20, &cs->wheel.center, SDL_FLIP_NONE);
	//cs->pure_color = color_from_wheel_angle(fmodf(SDL_GetTicks()/20 * PI/180.0f, 2*PI));

	/*
	SDL_Color pure_color = pure_hue_color(screen->foreground_color);
	printf("pure foreground color = (%u, %u, %u)\n", pure_color.r, pure_color.g, pure_color.b);
	float angle = wheel_angle_from_color(screen->foreground_color);
	printf("foreground color angle = %f * PI\n", angle/PI);
	pure_color = color_from_wheel_angle(angle);
	printf("pure foreground color = (%u, %u, %u)\n", pure_color.r, pure_color.g, pure_color.b);

	pure_color = pure_hue_color(screen->background_color);
	printf("pure background color = (%u, %u, %u)\n", pure_color.r, pure_color.g, pure_color.b);
	angle = wheel_angle_from_color(screen->background_color);
	printf("background color angle = %f * PI\n", angle/PI);
	pure_color = color_from_wheel_angle(angle);
	printf("pure background color = (%u, %u, %u)\n", pure_color.r, pure_color.g, pure_color.b);

	SDL_RenderCopyEx(screen->renderer, cs->wheel.texture, &cs->wheel.src_rect, &cs->wheel.dst_rect, wheel_angle_from_color(screen->background_color) * 180.0f/PI, &cs->wheel.center, SDL_FLIP_NONE);
	cs->pure_color = pure_hue_color(screen->background_color);
	*/
}


float color_saturation (SDL_Color *color) {
	uint8_t min_value = min(color->r, min(color->g, color->b));
	uint8_t max_value = max(color->r, max(color->g, color->b));
	return (max_value - min_value) / (float)0xff;
}

float color_lightness (SDL_Color *color) {
	uint8_t gray_influence = min(color->r, min(color->g, color->b));
	return gray_influence / (float)0xff;
}

void draw_color_triangle_cursor (Screen *screen, ColorSelector *cs) {
	//printf("lightness = %f, saturation = %f\n", cs->lightness, cs->saturation);
	SDL_Point target;
	union {
		struct { SDL_Rect left, right, upper, lower; };
		SDL_Rect array[4];
	} inner_rects, outer_rects;
	int out_long, out_lat, in_long, in_lat, border, rim;

	target.x = cs->triangle.dst_rect.x + cs->saturation * cs->triangle.dst_rect.w;
	target.y = cs->triangle.dst_rect.y + cs->triangle.black_vertex.y - (cs->lightness + 0.5f*cs->saturation) * cs->triangle.dst_rect.h;

	in_long = max(cs->wheel.outer_radius/30, 4);
	in_lat = max(cs->wheel.outer_radius/160, 1);
	border = max(in_lat/2, 1);
	out_long = in_long + 2*border;
	out_lat = in_lat + 2*border;
	rim = out_lat;

	// horizontal rectangles
	inner_rects.left = (SDL_Rect){ .x = target.x - (rim + border + in_long), .y = target.y - in_lat/2, .w = in_long, .h = in_lat };
	outer_rects.left = (SDL_Rect){ .x = target.x -(rim + out_long), .y = target.y - out_lat/2, .w = out_long, .h = out_lat };
	inner_rects.right = (SDL_Rect){ .x = target.x + rim + border, .y = target.y - in_lat/2, .w = in_long, .h = in_lat };
	outer_rects.right = (SDL_Rect){ .x = target.x + rim, .y = target.y - out_lat/2, .w = out_long, .h = out_lat };
	// vertical rectangles
	inner_rects.upper = (SDL_Rect){ .x = target.x - in_lat/2, .y = target.y - rim - border - in_long, .w = in_lat, .h = in_long };
	outer_rects.upper = (SDL_Rect){ .x = target.x - out_lat/2, .y = target.y - rim - out_long, .w = out_lat, .h = out_long };
	inner_rects.lower = (SDL_Rect){ .x = target.x - in_lat/2, .y = target.y + rim + border, .w = in_lat, .h = in_long };
	outer_rects.lower = (SDL_Rect){ .x = target.x - out_lat/2, .y = target.y + rim, .w = out_lat, .h = out_long };
	// draw them all
	SDL_SetRenderDrawColor(screen->renderer, 0x00, 0x00, 0x00, 0xff);
	for (int i = 0; i < 4; i++) SDL_RenderFillRect(screen->renderer, &outer_rects.array[i]);
	SDL_SetRenderDrawColor(screen->renderer, 0xff, 0xff, 0xff, 0xff);
	for (int i = 0; i < 4; i++) SDL_RenderFillRect(screen->renderer, &inner_rects.array[i]);


	SDL_SetRenderDrawColor(screen->renderer, screen->background_color.r, screen->background_color.g, screen->background_color.b, screen->background_color.a);
}


SDL_HitTestResult hit_test_callback (SDL_Window *window, const SDL_Point *area, void *data) {
	return SDL_HITTEST_DRAGGABLE;
}


void load_configurations (Configs *configs) {
	int result;
	configs->buffer = read_file(NULL, "misc/", "config");
	if (!configs->buffer.config_ptr) {
		configs->buffer.size = 11 * sizeof(Configuration);
		configs->buffer.config_ptr = malloc(configs->buffer.size);
		if (!configs->buffer.config_ptr) err("failed to allocate configurations: %s\n", strerror(errno));
		configs->buffer.count = configs->buffer.size;
		configs->count = 10;
		configs->load_window_configs = (bool *)&configs->buffer.config_ptr[configs->count].height;
		*configs->load_window_configs = true;
		configs->preferred_index = &configs->buffer.config_ptr[configs->count].width;
		*configs->preferred_index = 1;
		configs->preferred = configs->buffer.config_ptr;
		for (int i = 0; i < configs->count; i++) {
			configs->buffer.config_ptr[i] = default_config;
		}
		configs->buffer.config_ptr[10].width = *configs->preferred_index;
		SDL_RWops *file = SDL_RWFromFile("misc/config", "w");
		if (!file) err("failed to open configuration file to be written: %s\n", SDL_GetError());
		result = SDL_RWwrite(file, configs->buffer.ptr, sizeof(char), configs->buffer.size);
		if (result < configs->buffer.size) err("failed to write to configuration file: written %d, expected %d: %s\n", result, configs->buffer.size, SDL_GetError());
		result = SDL_RWclose(file);
		if (result != 0) err("failed to close configuration file: %s\n", SDL_GetError());
	} else {
		configs->count = configs->buffer.count / sizeof(Configuration) - 1;
		configs->load_window_configs = (bool *)&configs->buffer.config_ptr[configs->count].height;
		configs->preferred_index = &configs->buffer.config_ptr[configs->count].width;
		configs->preferred = &configs->buffer.config_ptr[*configs->preferred_index];
		//printf("number of configurations = %d; preferred index = %d\n", configs->count, *configs->preferred_index);
	}
}


void recreate_window (Screen *screen, Fonts *fonts, Cursor *cursor) {
	for (int i = 0; i < arrsz(fonts->array); i++) {
		SDL_DestroyTexture(fonts->array[i].texture);
		fonts->array[i].texture = NULL;
	}
	SDL_DestroyTexture(cursor->texture);
	cursor->texture = NULL;
	SDL_DestroyRenderer(screen->renderer);
	SDL_DestroyWindow(screen->window);
	// create window
	screen->window = SDL_CreateWindow("Navegador de Dicionários", screen->x_pos, screen->y_pos, screen->width/screen->high_dpi_scaling.x, screen->height/screen->high_dpi_scaling.y, screen->window_flags);
	if (!screen->window) err("failed to create window: %s\n", SDL_GetError());
	SDL_SetWindowOpacity(screen->window, screen->opacity);
	// create renderer
	screen->renderer = SDL_CreateRenderer(screen->window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC | SDL_RENDERER_TARGETTEXTURE);
	if (!screen->renderer) err("failed to create renderer: %s\n", SDL_GetError());
	SDL_SetRenderDrawColor(screen->renderer, screen->background_color.r, screen->background_color.g, screen->background_color.b, screen->background_color.a);
	// create textures
	screen->texture = SDL_CreateTexture(screen->renderer, screen->texture_format, SDL_TEXTUREACCESS_TARGET, screen->width, screen->height);
	if (!screen->texture) err("failed to recreate screen texture (%dx%d): %s\n", screen->width, screen->height, SDL_GetError());
	for (int i = 0; i < arrsz(fonts->array); i++) {
		render_font_texture(&fonts->array[i], screen, true);
	}
	new_cursor(cursor, &fonts->monospaced, screen, false);
	int result = SDL_SetWindowHitTest(screen->window, ((~screen->window_flags & SDL_WINDOW_BORDERLESS) ? NULL : hit_test_callback), NULL);
	if (result != 0) warn("could not set window hit test: %s\n", SDL_GetError());
	// reload icon
	load_window_icon(screen, "misc/icon.bmp");
}

void save_configuration (Configs *configs) {
	int result;
	SDL_RWops *file = SDL_RWFromFile("misc/config", "w");
	if (!file) err("failed to open configuration file to be written: %s\n", SDL_GetError());
	result = SDL_RWwrite(file, configs->buffer.ptr, sizeof(char), configs->buffer.size);
	if (result < configs->buffer.size) err("failed to write to configuration file: written %d, expected %d: %s\n", result, configs->buffer.size, SDL_GetError());
	result = SDL_RWclose(file);
	if (result != 0) err("failed to close configuration file: %s\n", SDL_GetError());
}

// if index < 0, it's a new window
void assign_configuration (Configs *configs, Screen *screen, Fonts *fonts, int index) {
	bool new_window = index < 0;
	if (new_window) index = *configs->preferred_index;
	Configuration *loaded_config = &configs->buffer.config_ptr[index];

	// screen configuration
	if (*configs->load_window_configs || new_window) {
		screen->width = loaded_config->width * (new_window ? 1 : screen->high_dpi_scaling.x);
		screen->height = loaded_config->height * (new_window ? 1 : screen->high_dpi_scaling.y);
		screen->x_pos = loaded_config->x_pos;
		screen->y_pos = loaded_config->y_pos;
		screen->window_flags = loaded_config->window_flags;
		screen->opacity = loaded_config->opacity;
	}
	screen->background_color = loaded_config->background_color;
	screen->foreground_color = loaded_config->foreground_color;
	screen->pure_bg_color = loaded_config->pure_bg_color;
	screen->pure_fg_color = loaded_config->pure_fg_color;
	screen->num_lines_before_input = loaded_config->num_lines_before_input;
	screen->max_width = loaded_config->max_width;
	screen->relative_line_gap = loaded_config->relative_line_gap;
	screen->show_entry_horizontal_border = loaded_config->show_entry_horizontal_border;

	// fonts configuration
	if (!new_window) {
		fonts->resize_amount = loaded_config->base_size - fonts->regular.size;
		fonts->reweight_amount = loaded_config->weight - fonts->weight;
		fonts->resqueeze_amount = loaded_config->squeezing - fonts->squeezing;
	}
}


int threaded_dictionaries_loading (void *ptr) {
	uint32_t timing = SDL_GetTicks();
	printd("thread initialized after %ums\n", timing);
	Language *langs = ptr;

	load_dictionary(&langs[0].dicts[0], "ernesto_faria.txt");
	load_dictionary(&langs[0].dicts[1], "lewis_and_short.txt");
	load_dictionary(&langs[1].dicts[0], "liddell_scott_jones.txt");

	printd("dictionaries loading took %ums\n", SDL_GetTicks() - timing);
	return 0;
}


void mark_current_first_lines (Language *langs, int num_langs, Fonts *fonts, Screen *screen) {
	for (int i = 0; i < num_langs; i++) {
		for (int j = 0; j < langs[i].num_dicts; j++) {
			Dictionary *dict = &langs[i].dicts[j];

			if (dict->made_a_move) {
				if (dict->expanded.entry_index >= 0 && dict->expanded.line_start <= 0 && dict->expanded.line_end >= 0) {
					// mark first line
					num_lines_from_expanded_entry(dict, fonts, screen, true);
				} else {
					dict->first_line_marker = NULL;
				}

				dict->made_a_move = false;
			}

		}
	}
}


void modify_fonts (Fonts *fonts, Cursor *cursor, Screen *screen) {
	// fonts modification, if required
	if (fonts->reweight_amount) {
		//mark_current_first_lines(langs, arrsz(langs), fonts, screen);
		// to change font thickness, the font files must be swapped
		fonts->weight = max(min(fonts->weight + fonts->reweight_amount, BOLD), THIN);
		fonts->squeezing = max(fonts->squeezing + fonts->resqueeze_amount, MIN_SQUEEZING);
		//printf("squeezing = %d\n", fonts->squeezing);

		for (int i = 1; i < arrsz(fonts->array); i++) {
			replace_font(&fonts->array[i], screen, noto_font_filename(min(fonts->weight + ((fonts->array[i].flags & BOLDER) ? (fonts->weight >= REGULAR ? 3 : 1) : 0), BLACK), fonts->array[i].flags), max(fonts->array[i].size + fonts->resize_amount, 1), fonts->squeezing * (fonts->array[i].flags & BOLDER ? (fonts->weight < MEDIUM ? 1.3f : 1.2f + 0.2f*(fonts->weight - MEDIUM)) : 1));
			for (uint32_t c = 0; c < 128; c++) {
				bake_char_glyph(&fonts->array[i], c, c, false);
			}
			render_font_texture(&fonts->array[i], screen, true);
		}

		int replace_mono_font = 0;
		if (fonts->weight < REGULAR && fonts->weight - fonts->reweight_amount >= REGULAR) {
			replace_mono_font = -1;
		} else if (fonts->weight >= REGULAR && fonts->weight - fonts->reweight_amount < REGULAR) {
			replace_mono_font = 1;
		}
		if (replace_mono_font) {
			replace_font(&fonts->monospaced, screen, (replace_mono_font > 0 ? "cmuntb.ttf" : "cmuntt.ttf"), max(fonts->monospaced.size + fonts->resize_amount, 1), fonts->squeezing);
			for (uint32_t c = 0; c < 128; c++) {
				bake_char_glyph(&fonts->monospaced, c, c, false);
			}
			render_font_texture(&fonts->monospaced, screen, true);
			new_cursor(cursor, &fonts->monospaced, screen, true);
		} else if (fonts->resize_amount || fonts->resqueeze_amount) {
			resize_font(&fonts->monospaced, screen, max(fonts->monospaced.size + fonts->resize_amount, 1), fonts->squeezing);
			new_cursor(cursor, &fonts->monospaced, screen, true);
		}
		fonts->resqueeze_amount = fonts->reweight_amount = fonts->resize_amount = 0;
		//adapt_to_resizing(langs, arrsz(langs), fonts, screen);
		screen->refresh_count = 0;

	} else if (fonts->resize_amount || fonts->resqueeze_amount) {
		//mark_current_first_lines(langs, arrsz(langs), fonts, screen);
		fonts->squeezing = max(fonts->squeezing + fonts->resqueeze_amount, MIN_SQUEEZING);
		//printf("squeezing = %d\n", fonts->squeezing);
		for (int i = 0; i < arrsz(fonts->array); i++) {
			resize_font(&fonts->array[i], screen, max(fonts->array[i].size + fonts->resize_amount, 1), fonts->squeezing * (fonts->array[i].flags & BOLDER ? (fonts->weight < MEDIUM ? 1.3f : 1.2f + 0.2f*(fonts->weight - MEDIUM)) : 1));
		}
		new_cursor(cursor, &fonts->monospaced, screen, true);
		//change_cursor(&cursor, lang
		fonts->resize_amount = fonts->resqueeze_amount = 0;
		//adapt_to_resizing(langs, arrsz(langs), fonts, screen);
		screen->refresh_count = 0;
	}
}


void load_window_icon (Screen *screen, char *img_filename) {
	SDL_Surface *surface;
	surface = SDL_LoadBMP(img_filename);
	if (!surface) err("failed to open icon's image file '%s': %s\n", img_filename, SDL_GetError());
	SDL_SetWindowIcon(screen->window, surface);
	SDL_FreeSurface(surface);
}


// signalizes if the configurations was set to active or inactive
void draw_signalizer (Screen *screen, Fonts *fonts, int state) {
	int border_width = max(0.5f*((screen->border_width * 0.5f) * (0.22f + (fonts->weight - 3) * 0.07f)) + 0.5f, 1);
	int square_side = 1.1f*fonts->regular.baked_chars.baked_ptr['m'].xadvance + 0.5f;
	SDL_Color fg, bg;
	SDL_Rect rect = {
		.x = screen->width - screen->excess_width - square_side - 2*border_width,
		.y = screen->height - square_side - 2*border_width,
		.w = square_side + 2*border_width,
		.h = square_side + 2*border_width,
	};
	if (state > 0) {
		fg = screen->foreground_color;
		bg = screen->background_color;
	} else if (state < 0) {
		fg = screen->background_color;
		bg = screen->foreground_color;
	}
	SDL_SetRenderDrawColor(screen->renderer, bg.r, bg.g, bg.b, bg.a);
	SDL_RenderFillRect(screen->renderer, &rect);

	rect.x += border_width;
	rect.y += border_width;
	rect.w -= 2*border_width;
	rect.h -= 2*border_width;

	SDL_SetRenderDrawColor(screen->renderer, fg.r, fg.g, fg.b, fg.a);
	SDL_RenderFillRect(screen->renderer, &rect);

	if (state > 0) SDL_SetRenderDrawColor(screen->renderer, screen->background_color.r, screen->background_color.g, screen->background_color.b, screen->background_color.a);
}


void update_input (Language *lang, Screen *screen, bool text_input_on) {
	for (int i = 0; i < lang->num_dicts; i++) {
		Dictionary *dict = &lang->dicts[i];
		dict->matched.entry_index = search_for_entry_index(dict, lang->strcmp, lang->input+MIN_INPUT_LENGTH);
		//printf("entry index = %d, max entry = %d\n", dict->matched.entry_index, dict->num_entries);
		dict->matched.line_start = dict->matched.line_end = screen->num_lines_before_input + 1;

		if (dict->expanded.entry_index >= 0) {
			int expansion = dict->expanded.line_end - dict->expanded.line_start;
			if (dict->expanded.entry_index >= dict->matched.entry_index) {
				dict->expanded.line_start = dict->matched.line_start + (dict->expanded.entry_index - dict->matched.entry_index);
				dict->expanded.line_end = dict->expanded.line_start + expansion;
			} else {
				dict->expanded.line_end = dict->matched.line_start - (dict->matched.entry_index - dict->expanded.entry_index) - 1;
				dict->expanded.line_start = dict->expanded.line_end - expansion;
			}
		}

		if (!text_input_on) {
			dict->selected = (dict->matched.entry_index == dict->expanded.entry_index) ? dict->expanded : dict->matched;
			if (dict->selected.entry_index >= dict->num_entries) {
				dict->selected.entry_index--;
				if (dict->selected.entry_index == dict->expanded.entry_index) {
					dict->selected = dict->expanded;
				} else {
					dict->selected.line_start -= 2;
					dict->selected.line_end -= 2;
				}
				if (dict->selected.line_start < 0) {
					displace_lines(dict, -dict->selected.line_start);
				}
			} else if (dict->selected.line_start >= screen->num_lines_on_screen) {
				displace_lines(dict, (screen->num_lines_on_screen-1) - dict->selected.line_start);
			}
			dict->selected_expected_line = max(dict->selected.line_start, 0);
		}

	}
}

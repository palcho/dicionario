#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
//#include <ctype.h>
//#include <string.h>

int read_entry (char *entry);
int greek_strcmp (char *str1, char *str2);
int latin_strcmp (char *str1, char *str2);
uint32_t utf8_to_unicode (char **string);

int main (int argc, char **argv) {
	char entry[3][120];
	int count = 0;

	for (int i = 1; i < 3; i++) {
		read_entry(entry[i]);
	}
	count = 3;

	while (read_entry(entry[count % 3]) > 0) {
		if (latin_strcmp(entry[(count + 2) % 3], entry[(count + 1) % 3]) <= 0) {
			printf("%d : %s | %s | %s\n", count-1, entry[(count + 1) % 3], entry[(count + 2) % 3], entry[count % 3]);
		}
		count++;
	} 

	return 0;
}

int read_entry (char *entry) {
	int first, last, ret;
	//ret = scanf(" n%*d#%n%s%n%*[^\n]", &first, entry, &last);
	ret = scanf(" %n%[^:]%n%*[^\n]", &first, entry, &last);
	if (ret > 0) {
		//entry[0] = tolower(entry[0]);
		if (entry[last - first - 1] == ',') {
			entry[last - first - 1] = '\0';
		}
	}
	return ret;
}

enum Greek_Unicodes {
	ALPHA_MAIUS   = 913,
	OMEGA_MAIUS   = 937,
	ALPHA_MINUS   = 945,
	OMEGA_MINUS   = 969,
	SIGMA_FINAL   = 962,
	SIGMA_COMMON  = 963,
	ZETA_MINUS    = 950,
	DIGAMMA_MAIUS = 988,
	DIGAMMA_MINUS = 989,
	YOT_MAIUS     = 895,
	YOT_MINUS     = 1011,
};

int greek_strcmp (char *str1, char *str2) {
	char *str[2] = { str1, str2 };
	int unicode[2];

	do {
		for (int i = 0; i < 2; i++) {
			unicode[i] = utf8_to_unicode(&str[i]);

			if (unicode[i] >= ALPHA_MINUS && unicode[i] <= OMEGA_MINUS) {
			avoid_testing_again:
				if (unicode[i] >= ZETA_MINUS && unicode[i] <= SIGMA_FINAL) {
					// make SIGMA_FINAL = SIGMA_COMMON and give space to DIGAMMA_MINUS
					unicode[i]++;
				}
			} else if (unicode[i] >= ALPHA_MAIUS && unicode[i] <= OMEGA_MAIUS) {
				unicode[i] += ALPHA_MINUS - ALPHA_MAIUS;
				goto avoid_testing_again;
			} else if (unicode[i] == DIGAMMA_MAIUS || unicode[i] == DIGAMMA_MINUS) {
				// put DIGAMMA_MINUS in its ancient place
				unicode[i] = ZETA_MINUS;
			} else if (unicode[i] == YOT_MAIUS) {
				unicode[i] = YOT_MINUS;
			}
		}

	} while (unicode[0] == unicode[1] && unicode[0] != '\0' && unicode[1] != '\0');

	return unicode[0] - unicode[1];
}

int latin_strcmp (char *str1, char *str2) {
	uint8_t *s1 = (uint8_t *)str1;
	uint8_t *s2 = (uint8_t *)str2;
	int c1, c2;
	do {
		c1 = (*s1 >= 'A' && *s1 <= 'Z') ? *s1 + ('a'-'A') : *s1;
		c2 = (*s2 >= 'A' && *s2 <= 'Z') ? *s2 + ('a'-'A') : *s2;
		s1++; s2++;
	} while (c1 == c2 && c1 && c2);

	return c1 - c2;
}

uint32_t utf8_to_unicode (char **string) {
	uint8_t *s = (uint8_t *)*string;
	uint32_t unicode;

	if ((*s & 0x80) == 0x00) {
		unicode = *s;
		*string += 1;
	} else if ((*s & 0xe0) == 0xc0) {
		unicode = ((*s & 0x3f) << 6) | (*(s+1) & 0x7f);
		*string += 2;
	} else if ((*s & 0xf0) == 0xe0) {
		unicode = ((*s & 0x1f) << 12) | ((*(s+1) & 0x7f) << 6) | (*(s+2) & 0x7f);
		*string += 3;
	} else if ((*s & 0xf8) == 0xf0) {
		unicode = ((*s & 0x0f) << 18) | ((*(s+1) & 0x7f) << 12) | ((*(s+2) & 0x7f) << 6) | (*(s+3) & 0x7f);
		*string += 4;
	}  else {
		unicode = 1;
		*string += 1;
		fprintf(stderr, "INCORRECT UTF-8: 0x%02x\n", *s);
	}

	return unicode;
}

.PHONY: clean

#LINUX
dicionario_latino: src/main.c include/stb_truetype.h makefile
	gcc $< -O3 -flto -Wall -Wextra -Iinclude -I/usr/include/freetype2 -Wl,-rpath,lib -lfreetype -lSDL2 -lX11 -lm -o $@
debug: src/main.c include/stb_truetype.h makefile
	gcc $< -O0 -ggdb -Wall -Wextra -Iinclude -I/usr/include/freetype2 -Wl,-rpath,lib -lfreetype -lSDL2 -lm -lX11 -o $@

# WINDOWS
#dicionario_latino.exe: src/main.c include/stb_truetype.h makefile
#	gcc $< -O3 -flto -Wall -Wextra -Iinclude -lmingw32 -lfreetype -lSDL2main -lSDL2 -lm -Wl,-rpath=. -Wl,-subsystem,windows -o $@
#debug.exe: src/main.c include/stb_truetype.h makefile
#	gcc $< -O0 -ggdb -Wall -Wextra -Iinclude -lmingw32 -lfreetype -lSDL2main -lSDL2 -lm -o $@

# MAC
#dicionario_latino: src/main.c include/stb_truetype.h makefile
#	gcc $< -O3 -flto -Wall -Wextra -Iinclude -I/usr/local/Cellar/freetype/2.10.4/include/freetype2 -Llib -Wl,-rpath,lib -lfreetype -lSDL2 -lm -o $@
#	install_name_tool -change /usr/local/opt/sdl2/lib/libSDL2-2.0.0.dylib @executable_path/lib/libSDL2-2.0.0.dylib $@
#	install_name_tool -change /usr/local/opt/freetype/lib/libfreetype.6.dylib @executable_path/lib/libfreetype.6.dylib $@
#debug: src/main.c include/stb_truetype.h makefile
#	gcc $< -O0 -ggdb -Wall -Wextra -Iinclude -I/usr/local/Cellar/freetype/2.10.4/include/freetype2 -Llib -Wl,-rpath,lib -lfreetype -lSDL2 -lm -o $@

tags: src/main.c include/stb_truetype.h
	ctags src/main.c include/*

clean:
	-rm -f dicionario_latino debug
